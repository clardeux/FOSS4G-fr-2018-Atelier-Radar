<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="2.18.19" minimumScale="inf" maximumScale="1e+08" hasScaleBasedVisibilityFlag="0">
  <pipe>
    <rasterrenderer opacity="1" alphaBand="-1" classificationMax="12" classificationMinMaxOrigin="CumulativeCutFullExtentEstimated" band="1" classificationMin="0" type="singlebandpseudocolor">
      <rasterTransparency/>
      <rastershader>
        <colorrampshader colorRampType="INTERPOLATED" clip="0">
          <item alpha="255" value="1" label="Mangrove cotière" color="#5f9200"/>
          <item alpha="255" value="2" label="Forêt" color="#007c04"/>
          <item alpha="255" value="3" label="Zones humides" color="#f6b16c"/>
          <item alpha="255" value="4" label="Zones herbacées" color="#a68cb8"/>
          <item alpha="255" value="5" label="Sols nu" color="#fcffd2"/>
          <item alpha="255" value="6" label="Eau" color="#485aab"/>
          <item alpha="255" value="7" label="Zone urbaine" color="#d43333"/>
          <item alpha="255" value="11" label="Mangrove bord de rivière (Peu dense)" color="#e3df54"/>
          <item alpha="255" value="12" label="Mangrove bord de rivière (dense)" color="#9cb800"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeBlue="128" grayscaleMode="0" saturation="0" colorizeStrength="100"/>
    <rasterresampler maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
