Ce repository recense l'ensemble des outils, données et tutoriels présenté lors de l'atelier radar lors du FOSS4G-fr 2018.

# Description de l'Atelier d’utilisation des séries temporelle radar Sentinel-1 pour la cartographie d’occupation du sol.
Nous présentons une boite à outil basée sur QGIS pour effectuer les pré-traitements des données radar Sentinel-1 nécessaires
pour leur utilisation dans les algorithmes de classifications (Kmeans, SVM, Random Forest, etc).

# Contenu
- Données : sources et/où pré-traité nécessaire pour réaliser le contenu de l'atelier
- Tutorial : tous les tutoriels (pdf accompagné de vidéos)
- ScriptInstallationUbuntu: script permettant d'installer les outils sur Ubuntu (base QGIS 2.18 + OTB)

# Lien vers une machine virtuelle
http://teledetection-radar.u-pem.fr/FOSS4G-fr/XubuntuRemoteSensing.ova