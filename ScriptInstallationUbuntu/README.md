# QGISProcessingScripts

This repository contain QGIS processing scripts

Alpha version, works on going !

# How to install ?
- Download all the files and subfolder in an empty directory
- Open a terminal
	- go to the directory using command like "cd /home/myuser/MyFolder
	- type the command "sh FullInstall.sh
	- It will ask you to give the sudo password
	- Wait
	- At the end reboot the computer
- Enjoy !!!

## Authors

- Cedric Lardeux
- Pierre-Louis Frison

## Acknowledgment

- Thierry Koleck for it's codes where we take some nice ideas<http://tully.ups-tlse.fr/koleckt/s1tiling/blame/v1.1/S1Processor.cfg>
- The team of <https://github.com/openforis/opensarkit> where we takes ideas from installer script