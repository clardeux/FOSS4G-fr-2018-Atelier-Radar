mkdir -p ~/.qgis2
mkdir -p ~/.qgis2/processing
mkdir -p ~/.qgis2/processing/scripts

cp -R $PWD/scripts/ ~/.qgis2/processing
cp -R $PWD/OnlineWMSRaster/ ~/.qgis2