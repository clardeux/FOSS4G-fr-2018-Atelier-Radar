#!/bin/bash
cd $(dirname $(readlink -f $0 || realpath $0))

sudo sh 1-Ubuntu16.04InstallScript.sh
sh 2-InstallQgisScripts.sh
sudo sh 3-InstallOTBCUstomUseSUDO.sh
