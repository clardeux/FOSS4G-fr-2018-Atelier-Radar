##Sentinel-1 Deforestation Process=group
##2 - Deforestation Analysis=name
##Input_Data_Folder=folder
##Start_Analysis_Date=string 20160124
##Cumulative_Change_Threshold_dB=number -1.
##Local_Change_Threshold_dB=number -0.25
##Minimum_Maping_Unit_Pixel=number 10
##Output_Data_Folder=folder
##Ram=number 256


# Authors: Cedric Lardeux et Pierre-Louis Frison

import os, sys, shutil, os.path
import subprocess
from datetime import datetime
from osgeo import gdal
import numpy as np
from scipy.signal import argrelextrema

# Variable
Input_Data_Folder = '/home/DataProcessing/FrenchGuyana/OptCont/p120'
Start_Analysis_Date = '20161225'

Output_Data_Folder = '/home/DataStorage/FrenchGuyana/DeforestationMaps/p120'
Ram = 15000
No_Data = 0

Cumulative_Change_Threshold_dB = -1.6
Local_Change_Threshold_dB = -0.5

Minimum_Maping_Unit_Pixel = 10
#
#Internal global variable
#BlockSize = 2048


eps = 1e-16

Input_Folder = Input_Data_Folder
Output_Folder = Output_Data_Folder
Cumulative_Derivatives_Threshold = Cumulative_Change_Threshold_dB
Derivatives_Threshold = Local_Change_Threshold_dB








'''
Program with help of Cesbio for Thierry col


This algorithm is dedicated to extract deforestation over time series
it alow to combine different bands at the same date
It's based on the general approach of
Reiche et al A Bayesian Approach to Combine Landsat and ALOS PALSAR Time Series
for Near Real-Time Deforestation Detection. Remote Sensing, 7, 4973-4996

Here:
Exemple of Sentinel-1 Time serie VV VH polarization
- Prepare the time series to have on exactly the same area on file per date (all indicaors you wants)
- Create a shape file containing polygon over
    - Stable forest (id=1)
    - Stable non forest (id=2)

INPUT
- Folder that contain all the dates (vrt or tiff)
- ROI Shape path
- Field name that contain the id of the classes (integer)
- The reference date to begin the analysis
- The reference date to build the Random Forest model
(date that is a good average of the Forest Non forest radiometry over all the Time Serie)
- Output Folder
- Forest probability threshold ie 0.8 (below)
 - Deforestation probability threshold i.e 0.9

The program do the following:
I)         Train the RF using roi over the reference date to generate the generic classification model

II)     For each date use the generic model and generate one classification and the confidence map (ConfMap between 0 an 1)

III)     From classification and confidence map
            create a Forest Probability map (FP)
                if Classif = 1 then FP = ConfMap
                Else FP = 1 - Confmap

IV)     Applys spatial and or time filtering of FP
        - Apply median filtering to reduce noise
        - If necessary apply explonential mean on the FP time series in order to reduce gaps in the FP
        that generaly decrease and not increase (deforestation is fast contrary to regeneration)
V)        Generate Deforestation probability DP using NFP = 1 - FP
            DP = (NFP(t-1) * NFP(t)) /((NFP(t-1) * NFP(t)) + ((1 - NFP(t-1)) * (1 - NFP(t))))

VI)        Analyse the FP time series
            For each date
                - Forest flag (Yes FF=1, No FF=0)
                - Deforestation flag (No DF = 0, Yes first time DF = 1, Yes second time just after DF = 2)
            - Check the probability to know if the area is F or NF (PF> 0.5)
                If forest
                    then FF=1 and DF = 0 and DP = 0 (erase DP)
                If No forest
                    then FF = 0
                    if FF (t-1) = 1
                        then DF = 1
                    else then DF = 0 and DP = 0 (erase DP)

'''
def GdalSieve(aInputRaster, aOutputFile,  aPixelTh):
    if 'nt' in os.name:
        cmd = "gdal_sieve.bat   "
    else:
        cmd = "gdal_sieve.py   "
    cmd += " -nomask"
    cmd += " -st " + str(aPixelTh)
    cmd += " -of GTiff "
    cmd += " " + aInputRaster
    cmd += " " + aOutputFile

#    progress.setInfo(cmd)
    # print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

def OTBConvertTomask(aInputRaster, aOutputRaster, aRam):
    cmd = "otbcli_BandMath -il "
    cmd += " " + aInputRaster
    cmd += " -ram " + str(aRam)
    cmd += " -out "
    cmd += aOutputRaster
    cmd += " -exp "
    cmd += "\"im1b1>0?1:0\""

    # progress.setInfo(cmd)
    # print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]


def OTBMask(aInputRaster1, aInputRaster2, aOutputRaster, aRam):
    cmd = "otbcli_BandMath -il "
    cmd += " " + aInputRaster1
    cmd += " " + aInputRaster2
    cmd += " -ram " + str(aRam)
    cmd += " -out "
    cmd += aOutputRaster
    cmd += " -exp "
    cmd += "\"im1b1*im2b1\""

    # progress.setInfo(cmd)
    # print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

def ApplyMMUToDeforestationMap(aInputDeforestationFile, aOutputDefMMU, aPixelTh, aRam):
    # We get File dir
    dir_path = os.path.dirname(os.path.realpath(aInputDeforestationFile)).replace("\\","/")

    # We create TMP dir
    TmpDir = os.path.join(dir_path, "tMp").replace("\\","/")
    if not os.path.exists(TmpDir):
        os.makedirs(TmpDir)

    # We create intermediate file nam
    FileMask = os.path.join(TmpDir, "FileMask.tif").replace("\\","/")
    FileMaskSieved = os.path.join(TmpDir, "FileMaskSieved.tif").replace("\\","/")

    OTBConvertTomask(aInputDeforestationFile, FileMask, aRam)

    GdalSieve(FileMask, FileMaskSieved,  aPixelTh)

    OTBMask(aInputDeforestationFile, FileMaskSieved, aOutputDefMMU, aRam)

    shutil.rmtree(TmpDir)

def getDateFromS1Raster(aFileName):
    aFileName = aFileName.split("/")[-1]


    pos=max([aFileName.find('_2015'),aFileName.find('_2016'),aFileName.find('_2017'),aFileName.find('_2018'),aFileName.find('_2019'),aFileName.find('_2020')])
    return aFileName[pos+1:pos+9]

def OtbGenerateRasterStat(aInputRaster, aNoData, aOutputStatFile):
    cmd = "otbcli_ComputeImagesStatistics -il "
    cmd += aInputRaster + " "
    cmd += " -bv " + str(aNoData)
    cmd += " -out " + aOutputStatFile

    # progress.setInfo(cmd)
    # print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

def OtbTrainRF(aInputRaster, aInputRasterStat, aInputVector, aField, aMaxNumSample, aNbTrees, aOutputModel, aRam):
    cmd = "otbcli_TrainImagesClassifier -io.il "
    cmd += aInputRaster + " "
    cmd += " -io.vd  " + aInputVector
    cmd += " -io.imstat  " + aInputRasterStat
    cmd += " -sample.mt  " + str(aMaxNumSample)
    cmd += " -sample.mv  " + str(aMaxNumSample)
    cmd += " -sample.vfn  " + aField
    cmd += " -classifier  rf "
    cmd += " -classifier.rf.max 25 "
    cmd += " -classifier.rf.min 25 "
    cmd += " -classifier.rf.nbtrees "  + str(aNbTrees)
    cmd += " -rand  1 "
    cmd += " -io.out " + aOutputModel
    cmd += " -ram " + str(aRam)

    # progress.setInfo(cmd)
    # print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

def OtbClassifier(aInputRaster, aInputRasterStat, aInputModel, aOutputClassif, aOutputConfMap, aRam):
    cmd = "otbcli_ImageClassifier -in "
    cmd += aInputRaster + " "
    cmd += " -model  " + aInputModel
    cmd += " -out  " + aOutputClassif
    cmd += " -confmap  " + aOutputConfMap
    cmd += " -imstat " + aInputRasterStat
    cmd += " -ram " + str(aRam)

    # progress.setInfo(cmd)
    # print '\n',cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

'''
This function list this immediate subdirectories in one given directory
'''
def get_immediate_subdirectories(a_dir):
    return [os.path.join(a_dir, name) for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

'''
This function list all file with a given file extention in one directory
'''
def directory_liste_ext_files(directory, filt_ext):
    list_file = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in filenames:
            if filename.endswith((filt_ext)):
                myfile = os.path.join(root, filename)
                list_file.append(myfile.replace("\\","/"))

    list_file.sort()
    return list_file

def CreateReferenceRandomForestClassifier(aInputRaster,aNoData, aStatFile, aInputVector, aField, aMaxNumSample, aNbTrees, aModel, aRam):
    # Generate raster stat file
    OtbGenerateRasterStat(aInputRaster, aNoData, aStatFile)

    # Train the classifier
    OtbTrainRF(aInputRaster, aStatFile, aInputVector, aField, aMaxNumSample, aNbTrees, aModel, aRam)

def GenerateAllClassification(aInputRasterList, aOutputClassifList, aOutputConfMapList, aInputRasterStat, aInputModel, aRam):
    for i in range(len(aInputRasterList)):
        InRaster = aInputRasterList[i]
        OutClassif = aOutputClassifList[i]
        OutConfMap = aOutputConfMapList[i]

        OtbClassifier(InRaster, aInputRasterStat, aInputModel, OutClassif, OutConfMap, aRam)

def OtbBandMathCreateNoForestProb(aInputClassif, aInputConfMap, aOutputForestProb, aRam):
    # Forest label = 1 Non forest = 2
    cmd = "otbcli_BandMath -il "
    cmd += aInputClassif + " "
    cmd += aInputConfMap + " "
    cmd += " -ram " + str(aRam)
    cmd += " -out "
    cmd += aOutputForestProb
    cmd += " -exp "
    cmd += "\" im1b1==2?im2b1:(im1b1==1?1-im2b1:0) \""

    # progress.setInfo(cmd)
    # print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

def GenerateNoForestProb(aInputClassifList, aInputConfMapList, aOutputForstProbList, aRam):
    for i in range(len(aInputClassifList)):
        InputClassif = aInputClassifList[i]
        InputConfMap = aInputConfMapList[i]
        OutputForestProb = aOutputForstProbList[i]
        OtbBandMathCreateNoForestProb(InputClassif, InputConfMap, OutputForestProb, aRam)

def EMWA(aData, aAlpha):
    Smooth = np.zeros(shape=aData.shape,dtype=np.float)
    SmoothOld = np.zeros(shape=aData.shape[1:3],dtype=np.float)

    SmoothOld = aData[0,:,:]
    Smooth[0,:,:] = aData[0,:,:]

    for i in range(1,aData.shape[0]):
        Smooth[i,:,:] = aAlpha * aData[i,:,:] + (1- aAlpha) * SmoothOld
        SmoothOld = Smooth[i,:,:]

    return Smooth

def GdalBuildVRT(aInputListFile, aOutputFile,  aSRCNoData, aVRTNoData):
    TmpListfile = aOutputFile.replace('.vrt', 'TMPListFile.txt')
    fic=open(TmpListfile,'w')
    for file in aInputListFile:
        fic.write(file +'\n')
    fic.close()

    cmd = "gdalbuildvrt "
    cmd += " -separate -overwrite "
    cmd += " -srcnodata " + str(aSRCNoData)
    cmd += " -vrtnodata " + str(aVRTNoData)
    cmd += " " + aOutputFile
    cmd += " -input_file_list " + TmpListfile

    # progress.setInfo(cmd)
    # print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

    if os.path.exists(TmpListfile):
        os.remove(TmpListfile)



def GetFilesFromVrt(aInputVRT):
    #Open the VRT
    ds = gdal.Open(aInputVRT)

    # Get file name path
    RawVrtFileList = ds.GetFileList()[1:]

    ds = None

    if '.ovr' in RawVrtFileList[0]:
        RawVrtFileList = RawVrtFileList[1:]

    return RawVrtFileList


def RadiometryAnalysis(aData, aCumDerivativeThreshold, aDerivativeThreshold):
    '''
    Analyse the FP time series
            For each date
                - Deforestation flag (No DF = 0,   Confirmed DF = 1)
            - If  CumDerivatives (t) <= -1 AND ( Derivative (t) <= 0.25 OR Derivative (t+1) <= 0.25)
                Flag = 1

    '''
    NumDates = aData.shape[0]

    DeforestationFlag = np.zeros(shape=aData.shape[1:3],dtype=np.int16)
    ConfirmedDeforestationFullMagnitude = np.zeros(shape=aData.shape[1:3],dtype=np.float)
    ConfirmedDeforestationChangeMagn = np.zeros(shape=aData.shape[1:3],dtype=np.float)
    DeforestationBandDate = np.zeros(shape=aData.shape[1:3],dtype=np.float)

    # Compute Derivatives
    Derivatives = np.diff(aData, axis=0)
    # Derivatives = convolve1d(aData,[-1,1],axis=0, mode='wrap')

    # Compute cumulatives derivatives
    CumDerivatives = np.cumsum(Derivatives, axis=0)

    NumDates = np.shape(Derivatives)[0]

    # Detect deforestation (Flag)
    # Loop thru dates (begin to second dates)
    # WhDerivative = 0
    # print 'NumDates, np.shape(Derivatives), np.shape(CumDerivatives)', NumDates, np.shape(Derivatives), np.shape(CumDerivatives)
    for i in range(0,NumDates):
        if i == 0:
            # print 'i  = 0 ', i
            WhDerivative = Derivatives[i+1,:,:] <= aDerivativeThreshold
        elif i < NumDates -1:
            # print 'i < NumDates -2', i
            WhDerivative = np.logical_or(Derivatives[i-1,:,:] <= aDerivativeThreshold,Derivatives[i+1,:,:] <= aDerivativeThreshold)
        else:
            # print 'i > NumDates -2', i
            WhDerivative = Derivatives[i-1,:,:] <= aDerivativeThreshold

        WhCumDer = np.logical_and(WhDerivative,  CumDerivatives[i,:,:] <= aCumDerivativeThreshold)
        WhConfirmedDef = np.logical_and(DeforestationFlag[:,:] == 0, WhCumDer)

        DeforestationFlag[:,:][WhConfirmedDef] = 1
    WhFinalNoDefFlag = DeforestationFlag == 0

    # Get Deforestation dates (first lowest derivatives)
    if NumDates <=2:
        DeforestationBandDate = np.argmin(Derivatives[:,:,:], axis = 0) + 1
    else:
        DeforestationBandDate = np.argmin(Derivatives[1:-1,:,:], axis = 0) + 1
    # Erase No deforestation area
    DeforestationBandDate[WhFinalNoDefFlag] = 0

    # Get lowest derivatives as one deforestation event magnitude
    ConfirmedDeforestationChangeMagn = np.amin(Derivatives[:,:,:], axis=0)
    # Erase No deforestation area
    #ConfirmedDeforestationChangeMagn[WhFinalNoDefFlag] = 0

    # Get lowest cumulative derivatives as total deforestation magnitude
    ConfirmedDeforestationFullMagnitude = np.amin(CumDerivatives[:,:,:], axis=0)
    # Erase No deforestation area
    #ConfirmedDeforestationFullMagnitude[WhFinalNoDefFlag] = 0

    return DeforestationBandDate, ConfirmedDeforestationFullMagnitude, ConfirmedDeforestationChangeMagn

def RadiometryAnalysisNew(aData, aCumDerivativeThreshold, aDerivativeThreshold):
    '''
    Analyse the FP time series
            For each date
                - Deforestation flag (No DF = 0,   Confirmed DF = 1)
            - If  CumDerivatives (t) <= -1 AND ( Derivative (t) <= 0.25 OR Derivative (t+1) <= 0.25)
                Flag = 1

    '''
    NumDates = aData.shape[0]

    DeforestationFlag = np.zeros(shape=aData.shape[1:3],dtype=np.int16)
    ConfirmedDeforestationFullMagnitude = np.zeros(shape=aData.shape[1:3],dtype=np.float)
    ConfirmedDeforestationChangeMagn = np.zeros(shape=aData.shape[1:3],dtype=np.float)
    DeforestationBandDate = np.zeros(shape=aData.shape[1:3],dtype=np.float)


    # Compute cumulatives derivatives
    # Extract extrema
    LocMaxAdTimeBand, LocMaxAdRow, LocMaxAdCols  =  argrelextrema(aData, np.greater, axis=0)
    LocMinAdTimeBand, LocMinAdRow, LocMinAdCols  =  argrelextrema(aData, np.less, axis=0)

    LocExtremaTime = np.concatenate((LocMaxAdTimeBand,LocMinAdTimeBand), axis=0 )
    LocExtremaRow = np.concatenate((LocMaxAdRow,LocMinAdRow), axis=0 )
    LocExtremaCols = np.concatenate((LocMaxAdCols,LocMinAdCols), axis=0 )

    # Create empty Cumulative derivative
    CumDerivatives = np.zeros(aData.shape)

    # Reference radiometry begin with first date and update when an extrema is found
    # Cast to int to save memory
    # Take for each pixel and each band the reference band used for CumDeri
    RefBand = np.zeros(aData.shape)

    Ref = np.zeros(aData.shape[1:3])
    Ref = aData[0,:,:]
    for i in range(NumDates):
        CumDerivatives[i,:,:] = aData[i,:,:] - Ref
        if i in LocExtremaTime:
            Wh = LocExtremaTime == i
            Ref[LocExtremaRow[Wh], LocExtremaCols[Wh]] = aData[LocExtremaTime[Wh], LocExtremaRow[Wh], LocExtremaCols[Wh]]

            RefBand[i,LocExtremaRow[Wh], LocExtremaCols[Wh]] = i
        # copy paste of actual ref band to next band to initiate next loop
        if i < NumDates - 1:
            RefBand[i+1, :, :] = RefBand[i, :, :]

    # Compute Derivatives
    Derivatives = np.diff(aData, axis=0)

    NumDates = np.shape(Derivatives)[0]

    # Detect deforestation (Flag)
    # Loop thru dates (begin to second dates)
    # WhDerivative = 0
    # print 'NumDates, np.shape(Derivatives), np.shape(CumDerivatives)', NumDates, np.shape(Derivatives), np.shape(CumDerivatives)

    # Initiate deforestationbanddate
    DeforestationBandDate = np.zeros(aData.shape[1:3])

    for i in range(0,NumDates):
        if i == 0:
            # print 'i  = 0 ', i
            WhDerivative = Derivatives[i+1,:,:] <= aDerivativeThreshold
        elif i < NumDates -1:
            # print 'i < NumDates -2', i
            WhDerivative = np.logical_or(Derivatives[i-1,:,:] <= aDerivativeThreshold,Derivatives[i+1,:,:] <= aDerivativeThreshold)
        else:
            # print 'i > NumDates -2', i
            WhDerivative = Derivatives[i-1,:,:] <= aDerivativeThreshold

        WhCumDer = np.logical_and(WhDerivative,  CumDerivatives[i,:,:] <= aCumDerivativeThreshold)
        WhConfirmedDef = np.logical_and(DeforestationFlag[:,:] == 0, WhCumDer)

        # We get deforestation band date equal to reference extrema + 1
        if i < NumDates + 1:
            DeforestationBandDate[WhConfirmedDef] = RefBand[i+1,:,:][WhConfirmedDef]
        else:
            DeforestationBandDate[WhConfirmedDef] = RefBand[i,:,:][WhConfirmedDef]

        DeforestationFlag[:,:][WhConfirmedDef] = 1
    WhFinalNoDefFlag = DeforestationFlag == 0

    # Erase No deforestation area
    DeforestationBandDate[WhFinalNoDefFlag] = 0

    # Get lowest derivatives as one deforestation event magnitude
    ConfirmedDeforestationChangeMagn = np.amin(Derivatives[:,:,:], axis=0)
    # Erase No deforestation area
    #ConfirmedDeforestationChangeMagn[WhFinalNoDefFlag] = 0

    # Get lowest cumulative derivatives as total deforestation magnitude
    ConfirmedDeforestationFullMagnitude = np.amin(CumDerivatives[:,:,:], axis=0)
    # Erase No deforestation area
    #ConfirmedDeforestationFullMagnitude[WhFinalNoDefFlag] = 0

    return DeforestationBandDate, ConfirmedDeforestationFullMagnitude, ConfirmedDeforestationChangeMagn

def getDayFromS1File(aFileName):
    aFileName = aFileName.split("/")[-1]


    pos=max([aFileName.find('_2015'),aFileName.find('_2016'),aFileName.find('_2017'),aFileName.find('_2018'),aFileName.find('_2019'),aFileName.find('_2020')])
    return aFileName[pos+1:pos+9]

def TileRasterRadAnalysis(aInputRaster, aOutputList, aCumDerivativeThreshold, aDerivativeThreshold, aBlockSize,aWindowSize):
    '''
    TODO: Add sliding window parameter


    This method show how to process huge raster
    using easy tilling based on block size
    and if necessary taking into account window size
    when necessary in processing like filtering for example
    aInputRasterPath string --> Contain input raster path to process
    aOutputRasterPath string --> Contain output raster path to process
    aBlockSize integer --> Contain the size of square block to Tile
    aWindowSize integer --> Contain windows size for sliding window (0 size by default)
    '''
    # aWindowSize = aTempWindowSize

    # we put X and Y block size based on aBlockSize
    xBSize = aBlockSize
    yBSize = aBlockSize

    # We get vrt file list
    VRTFNameList = GetFilesFromVrt(aInputRaster)
    # Get acquisition time of each data
    TimeList = []
    for file in VRTFNameList:
        FileName = os.path.basename(os.path.splitext(file)[0])

        TimeList.append(getDayFromS1File(FileName))

    # Get sorted index by date
    SortIndexVRTTimeList = np.argsort(TimeList)



    # Get acquisition date
    # Get file time
    AcqDates = [getDateFromS1Raster(file) for file in VRTFNameList]

    # We open the raster
    src_ds = gdal.Open( aInputRaster )
    if src_ds is None:
        print 'Could not open ' + fn
        sys.exit(1)

    # We get number of row and cols
    rows = src_ds.RasterYSize
    cols = src_ds.RasterXSize

    NumBands = src_ds.RasterCount

    # Create input band pointer
    InputBandPointerList = []
    for iB, indexB in enumerate(SortIndexVRTTimeList):
        InputBandPointerList.append(src_ds.GetRasterBand(int(indexB) + 1))

    # We force Float32 to read data
    BandType = gdal.GDT_Float32

    # We get Projection from input raster
    InputGeoTransform = src_ds.GetGeoTransform()
    InputProjection = src_ds.GetProjection()

    # Output file
    ConfDeforestationFlagFile = aOutputList[0]
    ConfDeforestationMagniFile = aOutputList[1]
    ConfDeforestationChange = aOutputList[2]

    format = "GTiff"
    driver = gdal.GetDriverByName( format )

    # Deforestation file (1 band)
    dst_ds_Def = driver.Create(ConfDeforestationFlagFile, cols, rows, 1, BandType )
    # We define Output projection based on input file projection
    dst_ds_Def.SetGeoTransform(InputGeoTransform)
    dst_ds_Def.SetProjection(InputProjection)

    BandDef = dst_ds_Def.GetRasterBand(1)
    BandDef.SetNoDataValue(0)

    #  Deforestation prob file (Numdate band)
    dst_ds_MagDef = driver.Create(ConfDeforestationMagniFile, cols, rows, 1, BandType )
    # We define Output projection based on input file projection
    dst_ds_MagDef.SetGeoTransform(InputGeoTransform)
    dst_ds_MagDef.SetProjection(InputProjection)

    BandMagDef = dst_ds_MagDef.GetRasterBand(1)
    BandMagDef.SetNoDataValue(0)

    # Old Deforestation file (Numdate band)
    dst_ds_ConfDefChange = driver.Create(ConfDeforestationChange, cols, rows, 1, BandType )
    # We define Output projection based on input file projection
    dst_ds_ConfDefChange.SetGeoTransform(InputGeoTransform)
    dst_ds_ConfDefChange.SetProjection(InputProjection)

    BandConfDef = dst_ds_ConfDefChange.GetRasterBand(1)
    BandConfDef.SetNoDataValue(0)

    # print 'Rows, Cols: ',rows, cols
    BlockId = 0
    for i in range(0, rows, yBSize):
        if i + yBSize < rows:
            numRows = yBSize
        else:
            numRows = rows - i
        # Backup i and numRows
        numRowsDefault = numRows
        iOriginal = i
        for j in range(0, cols, xBSize):
            i = iOriginal
            numRows = numRowsDefault
            # print '\n Block numero : ', BlockId
            if j + xBSize < cols:
                numCols = xBSize
            else:
                numCols = cols - j
            numColsDefault = numCols

            # print 'Indice i,j original : ', i, j
            # print 'numRows numCols :',numRows, numCols
            # Test for applying sliding window buffer
            # Backup j
            jOriginal = j

            iOffset = 0
            jOffset = 0
            numColsOffset = 0
            numRowsOffset = 0
            if i - aWindowSize >= 0:
                i = i - aWindowSize
                iOffset = aWindowSize
            if j - aWindowSize >= 0:
                j = j - aWindowSize
                jOffset = aWindowSize

            if jOriginal + numCols + aWindowSize <= cols:
                numCols = numCols + aWindowSize
                numColsOffset = aWindowSize
            numCols = numCols + jOffset

            if iOriginal + numRows + aWindowSize <= rows:
                numRows = numRows + aWindowSize
                numRowsOffset = aWindowSize
            numRows = numRows + iOffset

            # print 'Read as array j, i, numCols, numRows', j, i, numCols, numRows
            Data = np.zeros(shape=(NumBands, numRows, numCols ),dtype=np.float)

            #We get file values and dates
            DataDates = []
            for iB, indexB in enumerate(SortIndexVRTTimeList):
                Data[iB, :,:] = InputBandPointerList[iB].ReadAsArray(j, i, numCols, numRows)
                DataDates.append(datetime.strptime(AcqDates[indexB], '%Y%m%d'))
            '''

            Do something like

            '''
            ConfirmedDeforestationFlag, ConfirmedDeforestationMagni, ConfirmedDeforestationChange = RadiometryAnalysisNew(Data, aCumDerivativeThreshold, aDerivativeThreshold)

            # Convert ConfirmedDeforestationFlag from num band to days where day 0 is the first one
            # Get date difference from first date
            DaysFromFirstDates = [(dayS - DataDates[0]).days for dayS in DataDates ]

            for item in range(NumBands):
                WhDef = ConfirmedDeforestationFlag == item +1
                if item == 0:
                    ConfirmedDeforestationFlag[WhDef] = AcqDates[item]
                else:
                    ConfirmedDeforestationFlag[WhDef] = AcqDates[item - 1]

            BandDef.WriteArray(ConfirmedDeforestationFlag,jOriginal,iOriginal)
            BandConfDef.WriteArray(ConfirmedDeforestationChange,jOriginal,iOriginal)
            BandMagDef.WriteArray(ConfirmedDeforestationMagni,jOriginal,iOriginal)

            BlockId = BlockId + 1

    # We close all file
    for band in range(len(InputBandPointerList)):
        InputBandPointerList[band] = None

    src_ds = None

    BandDef = None
    BandConfDef = None
    BandMagDef = None

    dst_ds_Def = None
    dst_ds_MagDef = None
    dst_ds_ConfDefChange = None


def TileRasterBuildForestMask(aInputRaster, aOutputRasterMask, aRefDate, aBlockSize,aWindowSize):
    '''
    TODO: Add sliding window parameter


    This method show how to process huge raster
    using easy tilling based on block size
    and if necessary taking into account window size
    when necessary in processing like filtering for example
    aInputRasterPath string --> Contain input raster path to process
    aOutputRasterPath string --> Contain output raster path to process
    aBlockSize integer --> Contain the size of square block to Tile
    aWindowSize integer --> Contain windows size for sliding window (0 size by default)
    '''
    # aWindowSize = aTempWindowSize

    # we put X and Y block size based on aBlockSize
    xBSize = aBlockSize
    yBSize = aBlockSize

    # We get vrt file list
    VRTFNameList = GetFilesFromVrt(aInputRaster)
    SortIndexVRTTimeList = np.argsort(VRTFNameList)

    # Get acquisition date
    # Get file time
    AcqDates = [getDateFromS1Raster(file) for file in VRTFNameList]

    IndexTime = AcqDates.index(aRefDate)
    IndexSorted = SortIndexVRTTimeList.tolist().index(IndexTime)

    BandsToUse = SortIndexVRTTimeList[IndexSorted - 1:IndexSorted+1]

    # We open the raster
    src_ds = gdal.Open( aInputRaster )
    if src_ds is None:
        print 'Could not open ' + fn
        sys.exit(1)

    # We get number of row and cols
    rows = src_ds.RasterYSize
    cols = src_ds.RasterXSize

    # We force Float32 to read data
    BandType = gdal.GDT_Float32

    # We get Projection from input raster
    InputGeoTransform = src_ds.GetGeoTransform()
    InputProjection = src_ds.GetProjection()

    format = "GTiff"
    driver = gdal.GetDriverByName( format )

    # Forest mask file (1 band)
    dst_ds_ForMask = driver.Create(aOutputRasterMask, cols, rows, 1, gdal.GDT_Byte )
    # We define Output projection based on input file projection
    dst_ds_ForMask.SetGeoTransform(InputGeoTransform)
    dst_ds_ForMask.SetProjection(InputProjection)

    # print 'Rows, Cols: ',rows, cols
    BlockId = 0
    for i in range(0, rows, yBSize):
        if i + yBSize < rows:
            numRows = yBSize
        else:
            numRows = rows - i
        # Backup i and numRows
        numRowsDefault = numRows
        iOriginal = i
        for j in range(0, cols, xBSize):
            i = iOriginal
            numRows = numRowsDefault
            # print '\n Block numero : ', BlockId
            if j + xBSize < cols:
                numCols = xBSize
            else:
                numCols = cols - j
            numColsDefault = numCols

            # print 'Indice i,j original : ', i, j
            # print 'numRows numCols :',numRows, numCols
            # Test for applying sliding window buffer
            # Backup j
            jOriginal = j

            iOffset = 0
            jOffset = 0
            numColsOffset = 0
            numRowsOffset = 0
            if i - aWindowSize >= 0:
                i = i - aWindowSize
                iOffset = aWindowSize
            if j - aWindowSize >= 0:
                j = j - aWindowSize
                jOffset = aWindowSize

            if jOriginal + numCols + aWindowSize <= cols:
                numCols = numCols + aWindowSize
                numColsOffset = aWindowSize
            numCols = numCols + jOffset

            if iOriginal + numRows + aWindowSize <= rows:
                numRows = numRows + aWindowSize
                numRowsOffset = aWindowSize
            numRows = numRows + iOffset

            # print 'Read as array j, i, numCols, numRows', j, i, numCols, numRows
            Data = np.zeros(shape=(2, numRows, numCols ),dtype=np.float)
            NoForestMask = np.zeros(shape=( numRows, numCols ),dtype=np.dtype('b'))

            #We get file values and dates

            DataDates = []

            for iB, indexB in enumerate(BandsToUse):
                GetBand = src_ds.GetRasterBand(int(indexB) + 1)
                Data[iB, :,:] = GetBand.ReadAsArray(j, i, numCols, numRows)
                DataDates.append(datetime.strptime(AcqDates[indexB], '%Y%m%d'))
            '''
            Do something like

            '''


            # Generate conditional probability from 2 dates to be more accurate
            CondProb = Data[0,:,:] * Data[1,:,:] / ( Data[0,:,:] * Data[1,:,:] + (1 - Data[0,:,:]) * (1- Data[1,:,:]) )

            # print Data[0,:,:]
            # print Data[1,:,:]
            # print CondProb


            # Create No forest mask
            NoForestMask[CondProb < 0.5] = 1

            BandForMask = dst_ds_ForMask.GetRasterBand(1)
            BandForMask.SetNoDataValue(0)
            BandForMask.WriteArray(NoForestMask,jOriginal,iOriginal)

            BlockId = BlockId + 1

    # We close all file
    src_ds = None
    dst_ds_ForMask = None


# Main Program
Input_Folder = Input_Folder
# Start_Analysis_Date = Start_Analysis_Date
# Classification_Reference_Date = Classification_Reference_Date
Output_Folder = Output_Folder



# create main tmp dir
# TmpDir = os.path.join(Output_Folder, "tMp")
# if not os.path.exists(TmpDir):
#     os.makedirs(TmpDir)

# Create OutputProbability Folder
# ProbDir = os.path.join(Output_Folder, "Derivatives")
# if not os.path.exists(ProbDir):
#     os.makedirs(ProbDir)

# Generate the file list to use
# List all tif files
# ListFiles = directory_liste_ext_files(Input_Folder, 'vrt')

AllTifFile = directory_liste_ext_files(Input_Folder, 'tif')
# AllCopolFile = [file for file in AllTifFile if ('_VV_' in file) or  ('_HH_' in file)]
ListFiles = [file for file in AllTifFile if ('_HV_' in file) or  ('_VH_' in file)]

# Sort Files
ListFiles.sort()

# Filter the file to take only dates that begin at Stard date
UpdateInputList = []
for file in ListFiles:
    dateFile = getDateFromS1Raster(file)
    if int(dateFile) >= int(Start_Analysis_Date):
        UpdateInputList.append(file)


# Get acquisition time of each data
TimeList = []
for file in UpdateInputList:
    FileName = os.path.basename(os.path.splitext(file)[0])

    TimeList.append(getDayFromS1File(FileName))

# Get sorted index by date
SortIndexFiles = np.argsort(TimeList)
UpdateInputListOrder = [UpdateInputList[index] for index in  SortIndexFiles ]

FileDates = [ getDateFromS1Raster(day.replace("\\","/")) for day in UpdateInputListOrder]

# Create vrt
InputDataVRT = os.path.join(Input_Folder, 'InputVRT_From_' + Start_Analysis_Date + '_To_'+ FileDates[-1] +'.vrt')
# progress.setInfo(InputDataVRT)
# print InputDataVRT
InputDataVRT = InputDataVRT.replace("\\","/")
# print InputDataVRT
GdalBuildVRT(UpdateInputListOrder, InputDataVRT,  0, 0)




# Create Output file names
Suffix = '_From_' + Start_Analysis_Date + '_To_'+ FileDates[-1] +'.tif'
ConfDeforestationFlagFile = os.path.join(Output_Folder, 'ConfirmedDeforestationDates' + Suffix)
ConfDeforestationMagniFile = os.path.join(Output_Folder, 'ConfirmedDeforestationFullMagnitude' + Suffix)
ConfDeforestationChange = os.path.join(Output_Folder, 'ConfirmedDeforestationChangeMag' + Suffix)

OutputListFileDeforestation = [ConfDeforestationFlagFile, ConfDeforestationMagniFile, ConfDeforestationChange]


BlockSize = int(np.sqrt(float(Ram) * np.power(1024,2) /(4. * 2. *(6. + 4. * float(len(UpdateInputListOrder))))))

# print 'BlockSize',BlockSize


TileRasterRadAnalysis(InputDataVRT, OutputListFileDeforestation, Cumulative_Derivatives_Threshold, Derivatives_Threshold, BlockSize,0)

# Apply Sieve to remove small pixel agregate
ConfDeforestationChangeSieved = os.path.join(Output_Folder, 'ConfirmedDeforestationDates_Sieved' + str(Minimum_Maping_Unit_Pixel) + 'Pix' + Suffix)
ApplyMMUToDeforestationMap(ConfDeforestationFlagFile, ConfDeforestationChangeSieved, Minimum_Maping_Unit_Pixel, Ram)
