##Raster-Vector=group
## Multi band Raster Zonal Stats=name
##Input_Raster=raster
##Input_Vector=vector


from qgis.analysis import QgsZonalStatistics
from qgis.core import *
import qgis.utils

import gdal, ogr
import numpy as np

# Input_Raster = '/home/onfi/TMP/Divers/PNKK/S2PNKB/s2clippansharpclip.tif'
# Input_Vector = '/home/onfi/TMP/Divers/PNKK/S2PNKB/roitestzonal.shp'

# Open Raster data
raster = gdal.Open(Input_Raster)
bands = raster.RasterCount
raster = None

#specify polygon shapefile vector
polygonLayer = QgsVectorLayer(Input_Vector, 'zonepolygons', "ogr") 

for i in range(bands):
	zoneStat = QgsZonalStatistics (polygonLayer, Input_Raster, 'b'+str(i+1)+'-', i+1, QgsZonalStatistics.Mean)
	zoneStat.calculateStatistics(None)