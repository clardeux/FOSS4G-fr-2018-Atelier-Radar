##Sentinel-1 IW Additional Batch Processing=group
##Temporal average=name
##Input_Data_Folder=folder
##Output_in_dB=boolean True
##Ram=number 256

'''
Auteur (Avril 2017)
Cedric Lardeux clardeux@gmail.com cedric.lardeux@onfinternational.com
Pierre-Louis Frison pierre-louis.frison@u-pem.fr
'''

import sys, shutil
import glob, os, zipfile, os.path
import string
import subprocess
from datetime import datetime, date, time
import numpy as np
from scipy.ndimage.filters import uniform_filter
from osgeo import gdal

# This script is dedicated to orthorectified Sentinel-1 data in GRD IW mode and dual pol (VV, VH or HH, VH)
# It's based on OTB 5.6

#~ Input_Data_Folder='/home/DataStorage/do/ortho/p88'
#~ Output_in_dB= True
#~ Ram= 30000

'''
INPUT
'''


def DelDirAndItsFiles(aInputDir):
	for the_file in os.listdir(aInputDir):
		DelFile = os.path.join(aInputDir, the_file)
		if os.path.exists(DelFile):
			os.remove(DelFile)
	os.removedirs(aInputDir)

def DelDirFiles(aInputDir):
	for the_file in os.listdir(aInputDir):
		DelFile = os.path.join(aInputDir, the_file)
		if os.path.exists(DelFile):
			os.remove(DelFile)
'''
This function list this immediate subdirectories in one given directory
'''
def get_immediate_subdirectories(a_dir):
    return [os.path.join(a_dir, name) for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

'''
This function list all file with a given file extention in one directory
'''
def directory_liste_ext_files(directory, filt_ext):
	list_file = []
	for root, dirnames, filenames in os.walk(directory):
		for filename in filenames:
			if filename.endswith((filt_ext)):
				list_file.append(os.path.join(root, filename))

	list_file.sort()
	
	return list_file



def TileRasterProcessingAverage(aInputRasterListPath, aOutputRaster, aBlockSize):
    '''
    TODO: Add sliding window parameter


    This method show how to process huge raster
    using easy tilling based on block size
    and if necessary taking into account window size
    when necessary in processing like filtering for example
    aInputRasterPath string --> Contain input raster path to process
    aOutputRasterPath string --> Contain output raster path to process
    aBlockSize integer --> Contain the size of square block to Tile
    aWindowSize integer --> Contain windows size for sliding window (0 size by default)
    '''
    aWindowSize = 0

    # we put X and Y block size based on aBlockSize
    xBSize = aBlockSize
    yBSize = aBlockSize


    NumBands = len(aInputRasterListPath)

    # We open one raster to get rows and cols
    src_ds = gdal.Open( aInputRasterListPath[0] )
    if src_ds is None:
        print 'Could not open ' + fn
        sys.exit(1)

    # We get number of row and cols
    rows = src_ds.RasterYSize
    cols = src_ds.RasterXSize

    print 'rows, cols', rows, cols

    # We force Float32 to read data
    BandType = gdal.GDT_Float32

    # We get Projection from input raster
    InputGeoTransform = src_ds.GetGeoTransform()
    InputProjection = src_ds.GetProjection()

    src_ds = None

    # Open input files
    GdalFilePointerList = []
    GdalOutputFilePointerList = []
    for i in range(NumBands):
        GdalFilePointerList.append(gdal.Open( aInputRasterListPath[i] ))

    # Output file
    format = "GTiff"
    driver = gdal.GetDriverByName( format )
    dst_raster = driver.Create(aOutputRaster, cols, rows, 1, BandType )
    dst_raster.SetGeoTransform(InputGeoTransform)
    dst_raster.SetProjection(InputProjection)

    # print 'Rows, Cols: ',rows, cols
    BlockId = 0
    for i in range(0, rows, yBSize):
        if i + yBSize < rows:
            numRows = yBSize
        else:
            numRows = rows - i
        # Backup i and numRows
        numRowsDefault = numRows
        iOriginal = i
        for j in range(0, cols, xBSize):
            i = iOriginal
            numRows = numRowsDefault
            print '\n Block numero : ', BlockId
            if j + xBSize < cols:
                numCols = xBSize
            else:
                numCols = cols - j
            numColsDefault = numCols

            # print 'Indice i,j original : ', i, j
            # print 'numRows numCols :',numRows, numCols
            # Test for applying sliding window buffer
            # Backup j
            jOriginal = j

            iOffset = 0
            jOffset = 0
            numColsOffset = 0
            numRowsOffset = 0
            if i - aWindowSize >= 0:
                i = i - aWindowSize
                iOffset = aWindowSize
            if j - aWindowSize >= 0:
                j = j - aWindowSize
                jOffset = aWindowSize

            if jOriginal + numCols + aWindowSize <= cols:
                numCols = numCols + aWindowSize
                numColsOffset = aWindowSize
            numCols = numCols + jOffset

            if iOriginal + numRows + aWindowSize <= rows:
                numRows = numRows + aWindowSize
                numRowsOffset = aWindowSize
            numRows = numRows + iOffset

            # print 'Read as array j, i, numCols, numRows', j, i, numCols, numRows
            Data = np.zeros(shape=(NumBands, numRows, numCols ),dtype=np.float)

            #We get file values
            for it in range(len(GdalFilePointerList)):
                GetBand = GdalFilePointerList[it].GetRasterBand(1)
                Data[it, :,:] = GetBand.ReadAsArray(j, i, numCols, numRows)
            '''

            Do something like

            '''
            # Temporal average
            Mean = np.mean(Data, axis=0)

            print 'np.shape(Data)',np.shape(Data)
            print 'np.shape(Mean)',np.shape(Mean)

            #Clip the border
            Mean = Mean[iOffset:iOffset + numRowsDefault,jOffset:jOffset + numColsDefault]

            print 'np.shape(Mean)',np.shape(Mean)

            #We writte Quegan filtered data
            band = dst_raster.GetRasterBand(1)
            band.SetNoDataValue(0)
            band.WriteArray(Mean,jOriginal,iOriginal)

            BlockId = BlockId + 1

    # We close all file
    src_ds = None
    dst_raster = None

def Int2dB(InputFile, Band, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += InputFile + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\"" + Band + "==0 ? 0 : 10 * log10(" +Band +")\""

	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

# OTB Create HVdB - HHdB
def DiffdB(aInputFileCopol, aInputFileCrosspol, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += aInputFileCopol + " "
	cmd += aInputFileCrosspol + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\" im2b1 - im1b1 \""

	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

# OTB ratio
def RatioDualPol(aInputFileCopol, aInputFileCrosspol, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += aInputFileCopol + " "
	cmd += aInputFileCrosspol + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\" im2b1 / im1b1 \""

	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def GdalBuildVRT(aInputListFile, aOutputFile,  aSRCNoData, aVRTNoData):
	cmd = "gdalbuildvrt "
	cmd += " -separate "
	cmd += " -srcnodata " + str(aSRCNoData)
	cmd += " -vrtnodata " + str(aVRTNoData)
	cmd += " " + aOutputFile
	for File in aInputListFile:
		cmd += " " + File
	
	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def GenerateDualPolColorcompositiondB(aCopolFile, aCrosspolFile):
	CopoldBFile = aCopolFile.replace('VV_', 'VVdB_')
	CrosspoldBFile = aCopolFile.replace('VV_', 'VHdB_')

	DiffFile = aCopolFile.replace('VV_', 'VHdB-VVdB_')

	OutputColorCompVRTFile = aCopolFile.replace('VV_',  'VVdB_VHdB_VHdB-VVdB_')
	OutputColorCompVRTFile = OutputColorCompVRTFile.replace('.tif',  '.vrt')

	Int2dB(aCopolFile, 'im1b1', CopoldBFile, Ram)
	Int2dB(aCrosspolFile, 'im1b1', CrosspoldBFile, Ram)
	DiffdB(CopoldBFile, CrosspoldBFile, DiffFile, Ram)

	# Del IntCopol
	if os.path.exists(aCopolFile):
			os.remove(aCopolFile)

	# Del IntCrosspol
	if os.path.exists(aCrosspolFile):
			os.remove(aCrosspolFile)

	# VRT color composition
	GdalBuildVRT([CopoldBFile, CrosspoldBFile, DiffFile], OutputColorCompVRTFile,  0, 0)


def GenerateDualPolColorcompositionInt(aCopolFile, aCrosspolFile):
	Ratio = aCopolFile.replace('VV_', 'VH-VV_')
	print Ratio

	OutputColorCompVRTFile = aCopolFile.replace('VV_',  'VV_VH_VH-VV_')
	OutputColorCompVRTFile = OutputColorCompVRTFile.replace('.tif',  '.vrt')

	print OutputColorCompVRTFile

	RatioDualPol(aCopolFile, aCrosspolFile, Ratio, Ram)

	GdalBuildVRT([aCopolFile, aCrosspolFile, Ratio], OutputColorCompVRTFile,  0, 0)

def getDayFromS1File(aFileName):
	aFileName = aFileName.split("/")[-1]

	pos=max([aFileName.find('_2015'),aFileName.find('_2016'),aFileName.find('_2017'),aFileName.find('_2018'),aFileName.find('_2019'),aFileName.find('_2020')])

	return aFileName[pos+1:pos+9]


def GenerateTemporalAverage(aInputFolder):
	AllTifFile = directory_liste_ext_files(aInputFolder, 'tif')
	AllTifFile = [ file for file in AllTifFile if 'Quegan' not in file]
	AllCopolFile = [file for file in AllTifFile if ('VV' in file) or  ('HH' in file)]
	AllCrosspolFile = [file for file in AllTifFile if ('HV' in file) or  ('VH' in file)]


	# We get acquisition date
	ListDates = [getDayFromS1File(RastPath) for RastPath in AllCopolFile ]
	UniqueOutputDates = list(set(ListDates))
	UniqueOutputDates.sort()

	NumDate = len(AllCopolFile)

	BlockSize = int(np.sqrt(Ram * np.power(1024,2) /(4. * 2. *(2. * NumDate + 1.))))

	print Ram, NumDate, BlockSize

	OutputSuffix = 'Temporal_Average_' + UniqueOutputDates[0] + '_' + UniqueOutputDates[-1] + '.tif'

	# Apply average to Copol
	OutputCopol = os.path.join(aInputFolder, 'VV_' + OutputSuffix)
	OTBAverage(AllCopolFile, OutputCopol, Ram)

	# Apply filtering to crosspol
	OutputCrosspol = os.path.join(aInputFolder, 'VH_' + OutputSuffix)
	OTBAverage(AllCrosspolFile, OutputCrosspol, Ram)

	# Generate ratio intensity or convert in dB and creat vrt for color composition
	# List all folders in Time Filtering forlder
	# Get full path of time filtering folder
	if Output_in_dB:
		GenerateDualPolColorcompositiondB(OutputCopol, OutputCrosspol)
	else:
		GenerateDualPolColorcompositionInt(OutputCopol, OutputCrosspol)

def OTBAverage(aInputRasterList, aOutputRaster, aRam):
    cmd = "otbcli_BandMath -il "
    for rast in aInputRasterList:
        cmd += rast + ' '
    cmd += " -ram " + str(aRam)
    cmd += " -out "
    cmd += aOutputRaster
    cmd += " -exp "
    cmd += "\"avg("
    for i in range(len(aInputRasterList)):
        cmd += 'im' + str(i+1) + 'b1,'
    cmd = cmd[:-1]
    cmd += ")\""

    print cmd
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]
'''
Program

1 - List all files
2 - split one list for vh one fo hh one for vv
3 - create the output subdirectories
4 - do the filtering (if lee, in tmp)

'''

# List all SENTINEL-1 sub directories
GenerateTemporalAverage(Input_Data_Folder)
