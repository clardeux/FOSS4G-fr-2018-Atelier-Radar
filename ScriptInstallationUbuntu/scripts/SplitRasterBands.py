##Raster=group
##Split Raster band in VRT format=Name
##Input_Raster_File=raster

from osgeo import gdal,ogr,osr
import os, os.path,  glob
import subprocess
import numpy as np
import pylab as pl


NoData = 0

def GdalVrtSplitBands(aInputRaster):
    InputRasterName = os.path.splitext(os.path.basename(aInputRaster))[0]
    InputRasterFolder = os.path.dirname(aInputRaster)

    ds=gdal.Open(aInputRaster)
    bands = ds.RasterCount
    ds = None

    OutListFiles = []
    for band in range(bands):
        OutFile = os.path.join(InputRasterFolder,InputRasterName + '_b'+str(band+1)+'.vrt')
        OutListFiles.append(OutFile)

        cmd = 'gdalbuildvrt '
        cmd += '-srcnodata 0 -vrtnodata 0 '
        cmd += " -b " + str(band + 1) + ' '
        cmd += OutFile + ' '
        cmd += aInputRaster + ' '

        # progress.setInfo(cmd)
        print cmd

        p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
        ret= p1.communicate()[1]

    return OutListFiles


# Split rasters bands
ListBands = GdalVrtSplitBands(Input_Raster_File)
