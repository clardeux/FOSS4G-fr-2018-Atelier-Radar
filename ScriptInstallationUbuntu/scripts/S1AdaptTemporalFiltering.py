# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 15:51:57 2017

@author: cedric
"""


'''
This script do temporal filtering over sentinel-1 data
'''

'''
IMPORT
'''
import os, shutil, sys
from datetime import datetime
import numpy as np
from inspect import getsourcefile
import time

from argparse import ArgumentParser


'''
This function join path always using unix sep
'''
def modOsJoinPath(alistToJoin):
    joinedPath = os.path.join(*alistToJoin).replace("\\","/")

    return joinedPath


'''
This function return the qgis script folder
'''
def getQGISProcessingScriptFolder():
    from qgis.core import QgsApplication
    QGISProcessingScriptFolder = os.path.dirname(QgsApplication.qgisSettingsDirPath())
    QGISProcessingScriptFolder = modOsJoinPath([QGISProcessingScriptFolder,
    'processing', 'scripts'])

    return QGISProcessingScriptFolder

'''
This function load the necessary libs in different way
if we are running script in qgis processing or not
'''
def AddS1LibToPath():
    # Boolean to know if in qgis
    import qgis.utils
    inqgis = qgis.utils.iface is not None

    if inqgis:
        QGISProcessingScriptFolder = getQGISProcessingScriptFolder()

        # Create the S1Lib lib folder path
        ScriptPath = modOsJoinPath([QGISProcessingScriptFolder, 'S1Lib'])
    else:
        LocalScriptFileDir = os.path.dirname(os.path.abspath((getsourcefile(lambda:0)))).replace("\\","/")
        # Create the S1Lib lib folder path
        ScriptPath = modOsJoinPath([LocalScriptFileDir, 'S1Lib'])

    # Add path to sys
    sys.path.insert(0,ScriptPath)



# Load OTB Libs
AddS1LibToPath()

from S1OwnLib import (get_immediate_subdirectories,
                          GetNewDatesComparingOrthoFolderAndTempFiltFolder,
                          ApplyLeePreFiltering,
                          GetInputOutputListFilesForTempFiltering,
                          GenerateDualPolColorcompositiondB,
                          GenerateDualPolColorcompositionInt,
                          TileTemporalFilteringRIOS,
                          TileTemporalFilteringRIOSV2,
                          )


def main():
	parser = ArgumentParser()
	
	parser.add_argument('-InDir',type=str, help="Give the Input dir path", dest='Input_Data_Folder')
	parser.add_argument('-WinTempFilt',type=int, default = 11, help="Give window size for temporal filtering", dest='Spatial_Window_Size_for_Temporal_Filter')
	parser.add_argument('-AdaptTempChangeThresch',type=float, default = 0.97, help="Give value for adaptative change temporal filtering, use 0 to have standard Quegan filtering", dest='Temporal_Change_Thresold')
	parser.add_argument('-ApplyLeePreFilt',type=str, default = 'False', help="If you want to apply Lee filtering beforore temporal one", dest='Apply_Lee_Pre_Filtering')
	parser.add_argument('-WinLeeFilt',type=int, default = 5, help="Give window size for Lee filtering", dest='Spatial_Window_Size_for_Lee_Filter')
	parser.add_argument('-ENLLeeFilt',type=int, default = 5, help="Give Equivalent Number of Look for Lee filtering", dest='Looks_Number_for_Lee_Filter')
	parser.add_argument('-OutputIndB',type=str, default = 'True', help="If you want output in dB and not intensities", dest='Output_in_dB')
	parser.add_argument('-OutDir',type=str, help="Give the Output dir path", dest='Output_Data_Folder')
	parser.add_argument('-Ram',type=int, default = 512, help="Give the available ram you want to allocate", dest='Ram')
	#~ parser.add_argument('S1AdaptTemporalFilteringPy.py',\
	#~ help="python /home/osboxes/Git/Sentinel-1_Processing/S1AdaptTemporalFilteringPy.py -InDir /home/osboxes/RemoteSensing/Data/S1/Out/Ortho/p47 -WinTempFilt 11 -AdaptTempChangeThresch 0.97 -ApplyLeePreFilt False -WinLeeFilt 5 -ENLLeeFilt 5 -OutputIndB True -OutDir  /home/osboxes/RemoteSensing/Data/S1/Out/TempFilt -Ram 1024")
	
	
	#~ parser.print_help()
	args = parser.parse_args()
	
	
	Input_Data_Folder = args.Input_Data_Folder
	#~ print Input_Data_Folder
	Spatial_Window_Size_for_Temporal_Filter = args.Spatial_Window_Size_for_Temporal_Filter
	#~ print Spatial_Window_Size_for_Temporal_Filter
	Temporal_Change_Thresold = args.Temporal_Change_Thresold
	#~ print Temporal_Change_Thresold
	Apply_Lee_Pre_Filtering = args.Apply_Lee_Pre_Filtering
	Spatial_Window_Size_for_Lee_Filter = args.Spatial_Window_Size_for_Lee_Filter
	#~ print Spatial_Window_Size_for_Lee_Filter
	Looks_Number_for_Lee_Filter = args.Looks_Number_for_Lee_Filter
	#~ print Looks_Number_for_Lee_Filter
	Output_in_dB = args.Output_in_dB
	#~ print Output_in_dB
	Output_Data_Folder = args.Output_Data_Folder
	#~ print Output_Data_Folder
	Ram= args.Ram
	#~ print Ram
	
	RunProcessing(Input_Data_Folder,Spatial_Window_Size_for_Temporal_Filter, Temporal_Change_Thresold, Apply_Lee_Pre_Filtering,\
		Spatial_Window_Size_for_Lee_Filter, Looks_Number_for_Lee_Filter, Output_in_dB,Output_Data_Folder, Ram )
	
	
def RunProcessing(Input_Data_Folder,Spatial_Window_Size_for_Temporal_Filter, Temporal_Change_Thresold, Apply_Lee_Pre_Filtering,\
		Spatial_Window_Size_for_Lee_Filter, Looks_Number_for_Lee_Filter, Output_in_dB,Output_Data_Folder, Ram ):
			
	print 'Apply_Lee_Pre_Filtering dans func', Apply_Lee_Pre_Filtering

	if Apply_Lee_Pre_Filtering == 'True':
		Apply_Lee_Pre_Filtering = True
	else:
		Apply_Lee_Pre_Filtering = False
		
	if Output_in_dB == 'True':
		Output_in_dB = True
	else:
		Output_in_dB = False
	
	'''
	THE PROGRAM ITSELF
	'''
	# Internal variable
	# Begin time
	tic = time.clock()
	
	
	'''
	Main step
	1 - Get new date to process
	2 - If enable apply Lee pre filtering
	3 - Apply temporel filtering
	4 - Generate color composition
	'''
	
	
	
	
	
	#1 - List all Input files
	# List of all tif files
	
	NewDates, BoolPrevFilt = GetNewDatesComparingOrthoFolderAndTempFiltFolder(Input_Data_Folder,
	                                                            Output_Data_Folder)
	
	
	# print 'new filtering ?', BoolPrevFilt, len(NewDates)
	# Create tmp dir
	# Get time
	TimeNow = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
	TmpDir = modOsJoinPath([Output_Data_Folder, 'tmp' + TimeNow])
	if not os.path.exists(TmpDir):
	    os.makedirs(TmpDir)
	
	
	# Create Temp Filtering Tmp Dir
	TmpDirTempFilt = modOsJoinPath([TmpDir, "TempFilt"])
	if not os.path.exists(TmpDirTempFilt):
	    os.makedirs(TmpDirTempFilt)
	
	
	
	# List all SENTINEL-1 sub directories
	AllSubFolders = get_immediate_subdirectories(Input_Data_Folder)
	# Filter to get only new dates
	SubFolders = []
	for NDate in NewDates:
	    for folder in AllSubFolders:
	        if NDate in folder:
	            SubFolders.append(folder)


	#2 - If enable apply Lee pre filtering
	if Apply_Lee_Pre_Filtering:
		ApplyLeePreFiltering(SubFolders, TmpDir, Spatial_Window_Size_for_Lee_Filter, \
	                         Looks_Number_for_Lee_Filter, Ram)
	                         
		# Update Input dir to be Tmp folder
		Input_Data_Folder = TmpDir
	
	
	#3 - Apply temporel filtering
	# Get input output list file to filter
	# Copol in one side and cross pol
	AllInCopolList,AllOutCopolList, AllInCrosspolList, AllOutCrosspolList, \
	CopolQueguanFileSumPart, CopolQueguanFileCountPart, CrosspolQueguanFileSumPart, CrosspolQueguanFileCountPart \
	= GetInputOutputListFilesForTempFiltering(Input_Data_Folder,Output_Data_Folder,\
	                                        NewDates,Output_in_dB, TmpDirTempFilt,\
	                                        Spatial_Window_Size_for_Temporal_Filter)
	
	
	# print 'LEN',len(AllInCopolList), len(AllOutCopolList)
	print 'Begin  CopolQueguanFileSumPart, CopolQueguanFileCountPart, CrosspolQueguanFileSumPart, CrosspolQueguanFileCountPart' ,CopolQueguanFileSumPart, CopolQueguanFileCountPart, CrosspolQueguanFileSumPart, CrosspolQueguanFileCountPart
	# Test if new date if not exit
	if len(AllInCopolList) == 0:
	    ExceptionMessage = 'No new dates to process'
	    # Delete Tmp dir
	    shutil.rmtree(TmpDir)
	
	    raise Exception(ExceptionMessage)
	
	
	
	# Apply the temporal filtering
	NumDate = len(AllInCopolList)
	# Estimate the size of the block to use based on user available ram parameter
	BlockSize = int(np.sqrt(float(Ram) * np.power(1024,2) /(16. * 2. *(16. * float(NumDate + 1.)))))
	# BlockSize = np.power(int(np.sqrt(BlockSize)),2)
	
	print BlockSize
	
	start = time.time()
	# Usion RIOS lib Ongoing DEV
	TileTemporalFilteringRIOSV2(AllInCopolList, AllOutCopolList,
	                            CopolQueguanFileSumPart, CopolQueguanFileCountPart,
	                            BlockSize,Spatial_Window_Size_for_Temporal_Filter,
	                            Temporal_Change_Thresold, Output_Data_Folder)
	
	# Usion RIOS lib Ongoing DEV
	TileTemporalFilteringRIOSV2(AllInCrosspolList, AllOutCrosspolList,
	                            CrosspolQueguanFileSumPart, CrosspolQueguanFileCountPart,
	                            BlockSize,Spatial_Window_Size_for_Temporal_Filter,
	                            Temporal_Change_Thresold, Output_Data_Folder)
	
	
	end = time.time()
	
	
	#4 - Generate color composition
	SubFolders = get_immediate_subdirectories(TmpDirTempFilt)
	if Output_in_dB:
	  GenerateDualPolColorcompositiondB(SubFolders, Output_Data_Folder, Ram)
	else:
	  GenerateDualPolColorcompositionInt(SubFolders, Output_Data_Folder, Ram)
	
	# Delete old Quegan File
	print 'END CopolQueguanFileSumPart, CopolQueguanFileCountPart, CrosspolQueguanFileSumPart, CrosspolQueguanFileCountPart' ,CopolQueguanFileSumPart, CopolQueguanFileCountPart, CrosspolQueguanFileSumPart, CrosspolQueguanFileCountPart
	if os.path.exists(CopolQueguanFileSumPart):
	  print 'file to remove CopolQueguanFileSumPart ', CopolQueguanFileSumPart
	  os.remove(CopolQueguanFileSumPart)
	
	if os.path.exists(CopolQueguanFileCountPart):
	  os.remove(CopolQueguanFileCountPart)
	
	if os.path.exists(CrosspolQueguanFileSumPart):
	  os.remove(CrosspolQueguanFileSumPart)
	
	if os.path.exists(CrosspolQueguanFileCountPart):
	  os.remove(CrosspolQueguanFileCountPart)
	
	# Delete Tmp dir
	shutil.rmtree(TmpDir)
	
	print 'Temps execution co et cross pol filtering', end - start
	
if __name__ == '__main__':
	main()
