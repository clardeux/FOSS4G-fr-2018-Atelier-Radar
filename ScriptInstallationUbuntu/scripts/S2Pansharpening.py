##Optical=group
##Sentinel-2 Pan-Sharpening=Name
##Input_Folder=Folder
##Output_EPSG=crs EPSG:4326
##Apply_To_20m_Bands=boolean True
##Apply_To_60m_Bands=boolean False
##No_Data_Value=number 0
##Ram=number 512
###Output_Raster=output raster

from osgeo import gdal,ogr,osr
import os, os.path,  glob
import subprocess
import numpy as np
import pylab as pl
from datetime import datetime, date, time

InputFolder = Input_Folder
NoData = No_Data_Value

def directory_liste_ext_files(directory, filt_ext):
	list_file = []
	for root, dirnames, filenames in os.walk(directory):
		for filename in filenames:
			if filename.endswith((filt_ext)):
				list_file.append(os.path.join(root, filename))

	list_file.sort()
	return list_file

def GdalClipRasterWithVector(aInputRaster, aInputVector, aOutputFile, aSRCNoData, aOutEPSG, aResampleMethod):
	cmd = "gdalwarp "
	cmd += " -t_srs " + str(aOutEPSG)
	cmd += " -q -multi -crop_to_cutline -cutline "
	cmd += aInputVector
	cmd += " -dstnodata " + str(aSRCNoData)
	cmd += " -r " + aResampleMethod +" "
	cmd += " -of GTiff " + aInputRaster + ' ' + aOutputFile

	# progress.setInfo(cmd)
	# print cmd

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def GdalWarp(aInputFile, aOutputRaster, aNodataVal, aOutEPSG, aResampleMethod, aOutputResolution ):
	cmd = "gdalwarp  -dstnodata "
	cmd += str(aNodataVal)
	cmd += " -tr " + str(aOutputResolution) + ' ' + str(aOutputResolution) + ' '
	cmd += " -srcnodata " + str(aNodataVal) + " "
	cmd += " -multi "
	cmd += " -r " + aResampleMethod +" "
	cmd += " -of GTiff "
	cmd += "-t_srs " + str(aOutEPSG) + " "
	cmd += aInputFile
	cmd += " "
	cmd += aOutputRaster

	# progress.setInfo(cmd)
    # print cmd

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def GdalVrtStacking(aListFile, aOutputRaster, aNodataVal, aSeparate):
    cmd = 'gdalbuildvrt '
    if aSeparate: cmd += '-separate '
    cmd += ' -resolution highest '
    cmd += ' -srcnodata ' + str(aNodataVal) + ' -vrtnodata '+ str(aNodataVal) + ' '
    cmd += '-input_file_list ' + aListFile + ' -overwrite '
    cmd += aOutputRaster

    # progress.setInfo(cmd)
    # print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

def OTBPanSharpening(aInputFilePan, aInputMS, aOutputFile, aRam):
	cmd = "otbcli_BundleToPerfectSensor "
	cmd += " -inp " + aInputFilePan + " "
	cmd += " -inxs " + aInputMS + " "
	cmd += " -ram " + str(aRam)
	cmd += " -out " + aOutputFile

	progress.setInfo(cmd)
	# print cmd

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def CreatePseudoPan(aInputList, aOutputPan, aRam):
	cmd = "otbcli_BandMath -il "
	for file in aInputList: cmd += file + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += aOutputPan
	cmd += " -exp "
	cmd += "\" im1b1 "
	for i in xrange(len(aInputList) - 1):
		cmd += "+ im" + str(i+2) + "b1 "
	cmd += "\""

	progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]


FileDir = os.path.dirname(Output_Raster)

TimeNow = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
TmpDir = os.path.join(FileDir, TimeNow)

# Create the output dir of QL file
if not os.path.exists(TmpDir):
	os.makedirs(TmpDir)

# Create band list name depending on user options
# If only 20m
if Apply_To_20m_Bands: BandListBaseName = ['B02', 'B03', 'B04','B05', 'B06', 'B07', 'B08','B8A', 'B11', 'B12']

# If only 60m
if Apply_To_60m_Bands: BandListBaseName = ['B01','B02', 'B03', 'B04', 'B08', 'B09', 'B10']

# If 20m and 60m
if Apply_To_20m_Bands and Apply_To_60m_Bands: BandListBaseName = ['B01', 'B02', 'B03', 'B04', 'B05', 'B06', 'B07', 'B08', 'B8A', 'B09', 'B10', 'B11', 'B12']

# Create suffix of output pansharped file
SuffixFileName = '_'.join(BandListBaseName)
SuffixFileName = SuffixFileName.replace("B","")
OutputFileName = Output_Raster

# Temporary output file
OutputListFiles = os.path.join(TmpDir, 'TemporaryListFiles.txt')
OutputMultiSpectralFile = os.path.join(TmpDir, 'Multispectral.vrt')
OutputPseudoPan = os.path.join(TmpDir, 'PseudoPanchromatic.tif')

Jp2Files = directory_liste_ext_files(InputFolder, '.jp2')

# Reproject
BandsOutReprojListFiles = []
for i in xrange(len(Jp2Files)):
	if not 'QI_DATA' in Jp2Files[i]:
		Name = os.path.splitext(os.path.basename(Jp2Files[i]))[0]
		OutPath = os.path.join(TmpDir, Name + '_Item-' + str(i) + '_Reproj.tif')
		BandsOutReprojListFiles.append(OutPath)
		GdalWarp(Jp2Files[i], OutPath, str(NoData), Output_EPSG, 'bilinear',10 )

BandsInListFiles = []
OutputVrtBandList = []

# Loop thru file bands to  mosaic
for i in xrange(len(BandListBaseName)):
	MyOutputTxt = open(OutputListFiles, "w")
	OutputVRTBand = os.path.join(TmpDir, BandListBaseName[i] + '.vrt')
	OutputVrtBandList.append(OutputVRTBand)
	BandsInListFiles.append([each for each in BandsOutReprojListFiles if BandListBaseName[i] in each])

	for file in BandsInListFiles[i]:
		 MyOutputTxt.write(file + '\n')
	MyOutputTxt.close()

	GdalVrtStacking(OutputListFiles, OutputVRTBand, str(NoData), False)

MyOutputTxt = open(OutputListFiles, "w")
for file in OutputVrtBandList:
	MyOutputTxt.write(file + '\n')
MyOutputTxt.close()

GdalVrtStacking(OutputListFiles, OutputMultiSpectralFile, str(NoData), True)

# If only 20m
if Apply_To_20m_Bands:
	PanInputBands = [OutputVrtBandList[0], OutputVrtBandList[1], OutputVrtBandList[2],OutputVrtBandList[6]]

# If only 60m
if Apply_To_60m_Bands:
	PanInputBands = [OutputVrtBandList[1], OutputVrtBandList[2], OutputVrtBandList[3],OutputVrtBandList[4]]

# If 20m and 60m
if Apply_To_20m_Bands and Apply_To_60m_Bands:
	PanInputBands = [OutputVrtBandList[1], OutputVrtBandList[2], OutputVrtBandList[3],OutputVrtBandList[7]]

CreatePseudoPan(PanInputBands, OutputPseudoPan, Ram)

OTBPanSharpening(OutputPseudoPan, OutputMultiSpectralFile, OutputFileName, Ram)

shutil.rmtree(TmpDir)