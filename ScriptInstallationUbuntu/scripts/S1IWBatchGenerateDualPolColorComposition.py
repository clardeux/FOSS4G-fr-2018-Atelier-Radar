##Sentinel-1 IW GRD Batch Processing=group
##Generate Dual Pol Color Composition=name
##Input_Data_Folder=folder
##Output_Data_Folder=folder
##Ram=number 256

'''
Auteur (Avril 2017)
Cedric Lardeux clardeux@gmail.com cedric.lardeux@onfinternational.com
Pierre-Louis Frison pierre-louis.frison@u-pem.fr
'''


import sys
import glob, os, zipfile, os.path
import string
import subprocess
from datetime import datetime, date, time

# This script is dedicated to orthorectified Sentinel-1 data in GRD IW mode and dual pol (VV, VH or HH, VH)
# It's based on OTB 5.6

# Input_Data_Folder='/media/onfi/CL/livre_QGIS/OutputLee'
# Output_Data_Folder='/media/onfi/CL/livre_QGIS/OutputColorComp'
# Ram=1000


'''
INPUT
'''


DataFolder = Input_Data_Folder
OutputFolder = Output_Data_Folder

def DelDirAndItsFiles(aInputDir):
	for the_file in os.listdir(aInputDir):
		DelFile = os.path.join(aInputDir, the_file)
		if os.path.exists(DelFile):
			os.remove(DelFile)
	os.removedirs(aInputDir)

def DelDirFiles(aInputDir):
	for the_file in os.listdir(aInputDir):
		DelFile = os.path.join(aInputDir, the_file)
		if os.path.exists(DelFile):
			os.remove(DelFile)
'''
This function list this immediate subdirectories in one given directory
'''
def get_immediate_subdirectories(a_dir):
    return [os.path.join(a_dir, name) for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

'''
This function list all file with a given file extention in one directory
'''
def directory_liste_ext_files(directory, filt_ext):
	list_file = []
	for root, dirnames, filenames in os.walk(directory):
		for filename in filenames:
			if filename.endswith((filt_ext)):
				list_file.append(os.path.join(root, filename))

	list_file.sort()
	return list_file

def Int2dB(InputFile, Band, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += InputFile + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\"" + Band + "==0 ? 0 : 10 * log10(" +Band +")\""

	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

# OTB Create HVdB - HHdB
def DiffdB(aInputFileCopol, aInputFileCrosspol, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += aInputFileCopol + " "
	cmd += aInputFileCrosspol + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\" im2b1 - im1b1 \""

	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def GdalBuildVRT(aInputListFile, aOutputFile,  aSRCNoData, aVRTNoData):
	cmd = "gdalbuildvrt "
	cmd += " -separate "
	cmd += " -srcnodata " + str(aSRCNoData)
	cmd += " -vrtnodata " + str(aVRTNoData)
	cmd += " " + aOutputFile
	for File in aInputListFile:
		cmd += " " + File
	
	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]
'''
Program
'''
# List all SENTINEL-1 sub directories
SubFolders = get_immediate_subdirectories(DataFolder)

# Loop thru different S1 data (folder)
for Folder in SubFolders:
	# List all tif and tiff files
	AllTifFile = directory_liste_ext_files(Folder, 'tif')

	InDirName = os.path.split(Folder)[1]

	# Create Output subfolder
	OutFolder = os.path.join(OutputFolder,InDirName)
	if not os.path.exists(OutFolder):
		os.makedirs(OutFolder)

	Dual = False
	if '1SDV' in Folder:
		CopolFile = [s for s in AllTifFile if "VV" in s][0]
		CrosspolFile = [s for s in AllTifFile if "VH" in s][0]
		Dual = True

	elif '1SDH' in Folder:
		CopolFile = [s for s in AllTifFile if "HH" in s][0]
		CrosspolFile = [s for s in AllTifFile if "HV" in s][0]
		Dual = True
	else:
		print 'Not dual pol data'

	if Dual:
		CopolFileName = os.path.basename(os.path.splitext(CopolFile)[0])
		CrosspolFileName = os.path.basename(os.path.splitext(CrosspolFile)[0])

		CopoldBFile = os.path.join(OutFolder,  CopolFileName + '_dB' +'.tif')
		CrosspoldBFile = os.path.join(OutFolder,  CrosspolFileName + '_dB' +'.tif')

		if '1SDV' in Folder:
			BaseNameFile = CopolFileName.replace('VV','')
			BaseNameFile = os.path.basename(os.path.splitext(BaseNameFile)[0])

			DiffFile = os.path.join(OutFolder,  BaseNameFile + '_VHdB-VVdB' +'.tif')

			OutputColorCompVRTFile = os.path.join(OutFolder,  BaseNameFile + 'VVdB_VHdB_HVdB-VVdB' +'.vrt')
		else:
			BaseNameFile = CopolFileName.replace('HH','')
			BaseNameFile = os.path.basename(os.path.splitext(BaseNameFile)[0])

			DiffFile = os.path.join(OutFolder,  BaseNameFile + 'HVdB-HHdB' +'.tif')

			OutputColorCompVRTFile = os.path.join(OutFolder,  BaseNameFile + 'HHdB_HVdB_HVdB-HHdB' +'.vrt')
		
		Int2dB(CopolFile, 'im1b1', CopoldBFile, Ram)
		Int2dB(CrosspolFile, 'im1b1', CrosspoldBFile, Ram)
		DiffdB(CopoldBFile, CrosspoldBFile, DiffFile, Ram)



		# VRT color composition
		GdalBuildVRT([CopoldBFile, CrosspoldBFile, DiffFile], OutputColorCompVRTFile,  0, 0)
