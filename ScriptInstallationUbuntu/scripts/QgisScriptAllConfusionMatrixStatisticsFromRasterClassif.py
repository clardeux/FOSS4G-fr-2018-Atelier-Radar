##Classification=group
##Build all accuracy from classification and vector reference file=name
##Input_Classification_File=raster
##Input_Reference_Vector_File=vector
##Vector_Class_Field=field Input_Reference_Vector_File
##Output_Csv=output file
##EPSG_If_Reprojection_Required=crs EPSG:3857
import csv
import sys, os, shutil
import numpy as np
import pdb 
from datetime import datetime, date, time
import subprocess
from osgeo import gdal
from osgeo import osr

# Input_Classification_File = '/home/onfi/TMP/Cop/caf16_mbam_lccs_dichotomous_lc_period_a_raster.tif'
# Input_Reference_Vector_File = '/home/onfi/TMP/Cop/caf16_mbam_lccs_dichotomous_lc_period_a_vector/lca_caf_16_blind_u017.shp'
# Vector_Class_Field = 'CId2000'

# EPSG = 'EPSG:102022'

# OutputCsv = '/home/onfi/TMP/2017_03_24_15_57_25/ConfusionCaf162000.csv'


OutputCsv = Output_Csv

EPSG = str(EPSG_If_Reprojection_Required)

Ram = 128

BlockSize = 1024

# OutputCsv = Output_Csv

FileDir = os.path.dirname(OutputCsv)
TimeNow = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
TmpDir = os.path.join(FileDir, TimeNow)

# Create the output dir of QL file
if not os.path.exists(TmpDir):
	os.makedirs(TmpDir)

TmpCsv = os.path.join(TmpDir, 'OTBConfusion.csv')

def GdalWarp(aInputFile, aOutputRaster, aOutEPSG ):
	cmd = "gdalwarp  "
	cmd += " -multi "
	cmd += " -of GTiff "
	cmd += "-t_srs " + str(aOutEPSG) + " "
	cmd += aInputFile
	cmd += " "
	cmd += aOutputRaster

	# progress.setInfo(cmd)
	print cmd

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def OTBComputeConfusionMatrix(aInputRasterClassification, aInputRefVector, aFieldName, aOutputCsv, aRam):
	cmd = "otbcli_ComputeConfusionMatrix -in "
	cmd += aInputRasterClassification + " "
	cmd += " -ram " + str(aRam)
	cmd += " -out "
	cmd += aOutputCsv
	cmd += " -ref vector "
	cmd += " -ref.vector.in " + aInputRefVector
	cmd += " -ref.vector.field  " + aFieldName

	# progress.setInfo(cmd)
	# print cmd

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]


def ReadOTBConfusionMatrixFile(aInputCsv):
	Matrix = []
	f = open(aInputCsv, 'rt')
	try:
	    reader = csv.reader(f)
	    i = 0
	    for row in reader:
	    	if i == 0:
	    		Label1 = row[0].split(':')[1]
	    		row[0] = Label1
	    		ClassRefLabel = [int(rOW) for rOW in row ]

	    		i = i+1
	    	elif i == 1:
	    		Label1 = row[0].split(':')[1]
	    		row[0] = Label1
	    		ClassMapLabel = [int(rOW) for rOW in row ]
	    		i = i+1
	    	else:
	        	Matrix.append([float(Num) for Num in row])
	finally:
		f.close()
	
	# Get all unique label
	LabelMerge = ClassRefLabel + ClassMapLabel
	AllLabel = set(LabelMerge)

	print '\nClassRefLabel',ClassRefLabel
	print '\nClassMapLabel',ClassMapLabel


	# Convert list of list to numpy array
	npMatrix = np.array(Matrix)

	print '\nnpMatrix\n',npMatrix

	# Test if Ref or Map label are missing
	# Test Ref label
	RefMatrix = np.zeros(shape=(len(AllLabel),len(ClassMapLabel)))
	for ind, lab in enumerate(AllLabel):
		if lab in ClassRefLabel:
			index = ClassRefLabel.index(lab)
			RefMatrix[ind,:] = npMatrix[index,:]

	# print '\nRefMatrix\n',RefMatrix

	# Test Map label
	MapMatrix = np.zeros(shape=(len(AllLabel),len(AllLabel)))
	for ind, lab in enumerate(AllLabel):
		if lab in ClassMapLabel:
			index = ClassMapLabel.index(lab)
			MapMatrix[:,ind] = RefMatrix[:,index]

	print '\nMapMatrix\n',MapMatrix


	RawErrorMatrix = np.transpose(MapMatrix)
	
	return AllLabel, RawErrorMatrix

def ComputeAreaPerclasses(aInputRaster, aBlockSize,aWindowSize, aEPSG):
	# On Ubuntu epsg 102022 seems to be missed so we use proj4 definition
	if aEPSG == 'EPSG:102022':
		aEPSG = '"+proj=aea +lat_1=20 +lat_2=-23 +lat_0=0 +lon_0=25 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs "'
	
	Label = []
	Count = []
	# we put X and Y block size based on aBlockSize
	xBSize = aBlockSize
	yBSize = aBlockSize

	# We open one raster to get rows and cols
	src_ds = gdal.Open( aInputRaster)
	if src_ds is None:
		print 'Could not open ' + fn
		sys.exit(1)

	# We get number of row and cols
	rows = src_ds.RasterYSize
	cols = src_ds.RasterXSize

	

	prj=src_ds.GetProjection()
	srs=osr.SpatialReference(wkt=prj)
	if   not 'degree' in prj:
		print 'Projected'
		# We get pixel size to compute pixel area
		geotransform = src_ds.GetGeoTransform()
	else:
		print 'NOT Projected'
		tmpinputfile = os.path.join(TmpDir, TimeNow + 'tmp.tif')
		GdalWarp(aInputRaster, tmpinputfile, aEPSG )

		src_dsreproj = gdal.Open( tmpinputfile)
		if src_dsreproj is None:
			print 'Could not open ' + fn
			sys.exit(1)
		# We get pixel size to compute pixel area
		geotransform = src_dsreproj.GetGeoTransform()

		src_dsreproj = None


	PixelArea = -geotransform[1] * geotransform[5]
	# print 'PixelArea',PixelArea

	BlockId = 0
	for i in range(0, rows, yBSize):
		if i + yBSize < rows:
			numRows = yBSize
		else:
			numRows = rows -i
		for j in range(0, cols, xBSize):
			if j + xBSize < cols:
				numCols = xBSize
			else:
				numCols = cols - j

			# do something with the data here, before
			# reading the next block
			GetBand = src_ds.GetRasterBand(1)
			Data = GetBand.ReadAsArray(j, i, numCols, numRows)

			unique, counts = np.unique(Data, return_counts=True)
			uniqueList = unique.tolist()
			countsList = counts.tolist()
			
			if BlockId == 0:
				Label = uniqueList
				Count = countsList
			else:
				for ind, lab in enumerate(uniqueList):
					# pdb.set_trace()
					if lab in Label:
						Count[Label.index(lab)] += countsList[ind]
					else:
						Label.append(lab)
						Count.append(countsList[ind])

			BlockId = BlockId + 1
    # We close all file
	src_ds = None

	return Label, [float(value) * PixelArea for value in Count]

def WritteCsvRawErrorMatrix(ClassLabel, RawErrorMatrix, OuputFile):
	# Convert to int
	RawErrorMatrix = RawErrorMatrix.astype(int)
	f = open(OuputFile, 'a')
	try:
	    writer = csv.writer(f)
	    Title = ['Row Error Matrix']
	    writer.writerow( Title )

	    Header = []
	    Header.append('Class')
	    for i in range(NumClass): Header.append(ClassLabel[i])
	    Header.append('Total')
	    writer.writerow( Header )

	    TotalMapPoint = RawErrorMatrix.sum(axis = 1)
	    for i in range(NumClass):
	    	row = []
	    	row.append(ClassLabel[i])
	    	for j in range(NumClass): row.append(RawErrorMatrix[i,j])
	    	row.append(TotalMapPoint[i])
	    	writer.writerow( row )

	    TotalRefPoint = RawErrorMatrix.sum(axis = 0)
	    Footer = []
	    Footer.append('Total')
	    for i in range(NumClass): Footer.append(TotalRefPoint[i])
	    writer.writerow( Footer )

	    writer.writerow( '\n' )
	finally:
	    f.close()


def WritteCsvNormErrorMatrix(ClassLabel, NormErrorMatrix, OuputFile):
	f = open(OuputFile, 'a')
	try:
	    writer = csv.writer(f)
	    Title = ['Normalized Error Matrix']
	    writer.writerow( Title )

	    Header = []
	    Header.append('Class')
	    for i in range(NumClass): Header.append(ClassLabel[i])
	    Header.append('Total')
	    writer.writerow( Header )

	    TotalMapPoint = NormErrorMatrix.sum(axis = 1)
	    for i in range(NumClass):
	    	row = []
	    	row.append(ClassLabel[i])
	    	for j in range(NumClass): row.append(round(NormErrorMatrix[i,j],4))
	    	row.append(round(TotalMapPoint[i],4))
	    	writer.writerow( row )

	    TotalRefPoint = NormErrorMatrix.sum(axis = 0)
	    Footer = []
	    Footer.append('Total')
	    for i in range(NumClass): Footer.append(round(TotalRefPoint[i],4))
	    writer.writerow( Footer )

	    writer.writerow( '\n' )
	finally:
	    f.close()

def WritteCsvOverallAccuracy(OverAcc, OverAccCI, OuputFile):
	f = open(OuputFile, 'a')
	try:
	    writer = csv.writer(f)

	    Header = ['Overall Accuracy (Percent)', 'Uncertainty']
	    writer.writerow( Header )
	    writer.writerow( (round(OverAcc * 100,2), round(OverAccCI * 100,2)))

	    writer.writerow( '\n' )
	finally:
	    f.close()

def WritteCsvClassesStats(ClassLabel, ProdAcc, ProdAccCI, UserAcc, UserAccCI,\
 ClassArea, ClassAreaAdj, ClassAreaAdjDelta, OuputFile):
	f = open(OuputFile, 'a')
	try:
	    writer = csv.writer(f)

	    Title = ['Classes detailed statistics, In percent for accuracy and Map unit for Area']
	    writer.writerow( Title )

	    Header = ['Class', 'Producer Accuracy','Producer Accuracy Uncertainty',\
	     'User Accuracy','User Accuracy Uncertainty',\
	     'Map Area', 'Adjusted Map Area','Adjusted Map Area Uncertainty']
	    writer.writerow( Header )

	    for i in range(NumClass):
	    	row = []
	    	row.append(ClassLabel[i])

	    	row.append(round(ProdAcc[i]*100,2))
	    	row.append(round(ProdAccCI[i]*100,2))

	    	row.append(round(UserAcc[i]*100,2))
	    	row.append(round(UserAccCI[i]*100,2))

	    	row.append(round(ClassArea[i],2))

	    	row.append(round(ClassAreaAdj[i],2))
	    	row.append(round(ClassAreaAdjDelta[i],2))

	    	writer.writerow( row )

	    writer.writerow( '\n' )
	finally:
	    f.close()

# PROGRAM
# 
eps = 1e-16
# Compute OTB Confusion Matrix
OTBComputeConfusionMatrix(Input_Classification_File, Input_Reference_Vector_File, Vector_Class_Field, TmpCsv, Ram)

# Read OTB Confusion matrix 
Label, RawErrorMatrix = ReadOTBConfusionMatrixFile(TmpCsv)
RawErrorMatrix = RawErrorMatrix + eps

Label = [ lab for lab in Label]
print 'Labelssss',Label

# Compute area of the classes
RasterLabel, NumPix = ComputeAreaPerclasses(Input_Classification_File, BlockSize,0,EPSG)
ClassArea = [0] * len(Label)



for ind, lab in enumerate(RasterLabel):
	if lab in Label:
		ClassArea[Label.index(lab)] = float(NumPix[ind])

ClassArea = np.asarray(ClassArea)
ClassArea = ClassArea + eps
NumClass = len(Label)

# Generate the statistics
# 

# INPUT - OUTPUT
Percentile = 1.96

# PROGRAM
if os.path.exists(OutputCsv):
	os.remove(OutputCsv)

AreaWeight = ClassArea / ClassArea.sum()

# Contain the total number of point for each class in the map
TotalMapPoints = RawErrorMatrix.sum(axis = 1)

# Contain normalized error matrix
NormErrorMatrix = np.zeros(shape = (NumClass,NumClass))
for i in range(NumClass): # Loop on Cols
	for j in range(NumClass):# Loop on Rows
		NormErrorMatrix[j,i] = RawErrorMatrix[j,i] / TotalMapPoints[j] * AreaWeight[j] 

# Contain the total proportion for each class from Normalized matrix
PropRef = NormErrorMatrix.sum(axis = 0)


# COMPUTE STATS Indicators
# Contain Producer accuracy for each class
ProdAcc = [ NormErrorMatrix[i,i] / PropRef[i] for i in range(NumClass) ]

# Contain User accuracy for each class
UserAcc = [ NormErrorMatrix[i,i] / AreaWeight[i] for i in range(NumClass) ]

# Contain Overall Accuracy
OverAcc = sum([ NormErrorMatrix[i,i] for i in range(NumClass) ])


# COMPUTE Confidence Interval CI on PA, UA and OA
# Contain Overall Accuracy Confidence interval for 95percent confidence
OverAccCI = Percentile * \
np.sqrt(sum([ NormErrorMatrix[i,i] * \
	(AreaWeight[i] - NormErrorMatrix[i,i]) / TotalMapPoints[i] \
	for i in range(NumClass) ]))

# Contain the total number of point for each class in the Reference
TotalRefPoints = RawErrorMatrix.sum(axis = 0)

# Contain User Accuracy Confidence interval for 95percent confidence
UserAccCI = [ Percentile * \
np.sqrt(NormErrorMatrix[i,i] * \
	(AreaWeight[i] - NormErrorMatrix[i,i]) / \
	(pow(AreaWeight[i],2) * TotalMapPoints[i])) \
	for i in range(NumClass) ]


ProdAccCI = []
for j in range(NumClass):
	CalcElem1 = NormErrorMatrix[j,j] * pow(PropRef[j],-4)

	RangeList = range(NumClass)
	RangeList.remove(j)
	CalcElem2 = NormErrorMatrix[j,j] *\
	(sum( [ NormErrorMatrix[i,j] * (AreaWeight[i] - NormErrorMatrix[i,j]) / TotalMapPoints[i]\
	  for i in RangeList ] ) )
	
	CalcElem3 = (AreaWeight[j] - NormErrorMatrix[j,j]) * pow(PropRef[j] - NormErrorMatrix[j,j] , 2) / TotalRefPoints[j]

	ProdAccCITmp = Percentile *	np.sqrt( CalcElem1 * ( CalcElem2 + CalcElem3))

	ProdAccCI.append(ProdAccCITmp)

# COMPUTE Confidence Interval CI on Area Estimate
ClassAreaCI = []
for j in range(NumClass):
	ClassAreaCITMP = np.sqrt(sum( [ (AreaWeight[i] * NormErrorMatrix[i,j] - pow(NormErrorMatrix[i,j] ,2) ) /( TotalMapPoints[i] -1 )  \
	  for i in range(NumClass) ] ) )

	ClassAreaCI.append(ClassAreaCITMP)

ClassAreaAdj = [ PropRef[i] * ClassArea.sum()  for i in range(NumClass)]

ClassAreaAdjDelta = [ 2 * ClassArea.sum() *ClassAreaCI[i] for i in range(NumClass)]

WritteCsvRawErrorMatrix(Label, RawErrorMatrix, OutputCsv)
WritteCsvNormErrorMatrix(Label, NormErrorMatrix, OutputCsv)
WritteCsvOverallAccuracy(OverAcc, OverAccCI, OutputCsv)

WritteCsvClassesStats(Label, ProdAcc, ProdAccCI, UserAcc, UserAccCI,\
 ClassArea, ClassAreaAdj, ClassAreaAdjDelta, OutputCsv)

shutil.rmtree(TmpDir)