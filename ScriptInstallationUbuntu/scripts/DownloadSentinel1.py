#! /usr/bin/env python
# -*- coding: iso-8859-1 -*-
import json
import time
import os, os.path, optparse,sys
from datetime import date

from osgeo import ogr
import shutil, subprocess
from datetime import datetime
import shlex
import logging


from subprocess import Popen, PIPE


def modOsJoinPath(alistToJoin):
    joinedPath = os.path.join(*alistToJoin).replace("\\","/")
    
    return joinedPath

'''
python peps_download.py  -c S1 --lonmin -57.92 --lonmax -57.15 --latmin -11.5 --latmax -11 -a peps.txt -d 2016-10-08 -f 2016-11-03 -p GRD -m IW -n FALSE --json outpeps.json
'''

def run_command(command):
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    while True:
        output = process.stdout.readline()
        if output == '' and process.poll() is not None:
            break
        if output:
            print output.strip()
            logging.info('Errore durring reprojection')
            logging.info(output.strip())
    rc = process.poll()
    return rc

def GetShapeExtent(aInputShape):
    # Get a Layer's Extent
    inDriver = ogr.GetDriverByName("ESRI Shapefile")
    inDataSource = inDriver.Open(aInputShape, 0)
    inLayer = inDataSource.GetLayer()
    extent = inLayer.GetExtent()

    return extent

def RunGetExtent(aInputShape):
    TmpShape = aInputShape.replace('.shp', TimeNow + 'TMP.shp')

    # Convert shape
    logging.info('Trying to reproject the input shape')
    EPSG = 4326
    ReprojVector(aInputShape, TmpShape, EPSG)

    # Get extent
    logging.info('Trying to get extent')
    Extent = GetShapeExtent(TmpShape)

    # Del tmp shape
    logging.info('Trying to delete temporary file')
    outDriver = ogr.GetDriverByName("ESRI Shapefile")
    if os.path.exists(TmpShape):
        outDriver.DeleteDataSource(TmpShape)

    return Extent


'''
This function call ogr in system command to reproject a vector
aEPSG is the epsg code like 4326
'''
def ReprojVector(aInputVector, aOutputVector, aEPSG):
    CmdList = [
        'ogr2ogr -overwrite',
        '-f \"ESRI Shapefile\" %s'     % aOutputVector,
        '%s' % aInputVector,
        '-t_srs EPSG:%s'     % str(aEPSG)
    ]

    run_command(' '.join(CmdList))

def RunPepsDownload():
    logging.info('Begin Peps Query')
    geom = 'rectangle'


    query_geom='box={lonmin},{latmin},{lonmax},{latmax}'.format(latmin=latmin,latmax=latmax,lonmin=lonmin,lonmax=lonmax)
    logging.info('Geom query OK')

    # date parameters of catalog request    
    if Start_Date!=None:    
        start_date=Start_Date
        if End_Date!=None:
            end_date=End_Date
        else:
            end_date=date.today().isoformat()

    logging.info('Dates query OK')
    if Sentinel_Sensor=='S2':
        if  Start_Date>= '2016-12-05':
            print "**** products after '2016-12-05' are stored in Tiled products collection"
            print "**** please use option -c S2ST"
            time.sleep(5)
        elif End_Date>= '2016-12-05':
            print "**** products after '2016-12-05' are stored in Tiled products collection"
            print "**** please use option -c S2ST to get the products after that date"
            print "**** products before that date will be downloaded"
            time.sleep(5)

    if Sentinel_Sensor=='S2ST':
        if  End_Date< '2016-12-05':
            print "**** products before '2016-12-05' are stored in non-tiled products collection"
            print "**** please use option -c S2"
            time.sleep(5)
        elif Start_Date< '2016-12-05':
            print "**** products before '2016-12-05' are stored in non-tiled products collection"
            print "**** please use option -c S2 to get the products before that date"
            print "**** products after that date will be downloaded"
            time.sleep(5)

    logging.info('Sensor OK')
    #====================
    # read authentification file
    #====================
    try:
        f=file(pepsAccesFile)
        (email,passwd)=f.readline().split(' ')
        if passwd.endswith('\n'):
            passwd=passwd[:-1]
        f.close()
        logging.info('Read Peps Acces file OK')
    except :
        print "error with password file"
        logging.info('Peps access error with password file')
        sys.exit(-2)



    if os.path.exists(Output_Catalog_File):
        os.remove(Output_Catalog_File)
        

     
    # search in catalog
    if (Product_Type=="") and (Sensor_Mode=="") :
        search_catalog='curl -k -o %s https://peps.cnes.fr/resto/api/collections/%s/search.json?%s\&startDate=%s\&completionDate=%s\&maxRecords=500'%(Output_Catalog_File,Sentinel_Sensor,query_geom,start_date,end_date)
        # search_catalog='curl -k -o %s https://peps.cnes.fr/resto/api/collections/%s/search.json?%s\&startDate=%s\&completionDate=%s'%(Output_Catalog_File,Sentinel_Sensor,query_geom,start_date,end_date)
    else :
        if Relative_Orbit == None:
            search_catalog='curl -k -o %s https://peps.cnes.fr/resto/api/collections/%s/search.json?%s\&startDate=%s\&completionDate=%s\&maxRecords=500\&productType=%s\&sensorMode=%s'%(Output_Catalog_File,Sentinel_Sensor,query_geom,start_date,end_date,Product_Type,Sensor_Mode)

        else:
            search_catalog='curl -k -o %s https://peps.cnes.fr/resto/api/collections/%s/search.json?%s\&startDate=%s\&completionDate=%s\&maxRecords=500\&productType=%s\&sensorMode=%s\&relativeOrbitNumber=%s'%(Output_Catalog_File,Sentinel_Sensor,query_geom,start_date,end_date,Product_Type,Sensor_Mode, Relative_Orbit)
        # search_catalog='curl -k -o %s https://peps.cnes.fr/resto/api/collections/%s/search.json?%s\&startDate=%s\&completionDate=%s\&productType=%s\&sensorMode=%s'%(Output_Catalog_File,Sentinel_Sensor,query_geom,start_date,end_date,Product_Type,Sensor_Mode)


    # print search_catalog
    logging.info('Search catalog OK')
    logging.info(search_catalog)

    os.system(search_catalog)
    time.sleep(5)

    logging.info('Search catalog Command OK')

    # Filter catalog result
    with open(Output_Catalog_File) as data_file:    
        data = json.load(data_file)

    if 'ErrorCode' in data :
        print data['ErrorMessage']
        sys.exit(-2)

    #Sort data
    download_dict={}
    storage_dict={}
    for i in range(len(data["features"])):
        polar = data["features"][i]["properties"]["polarisation"]
        # print 'polar polarisation', polar, Polarisation
        prod      =data["features"][i]["properties"]["productIdentifier"]
        feature_id=data["features"][i]["id"]
        storage   =data["features"][i]["properties"]["storage"]["mode"]
        platform  =data["features"][i]["properties"]["platform"]
        #recup du numero d'orbite
        orbitN=data["features"][i]["properties"]["orbitNumber"]
        if platform=='S1A':
        #calcul de l'orbite relative pour Sentinel 1A
            relativeOrbit=((orbitN-73)%175)+1
        elif platform=='S1B':
        #calcul de l'orbite relative pour Sentinel 1B
            relativeOrbit=((orbitN-27)%175)+1

        # print data["features"][i]["properties"]["productIdentifier"],data["features"][i]["id"],data["features"][i]["properties"]["startDate"],storage

        if Relative_Orbit!=None and polar == Polarisation:
            if platform.startswith('S2'):
                if prod.find("_R%03d"%Relative_Orbit)>0:
                    download_dict[prod]=feature_id
                    storage_dict[prod]=storage
            elif platform.startswith('S1'):
                if relativeOrbit==Relative_Orbit:
                    download_dict[prod]=feature_id
                    storage_dict[prod]=storage
        else:
            download_dict[prod]=feature_id
            storage_dict[prod]=storage


    logging.info('Filter on catalog OK')
    #====================
    # Download
    #====================


    if len(download_dict)==0:
        # print "No product matches the criteria"
        logging.info('No product matches the criteria')
    else:
        logging.info("\n Numbre of product to download : %s"%len(download_dict))

        # iterator to count the number of data to download (that does not exist
        CountDataToDownload = 0
        DownloadedZipList = []
        for prod in download_dict.keys():      
            file_exists= os.path.exists(("%s/%s.SAFE")%(Output_Directory,prod)) or  os.path.exists(("%s/%s.zip")%(Output_Directory,prod))
            tmticks=time.time()
            tmpfile=("%s/tmp_%s.tmp")%(Output_Directory,tmticks)
            # print "\nDownload of product : %s"%prod
            get_product='curl -o %s -k -u %s:%s https://peps.cnes.fr/resto/collections/%s/%s/download/?issuerId=peps'%(tmpfile,email,passwd,Sentinel_Sensor,download_dict[prod])
            # print get_product
            
            if (Download_Data and not(file_exists)):
                logging.info("\nDownload of product : %s"%prod)
                logging.info(get_product)
                CountDataToDownload = CountDataToDownload +1
                if storage_dict[prod]=="tape":
                    #downloading product from tape requires several attemps, waiting for the tape to be read
                    # print "\n***product is on tape, we'll have to wait a little"
                    logging.info('\n***product is on tape, we will have to wait a little')
                    for attempt in range(5):
                        # print "\t attempt", attempt+1
                        os.system(get_product)
                        if not os.path.exists(("%s/tmp_%s.tmp")%(Output_Directory,tmticks)):
                            
                            if attempt==4 :
                                # print "*********download timed out**********"
                                logging.info('*********download timed out**********')
                                sys.exit(-2)

                            time.sleep(180)
                        else:
                            break
                                
                else :
                    os.system(get_product)
                    #check if binary product
                with open(tmpfile) as f_tmp:
                    try:
                        tmp_data=json.load(f_tmp)
                        # print "Result is a text file (might come from a wrong password file)"
                        # print tmp_data
                        logging.info('Result is a text file (might come from a wrong password file)')
                        logging.info(tmp_data)

                        sys.exit(-1)
                    except ValueError:
                        pass
                
                os.rename("%s"%tmpfile,"%s/%s.zip"%(Output_Directory,prod))
                DownloadedZipList.append(modOsJoinPath([Output_Directory, prod, '.zip']))
                # print "product saved as : %s/%s.zip"%(Output_Directory,prod)
                logging.info("product saved as : %s/%s.zip"%(Output_Directory,prod))
            elif file_exists:
                # print "%s already exists"%prod
                logging.info("%s already exists"%prod)
            elif not(Download_Data):
                # print "no download (-n) option was chosen"
                logging.info("no download (-n) option was chosen")

    # Check the downloaded files
    NumberOfCorruptedZipFiles, NumberOfTmpFiles = CheckDownloadedZipFile(Output_Directory, DownloadedZipList)
    logging.info('Finnish')

    return CountDataToDownload, len(DownloadedZipList), NumberOfCorruptedZipFiles, NumberOfTmpFiles

'''
This function list all file with a given file extention in one directory
'''
def GetFileByExtensionFromDirectory(directory = '/Dir/To/Scan', filt_ext = 'abs.safe'):
    list_file = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in filenames:
            if filename.endswith((filt_ext)):
                list_file.append(modOsJoinPath([root, filename]))

    list_file.sort()
    return list_file

'''
This function join path always using unix sep
'''
def modOsJoinPath(alistToJoin):
    joinedPath = os.path.join(*alistToJoin).replace("\\","/")
    
    return joinedPath

'''
Scan the directory to list all zip file and all *.tmp files
it delete all *.tmp file and all corrupted zip
'''
def CheckDownloadedZipFile(aInputDirectory, aDownloadedZipList):
    import zipfile


    # Get *.tmp list files
    tmpFiles = GetFileByExtensionFromDirectory(aInputDirectory, '.tmp')
    NumberOfTmpFiles = len(tmpFiles)
    logging.info(str(len(tmpFiles)) + 'number of tmp file to delete')
    for tmpFile in tmpFiles:
        # print 'Tmp file to remove: ', tmpFile
        logging.info('Tmp file to remove: ' + tmpFile)
        os.remove(tmpFile)

    NumberOfCorruptedZipFiles = 0

    for zfile in aDownloadedZipList:
        try:
            archive = zipfile.ZipFile(zfile, 'r')
            if archive.testzip() is not None:
                # print 'Corrupted ZIP ', zfile
                logging.info('Corrupted ZIP ' + zfile)
                os.remove(zfile)
                NumberOfCorruptedZipFiles = NumberOfCorruptedZipFiles + 1
            else:
                # print 'Zip OK ', zfile
                logging.info(' ZIP OK ' + zfile)
            archive.close()
        except  zipfile.BadZipfile:
            # print "Not compliant ZIP file ", zfile
            logging.info('Bad ZIP ' + zfile)
            os.remove(zfile)
            NumberOfCorruptedZipFiles = NumberOfCorruptedZipFiles + 1

    logging.info('Wrong files, zip, tmp : ' + ' ' + str(NumberOfCorruptedZipFiles)+ ' ' + str(NumberOfTmpFiles))
    
    return NumberOfCorruptedZipFiles, NumberOfTmpFiles

'''
MAIN PROGRAM
'''
Sentinel_Sensor = 'S1' # or 'S2'
pepsAccesFile = '/home/DataStorage/Program/peps_download-master/peps.txt'
Product_Type = 'GRD' # GRD, SLC, OCN (for S1) | S2MSI1C (for S2)
Sensor_Mode = 'IW' # EW, IW , SM, WV (for S1) | INS-NOBS, INS-RAW (for S2)
Polarisation = 'VV VH' # VV VH or 
Relative_Orbit_To_Process = '120-47'
Start_Date = '2018-02-23'
End_Date = '2018-04-25'
Download_Data = True
Output_Directory = '/home/DataStorage/FrenchGuyana/Input'



InputShape = '/home/DataStorage/TestGuyane/FrenchGuyaneStudyAreaOrbit.shp'
TimeNow = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

LogFile =  modOsJoinPath([Output_Directory, 'LogFile-' + TimeNow + '.txt'])




logging.basicConfig(filename=LogFile,level=logging.INFO)


lonmin, lonmax, latmin, latmax = RunGetExtent(InputShape)


if Relative_Orbit_To_Process =='':
    Relative_Orbit = None
    Output_Catalog_File = modOsJoinPath([Output_Directory, 'Catalog-'+ TimeNow + '.json'])
    DataToDownload, DownloadedFile, NumberOfCorruptedZipFiles, NumberOfTmpFiles = RunPepsDownload()

    # Sum of errors
    DownloadedErrors = DataToDownload - DownloadedFile + NumberOfCorruptedZipFiles + NumberOfTmpFiles

    if DownloadedErrors != 0:
        DataToDownload, DownloadedFile, NumberOfCorruptedZipFiles, NumberOfTmpFiles = RunPepsDownload()
    
    logging.info('Downloading process Finnish')
    logging.info('Remaining errors, DataToDownload, DownloadedFile, NumberOfCorruptedZipFiles, NumberOfTmpFiles ' + str(DataToDownload)+ ' ' + str(DownloadedFile)+ ' ' + str(NumberOfCorruptedZipFiles)+ ' ' + str(NumberOfTmpFiles))
else:
    Relative_Orbit_List = Relative_Orbit_To_Process.split('-')
    Relative_Orbit_List = [int(path) for path in Relative_Orbit_List]

    for Orb in Relative_Orbit_List:
        Relative_Orbit = Orb
        Output_Catalog_File = modOsJoinPath([Output_Directory, 'Catalog-Path' + str(Orb) + '_' + TimeNow + '.json'])
        DataToDownload, DownloadedFile, NumberOfCorruptedZipFiles, NumberOfTmpFiles = RunPepsDownload()

        # Sum of errors
        DownloadedErrors = DataToDownload - DownloadedFile + NumberOfCorruptedZipFiles + NumberOfTmpFiles

        if DownloadedErrors != 0:
            DataToDownload, DownloadedFile, NumberOfCorruptedZipFiles, NumberOfTmpFiles = RunPepsDownload()
        
        logging.info('Downloading process Finnish')
        logging.info('Remaining errors, DataToDownload, DownloadedFile, NumberOfCorruptedZipFiles, NumberOfTmpFiles ' + str(DataToDownload)+ ' ' + str(DownloadedFile)+ ' ' + str(NumberOfCorruptedZipFiles)+ ' ' + str(NumberOfTmpFiles))


