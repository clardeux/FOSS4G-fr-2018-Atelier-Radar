# Authors: Cedric Lardeux et Pierre-Louis Frison


# Import LIB
import os, sys, subprocess
from rios import applier
from rios import cuiprogress
import numpy as np
from datetime import datetime
import time
import multiprocessing

from argparse import ArgumentParser

''' User parameters '''
# Input data
# InputFolder = '/home/cedric/Git/jupyter_notebook_test/Data/TempFilt'
# OutputFolder = '/home/cedric/Git/jupyter_notebook_test/Data/StatsTemp'

# Start_Analysis_Date = '20171113'
# End_Analysis_Date = '20180218'

# Ram = 200



# Stat list
# Full stats StatListToCompute = ['Median', 'Mean', 'Mad', 'Stdev', 'Min', 'Max', 'P10', 'P90', 'P25', 'P75' ]
# StatListToCompute = ['Median', 'Mean', 'Mad', 'Stdev', 'Min', 'Max', 'P10', 'P90', 'P25', 'P75' ]






''' Function '''
def main():
    parser = ArgumentParser()
    
    parser.add_argument('-InDir',type=str, help="Give the Input dir path", dest='Input_Data_Folder')
    parser.add_argument('-StartDate',type=str, help="Start date to compute stats", dest='Start_Analysis_Date')
    parser.add_argument('-EndDate',type=str, help="End date to compute stats", dest='End_Analysis_Date')
    parser.add_argument('-StatListToCompute',type=str, default = 'Median_Mean_Mad_Stdev_Min_Max_P10_P90_P25_P75', help="List of required stats", dest='StatListToCompute')
    parser.add_argument('-OutDir',type=str, help="Give the Output dir path", dest='Output_Data_Folder')
    parser.add_argument('-Ram',type=int, default = 512, help="Give the available ram you want to allocate", dest='Ram')
    #~ parser.add_argument('S1AdaptTemporalFilteringPy.py',\
    #~ help="python TemporalStats.py -InDir /home/cedric/Git/jupyter_notebook_test/Data/TempFilt -StartDate 20171113 -EndDate 20180218 -StatListToCompute Median_Mean_Mad_Stdev_Min_Max_P10_P90_P25_P75 -OutDir  /home/cedric/Git/jupyter_notebook_test/Data/StatsTemp -Ram 1024")
    
    
    #~ parser.print_help()
    args = parser.parse_args()
    
    
    Input_Data_Folder = args.Input_Data_Folder

    Start_Analysis_Date = args.Start_Analysis_Date

    End_Analysis_Date = args.End_Analysis_Date

    StatListToCompute = args.StatListToCompute

    Output_Data_Folder = args.Output_Data_Folder
    #~ print Output_Data_Folder
    Ram= args.Ram
    #~ print Ram
    
    RunProcessing(Input_Data_Folder, Output_Data_Folder, Start_Analysis_Date, End_Analysis_Date, Ram, StatListToCompute )




'''
This function list all file with a given file extention in one directory
'''
def directory_liste_ext_files(directory, filt_ext):
    list_file = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in filenames:
            if filename.endswith((filt_ext)):
                myfile = os.path.join(root, filename)
                list_file.append(myfile.replace("\\","/"))

    list_file.sort()
    return list_file    

def CreateFileList(aInputFolder, aExtension, aKeyWordList, aNotInKeywordList):
    AllTifFile = GetFileByExtensionFromDirectory(Folder, 'tif')
    CopolFile = [s for s in AllTifFile if "VV" in s][0]
    
'''
This function generate a vrt from input raster list
'''
def GdalBuildVRT(aInputListFile, aOutputFile,  aSRCNoData, aVRTNoData):
    TmpListfile = aOutputFile.replace('.vrt', 'TMPListFile.txt')
    fic=open(TmpListfile,'w')
    for file in aInputListFile:
        fic.write(file +'\n')
    fic.close()

    cmd = "gdalbuildvrt "
    cmd += " -separate -overwrite "
    cmd += " -srcnodata " + str(aSRCNoData)
    cmd += " -vrtnodata " + str(aVRTNoData)
    cmd += " " + aOutputFile
    cmd += " -input_file_list " + TmpListfile
    
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

    if os.path.exists(TmpListfile):
        os.remove(TmpListfile)

def getDateFromS1Raster(aFileName):
    aFileName = aFileName.split("/")[-1]
                  
    pos=max([aFileName.find('_2015'),aFileName.find('_2016'),aFileName.find('_2017'),aFileName.find('_2018'),aFileName.find('_2019'),aFileName.find('_2020')])
    return aFileName[pos+1:pos+9]

def GetListFile(aInputFolder, aPolar, aExt, aBegin_Date, aEnd_Date):
    AllTifFile = directory_liste_ext_files(aInputFolder, aExt)
                  
    # Sort Files
    AllTifFile.sort()

    # Filter the file to take only dates that begin at Stard date
    UpdateInputList = []
    for file in AllTifFile:
        dateFile = getDateFromS1Raster(file)
        if int(dateFile) >= int(aBegin_Date) and int(dateFile) <= int(aEnd_Date):
            UpdateInputList.append(file.replace("\\","/"))

    ListFile = [file.replace("\\","/") for file in UpdateInputList if (aPolar in file) ]
                
    return ListFile

'''
This function join path always using unix sep
'''
def modOsJoinPath(alistToJoin):
    joinedPath = os.path.join(*alistToJoin).replace("\\","/")
    
    return joinedPath

def mad(a, c=1.4826, axis=None):
    """
    Compute *Median Absolute Deviation* of an array along given axis.
    """

    # Median along given axis, but *keeping* the reduced axis so that
    # result can still broadcast against a.
    med = np.median(a, axis=axis, keepdims=True)
    mad = c * np.median(np.absolute(a - med), axis=axis)  # MAD along given axis

    return mad

def calcstats(info, inputs, outputs, otherargs):
    OutList = []

    for stat in otherargs.StatList:
        if 'Stdev' in stat:
            # Calc standard deviation of bands for each pixel
            MyStat = np.std(inputs.inimage, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))

        elif 'Mean'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = np.mean(inputs.inimage, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))
            
        elif 'Median'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = np.median(inputs.inimage, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))
            
        elif 'Mad'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = mad(inputs.inimage, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))
            
        elif 'Min'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = np.min(inputs.inimage, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))
            
        elif 'Max'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = np.max(inputs.inimage, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))
            
        elif 'P10'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = np.percentile(inputs.inimage, 10, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))
            
        elif 'P90'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = np.percentile(inputs.inimage, 90, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))
            
        elif 'P25'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = np.percentile(inputs.inimage, 25, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))
                        
        elif 'P75'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = np.percentile(inputs.inimage, 75,axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))

        else:
            print 'nothing to compute'

    outputs.imgs = OutList

def calcstatsMasked(info, inputs, outputs, otherargs):
    OutList = []

    mdata = np.ma.masked_where(inputs.inimage == 0, inputs.inimage)
    for stat in otherargs.StatList:
        if 'Stdev' in stat:
            # Calc standard deviation of bands for each pixel
            MyStat = np.ma.std(mdata, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))

        elif 'Mean'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = np.ma.mean(mdata, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))
            
        elif 'Median'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = np.ma.median(mdata, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))
            
        elif 'Mad'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = mad(mdata, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))
            
        elif 'Min'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = np.ma.min(mdata, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))
            
        elif 'Max'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = np.ma.max(mdata, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))
            
        elif 'P10'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = np.percentile(mdata, 10, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))
            
        elif 'P90'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = np.percentile(mdata, 90, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))
            
        elif 'P25'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = np.percentile(mdata, 25, axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))
                        
        elif 'P75'  in stat:
            # Calc Mean of bands for each pixel
            MyStat = np.percentile(mdata, 75,axis=0)
            OutList.append(MyStat.reshape((1,MyStat.shape[0],MyStat.shape[1])))

        else:
            print 'nothing to compute'

    outputs.imgs = OutList

''' Program itself '''
def RunProcessing(InputFolder, OutputFolder, Start_Analysis_Date, End_Analysis_Date, Ram, StatListToCompute ):
    StatListToCompute = StatListToCompute.split('_')
    if len(StatListToCompute) < 1:
        StatListToCompute = ['Median', 'Mean', 'Mad', 'Stdev', 'Min', 'Max', 'P10', 'P90', 'P25', 'P75']


    ''' Internal settings '''
    # Number of thread
    NumThreads = 4

    # Control that there is enough cpu
    ComputerThread = multiprocessing.cpu_count()
    if NumThreads > ComputerThread:
        NumThreads = ComputerThread

    # Nodata value
    NoData = 0

    MaxBlock = 512

    # Begin time
    start = time.time()

    # Polar VH
    Polar = '_VH_'
    # Get File list to use for stats
    ListFile = GetListFile(InputFolder, Polar, 'tif', Start_Analysis_Date, End_Analysis_Date)

    # NumDate
    NumDate = len(ListFile)
    # Block size
    BlockSize = int(np.sqrt(float(Ram) * np.power(1024,2) /(4. * 2. *(float(NumDate) + len(StatListToCompute))) / np.power(NumThreads,2)))


    if BlockSize > MaxBlock:
        BlockSize = MaxBlock
    else:
        BlockSize = np.power(2,int(np.log2(BlockSize)))

    # Create tmp vrt for process
    # TmpOutputpath
    TimeNow = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    TmpVrt = modOsJoinPath([OutputFolder, TimeNow + '.vrt'])

    GdalBuildVRT(ListFile, TmpVrt,  NoData, NoData)

    # Define Output Files
    OutputRasterStatListPath = []
    Suffix = '_From_' + Start_Analysis_Date + '_To_'+ End_Analysis_Date
    for name in StatListToCompute:
        OutputRasterStatListPath.append(os.path.join(OutputFolder,name + '_' + str(Polar) + Suffix + '.tif').replace("\\","/"))
        OutputVrt = os.path.join(OutputFolder, '-'.join(StatListToCompute) + '_' + str(Polar) + Suffix + '.vrt').replace("\\","/")

    # Process
    infiles = applier.FilenameAssociations()
    infiles.inimage = TmpVrt

    outfiles = applier.FilenameAssociations()
    outfiles.imgs = OutputRasterStatListPath

    # Set up options for output file
    controls = applier.ApplierControls()

    # Output format
    controls.setOutputDriverName("GTiff")
    controls.setCreationOptions(["COMPRESS=DEFLATE",  'PREDICTOR=3', 'ZLEVEL=9', 'BIGTIFF=YES' , 'TILED=YES'])
    # controls.setOmitPyramids(True)
    controls.setCalcStats(False)

    # Multithread
    controls.setNumThreads(NumThreads)
    controls.setJobManagerType('multiprocessing')

    # Define other args to use
    otherargs = applier.OtherInputs()

    otherargs.StatList = StatListToCompute

    # Block size
    controls.setWindowXsize(BlockSize)
    controls.setWindowYsize(BlockSize)

    # enable progress bar
    # controls.progress = cuiprogress.CUIProgressBar()

    applier.apply(calcstats, infiles, outfiles,otherargs, controls=controls)

    # END time
    end = time.time()
    print 'Num Thread ', NumThreads
    print ' Block ', BlockSize
    print 'Temps execution co et cross pol filtering', end - start

    # Build vrt to stack stats
    GdalBuildVRT(OutputRasterStatListPath, OutputVrt,  0, 0)

    # Delete tmp vrt
    if os.path.exists(TmpVrt):
        os.remove(TmpVrt)



    '''VV '''
    # Polar VV
    Polar = '_VV_'
    # Get File list to use for stats
    ListFile = GetListFile(InputFolder, Polar, 'tif', Start_Analysis_Date, End_Analysis_Date)

    # NumDate
    NumDate = len(ListFile)
    # Block size
    BlockSize = int(np.sqrt(float(Ram) * np.power(1024,2) /(4. * 2. *(float(NumDate) + len(StatListToCompute))) / np.power(NumThreads,2)))


    if BlockSize > MaxBlock:
        BlockSize = MaxBlock
    else:
        BlockSize = np.power(2,int(np.log2(BlockSize)))

    # Create tmp vrt for process
    # TmpOutputpath
    TimeNow = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    TmpVrt = modOsJoinPath([OutputFolder, TimeNow + '.vrt'])

    GdalBuildVRT(ListFile, TmpVrt,  NoData, NoData)

    # Define Output Files
    OutputRasterStatListPath = []
    Suffix = '_From_' + Start_Analysis_Date + '_To_'+ End_Analysis_Date
    for name in StatListToCompute:
        OutputRasterStatListPath.append(os.path.join(OutputFolder,name + '_' + str(Polar) + Suffix + '.tif').replace("\\","/"))
        OutputVrt = os.path.join(OutputFolder, '-'.join(StatListToCompute) + '_' + str(Polar) + Suffix + '.vrt').replace("\\","/")

    # Process
    infiles = applier.FilenameAssociations()
    infiles.inimage = TmpVrt

    outfiles = applier.FilenameAssociations()
    outfiles.imgs = OutputRasterStatListPath

    # Set up options for output file
    controls = applier.ApplierControls()

    # Output format
    controls.setOutputDriverName("GTiff")
    controls.setCreationOptions(["COMPRESS=DEFLATE",  'PREDICTOR=3', 'ZLEVEL=9', 'BIGTIFF=YES' , 'TILED=YES'])
    # controls.setOmitPyramids(True)
    controls.setCalcStats(False)

    # Multithread
    controls.setNumThreads(NumThreads)
    controls.setJobManagerType('multiprocessing')

    # Define other args to use
    otherargs = applier.OtherInputs()

    otherargs.StatList = StatListToCompute

    # Block size
    controls.setWindowXsize(BlockSize)
    controls.setWindowYsize(BlockSize)

    # enable progress bar
    # controls.progress = cuiprogress.CUIProgressBar()

    applier.apply(calcstats, infiles, outfiles,otherargs, controls=controls)

    # END time
    end = time.time()
    print 'Num Thread ', NumThreads
    print ' Block ', BlockSize
    print 'Temps execution co et cross pol filtering', end - start

    # Build vrt to stack stats
    GdalBuildVRT(OutputRasterStatListPath, OutputVrt,  0, 0)

    # Delete tmp vrt
    if os.path.exists(TmpVrt):
        os.remove(TmpVrt)


if __name__ == '__main__':
    main()