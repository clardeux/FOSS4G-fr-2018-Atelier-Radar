# QGIS Header
##Sentinel-1 IW GRD Batch Processing=group
##Time Series Statistics=name
##Input_Data_Folder=folder
##Start_Analysis_Date=string 20160124
##End_Analysis_Date=string 20161123
##*ParameterBoolean|Median|Median|True|True
##*ParameterBoolean|Mean|Mean|True|True
##*ParameterBoolean|Mad|Median Absolute Deviation|True|True
##*ParameterBoolean|Stdev|Standard deviation|True|True
##*ParameterBoolean|Min|Minimum|True|True
##*ParameterBoolean|Max|Maximum|True|True
##*ParameterBoolean|P10|10th percentile|True|True
##*ParameterBoolean|P90|90th percentile|True|True
##*ParameterBoolean|P25|25th percentile|True|True
##*ParameterBoolean|P75|75th percentile|True|True
##Output_Data_Folder=folder
##Ram=number 512

# Authors: Cedric Lardeux et Pierre-Louis Frison


StatListToCompute = []

if Median:
	StatListToCompute.append('Median')

if Mean:
	StatListToCompute.append('Mean')

if Mad:
	StatListToCompute.append('Mad')

if Stdev:
	StatListToCompute.append('Stdev')

if Min:
	StatListToCompute.append('Min')

if Max:
	StatListToCompute.append('Max')

if P10:
	StatListToCompute.append('P10')

if P90:
	StatListToCompute.append('P90')

if P25:
	StatListToCompute.append('P25')

if P75:
	StatListToCompute.append('P75')

StatListToCompute = '_'.join(StatListToCompute)

# Import LIB
import os, sys, glob, re, subprocess
from qgis.core import QgsApplication

QgisSettingPath = QgsApplication.qgisSettingsDirPath()
ScriptPath = os.path.join(QgisSettingPath, 'processing/scripts/TemporalStats.py')
ScriptPath = ScriptPath.replace("\\","/")


cmd = "python "
cmd += ScriptPath
cmd += ' -InDir ' + Input_Data_Folder
cmd += ' -StartDate ' + str(Start_Analysis_Date)
cmd += ' -EndDate ' + str(End_Analysis_Date)
cmd += ' -StatListToCompute ' + StatListToCompute
cmd += '  -OutDir ' + Output_Data_Folder
cmd += ' -Ram ' + str(Ram)

p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
ret= p1.communicate()[1]