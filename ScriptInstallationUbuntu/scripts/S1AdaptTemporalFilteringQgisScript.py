##Sentinel-1 IW GRD Batch Processing=group
##2 - Adaptative Temporal Filter=name
##Input_Data_Folder=folder
##ParameterNumber|Spatial_Window_Size_for_Temporal_Filter|Window size for temporal filtering|3|21|11|False
##ParameterNumber|Temporal_Change_Thresold|Change threshold for temporal filtering. 0 for Quegan equivalent|0.|1.|0.97|False
##*ParameterBoolean|Apply_Lee_Pre_Filtering|Apply Lee pre filtering|False|False
##*ParameterNumber|Spatial_Window_Size_for_Lee_Filter|Window size for Lee filtering|3|21|5|False
##*ParameterNumber|Looks_Number_for_Lee_Filter|Number of look to use for lee filtering|1|100|5|False
##OutputDirectory|Output_Data_Folder|Set the directory where the the files will be saved
##*ParameterBoolean|Output_in_dB|Output in dB|True|False
##Ram=number 512



'''
Output_in_dB=boolean True
'''
import os, sys, glob, re, subprocess
from qgis.core import QgsApplication

QgisSettingPath = QgsApplication.qgisSettingsDirPath()
ScriptPath = os.path.join(QgisSettingPath, 'processing/scripts/S1AdaptTemporalFiltering.py')
ScriptPath = ScriptPath.replace("\\","/")

cmd = "python "
cmd += ScriptPath
cmd += ' -InDir ' + Input_Data_Folder
cmd += ' -WinTempFilt ' + str(Spatial_Window_Size_for_Temporal_Filter)
cmd += ' -AdaptTempChangeThresch ' + str(Temporal_Change_Thresold)
cmd += ' -ApplyLeePreFilt ' + str(Apply_Lee_Pre_Filtering)
cmd += ' -WinLeeFilt ' + str(Spatial_Window_Size_for_Lee_Filter)
cmd += '  -ENLLeeFilt ' + str(Looks_Number_for_Lee_Filter)
cmd += ' -OutputIndB ' + str(Output_in_dB)
cmd += '  -OutDir ' + Output_Data_Folder
cmd += ' -Ram ' + str(Ram)


p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
ret= p1.communicate()[1]
