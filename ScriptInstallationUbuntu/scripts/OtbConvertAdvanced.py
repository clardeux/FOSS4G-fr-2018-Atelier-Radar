##Raster=group
##Convert raster to any format specifying output type=name
##Ram=number 128
##Input_Raster=file
##Output_Type_uint8_uint16_float=string uint8
##Output_Raster=output raster

import os, subprocess


cmd = "otbcli_Convert"
cmd += " -ram " + str(Ram)
cmd +=  " -in " + str(Input_Raster)
cmd += " -out " + str(Output_Raster)
cmd += " " + Output_Type_uint8_uint16_float


#-in QB_Toulouse_Ortho_XS.tif -out otbConvertWithScalingOutput.png uint8


p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
ret= p1.communicate()[1]