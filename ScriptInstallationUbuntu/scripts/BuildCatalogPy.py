import os, sys, glob, re, subprocess, pdb
from osgeo import gdal, ogr, osr
from datetime import datetime, date, time
import datetime as dT

# Dossier_Contenant_Les_Donnees_Satellite = 'E:\onfi\CL\ONFGuyane\SampleDatasetCatalog\ONFGArb'
# Nom_Du_Catalogue = 'Catalogue'

# InputDirectory = str(Dossier_Contenant_Les_Donnees_Satellite)
# InputDirectory = os.path.dirname(Dossier_Contenant_Les_Donnees_Satellite)
# InputDirectory = os.path.join(InputDirectory, os.sep)


# InputDirectory = 'E:\onfi\CL\ONFGuyane\SampleDatasetCatalog\ONFGArb'
# Fichier_Catalogue = 'E:\onfi\CL\ONFGuyane\SampleDatasetCatalog\ONFGArb\Catalogue.shp'
# InputDirectory = str(InputDirectory)
# Nom_Du_Catalogue = str(Nom_Du_Catalogue)
# Fichier_Catalogue = str(Fichier_Catalogue)


# progress.setInfo('\nInputDirectory ' + InputDirectory)
# progress.setInfo('\nNom_Du_Catalogue ' + Nom_Du_Catalogue)

'''
ToDo
- Filtre capteur
	- Landsat 4/5/7/8
	- Spot 4/5/6
	- Sentinel-2
	- Sentinel-1
- Champ Chemin fichier source
- Champ Chemin QuickLook (tif)
- Champ Chemin preview (tif)
- Champ capteur
'''
def main():
	print len(sys.argv)
	if (len(sys.argv) != 3):
		print("Usage : python BuildCatalogPy.py ", "FullPathDataDirectory", " CatalogueName")
		print("Example : python BuildCatalogBetaV8.py ", "d:\MesDonnees", " Catalogue")

		exit()

	# Run
	InputDirectory=sys.argv[1]
	Nom_Du_Catalogue=sys.argv[2]
	OutputShapeFile = os.path.join(InputDirectory, Nom_Du_Catalogue + ".shp")

	GenerateCatalogue(InputDirectory, OutputShapeFile)

'''
Function
'''
def DelDirAndItsFiles(aInputDir):
	for the_file in os.listdir(aInputDir):
		DelFile = os.path.join(aInputDir, the_file)
		if os.path.exists(DelFile):
			os.remove(DelFile)
	os.removedirs(aInputDir)

def DelDirFiles(aInputDir):
	for the_file in os.listdir(aInputDir):
		DelFile = os.path.join(aInputDir, the_file)
		if os.path.exists(DelFile):
			os.remove(DelFile)

def directory_liste_ext_files(directory, filt_ext):
    list_file = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in filenames:
            if filename.lower().endswith((filt_ext)):
                list_file.append(os.path.join(root, filename))

    list_file.sort()
    return list_file

def DissolveAllFeatures(aInputFile, aOutputFile):
	# ogr2ogr output.shp input.shp -dialect sqlite -sql "SELECT ST_Union(geometry) AS geometry FROM input"

	InputFileName = os.path.basename(os.path.splitext(aInputFile)[0])

	cmd = "ogr2ogr  "
	cmd += aOutputFile + " "
	cmd += aInputFile + " "
	cmd += " -dialect sqlite -sql \"SELECT ST_Union(geometry) AS geometry FROM "
	cmd += InputFileName + "\""

	# progress.setInfo('\nDissolveAllFeatures ' + cmd)
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def ConvertMLToMask(aInputFile, aOutputFile, aNodata):
	# Multilook
	cmd = "gdal_translate  "
	cmd += " -b mask,1 -of GTiff "
	cmd += "-a_nodata " + str(aNodata) + " "
	cmd += aInputFile + " "
	cmd += aOutputFile

	# progress.setInfo('\nConvertMLToMask ' + cmd)
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def ReprojToVRT(aInputFile, aOutputFile, aNodata, aFactor):
	src_ds = gdal.Open( aInputFile )
	if src_ds is None:
		print 'Could not open ' + fn
		# sys.exit(1)
	else:
		# Get raster information
		cols = src_ds.RasterXSize
		rows = src_ds.RasterYSize


		#lecture de la taille de pixel
		cols_out = int(cols / aFactor)
		rows_out = int(rows / aFactor)

		# Multilook
	 	cmd = "gdalwarp  "
	 	cmd += "-t_srs EPSG:4326 "
	 	cmd += "-srcnodata " + str(aNodata)
	 	cmd += " -dstnodata " + str(aNodata)
		cmd += " -multi "
		cmd += " -r bilinear -of vrt "
		cmd += " -ts " + str(cols_out) + " " + str(rows_out) + " "
		cmd += aInputFile + " "
		cmd += aOutputFile

		# progress.setInfo('\nReprojToVRT ' + cmd)
		p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
		ret= p1.communicate()[1]
	
	src_ds = None

def Polygonize(aInputRaster, aOutputVector):
	# Multilook
	if 'nt' in os.name:
		cmd = "gdal_polygonize.bat   "
	else:
		cmd = "gdal_polygonize.py   "

	cmd += aInputRaster + " "
	cmd += " -mask " + aInputRaster
	cmd += " -b 1 -f \"ESRI Shapefile\" "
	cmd += aOutputVector

	# progress.setInfo('\nPolygonize ' + cmd)
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def ConvexHull(aInputShape, aOutputShape):


	# Get a Layer
	inDriver = ogr.GetDriverByName("ESRI Shapefile")
	inDataSource = inDriver.Open(aInputShape, 0)
	inLayer = inDataSource.GetLayer()

	# prj name of shape
	outPRJfile = aOutputShape.replace('.shp', '.prj')

	#get spatial ref
	spatialRef = inLayer.GetSpatialRef()

	# Collect all Geometry
	geomcol = ogr.Geometry(ogr.wkbGeometryCollection)
	for feature in inLayer:
	    geomcol.AddGeometry(feature.GetGeometryRef())

	# Calculate convex hull
	convexhull = geomcol.ConvexHull()

	# Save extent to a new Shapefile
	outDriver = ogr.GetDriverByName("ESRI Shapefile")

	# Remove output shapefile if it already exists
	if os.path.exists(aOutputShape):
	    outDriver.DeleteDataSource(aOutputShape)

	# Create the output shapefile
	outDataSource = outDriver.CreateDataSource(aOutputShape)
	outLayer = outDataSource.CreateLayer("states_convexhull", geom_type=ogr.wkbPolygon)

	# Add an ID field
	idField = ogr.FieldDefn("id", ogr.OFTInteger)
	outLayer.CreateField(idField)

	# Create the feature and set values
	featureDefn = outLayer.GetLayerDefn()
	feature = ogr.Feature(featureDefn)
	feature.SetGeometry(convexhull)
	feature.SetField("id", 1)
	outLayer.CreateFeature(feature)

	spatialRef.MorphToESRI()
	file = open(outPRJfile, 'w')
	file.write(spatialRef.ExportToWkt())
	file.close()

	feature = None

	# Save and close DataSource
	inDataSource = None
	outDataSource = None

def GdalMultilookFactor(aInputFile, aOutputFile, aNodata, aFactor):
 	src_ds = gdal.Open( aInputFile )
	if src_ds is None:
		print 'Could not open ' + fn
		# sys.exit(1)
	else:
		# Get raster information
		cols = src_ds.RasterXSize
		rows = src_ds.RasterYSize

		#lecture de la taille de pixel
		cols_out = int(cols / aFactor)
		rows_out = int(rows / aFactor)

		# Multilook
	 	cmd = "gdalwarp  "
	 	cmd += "-t_srs EPSG:4326 "
	 	cmd += "-srcnodata " + str(aNodata)
	 	cmd += " -dstnodata " + str(aNodata)
		cmd += " -multi "
		cmd += " -r average -of vrt "
		cmd += " -ts " + str(cols_out) + " " + str(rows_out) + " "
		cmd += aInputFile + " "
		cmd += aOutputFile

		# progress.setInfo('\nGdalMultilookFactor ' + cmd)
		p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
		ret= p1.communicate()[1]

	src_ds = None

def GetAcquistionTimeFromFileName(aFileName, aSensor):
	AcqTime = 'Not recognised'

	if aSensor == 'Spot-6' or aSensor == 'Spot-7':
		TimeString = aFileName.split('_')[3]
		if len(TimeString) == 15 and TimeString.isdigit():
			AcqTime = TimeString[0:8]

	elif aSensor == 'Sentinel-1a' or aSensor == 'Sentinel-1b':
		if len(aFileName.split('_')) >= 9:
			TimeString = aFileName.split('_')[4]
			if len(TimeString) == 15 and TimeString[0:8].isdigit():
				AcqTime = TimeString[0:8]

		elif len(aFileName.split('-')) >= 9:
			TimeString = aFileName.split('-')[4]

			if len(TimeString) == 15 and TimeString[0:8].isdigit():
				AcqTime = TimeString[0:8]


	elif aSensor == 'Sentinel-2a' or aSensor == 'Sentinel-2b' or aSensor == 'Sentinel-2':
		TimeString = aFileName.split('_')
		if len(TimeString) > 7:
			TimeString = TimeString[7]

		if len(TimeString) == 15 and TimeString[0:8].isdigit():
			AcqTime = TimeString[0:8]
		elif len(aFileName.split('_')) >= 2:
			TimeStringTraitee = aFileName.split('_')[2]
			if TimeStringTraitee.isdigit():
				AcqTime = TimeStringTraitee

	elif 'Landsat-' in aSensor:
		TimeString = aFileName[9:16]
		if TimeString.isdigit():
			day = TimeString[4:7]
			year = TimeString[0:4]
			date = dT.datetime(int(year), 1, 1) + dT.timedelta(int(day))
			AcqTime = date.strftime('%Y%m%d')
		elif len(aFileName.split('_')) >= 2:
			TimeStringTraitee = aFileName.split('_')[2]
			if TimeStringTraitee.isdigit():
				AcqTime = TimeStringTraitee

	return AcqTime

def GetSensorNameFromFileName(aFilePath):
	SpotSensorName = ['Spot-4or5','Spot-6','Spot-7']
	SentinelSensorName = ['Sentinel-1a','Sentinel-1b','Sentinel-2a','Sentinel-2b','Sentinel-3a','Sentinel-3b', 'Sentinel-2']

	BeginFileName = os.path.basename(aFilePath)[0:3]

	FileName = os.path.basename(aFilePath).lower()
	# Get Sensor name
	# Test Spot 4 or 5
	if 'imagery' in FileName:
		Sensor = SpotSensorName[0]

	# Test Spot 6
	elif 'spot6' in FileName:
		Sensor = SpotSensorName[1]

	# Test Spot 7
	elif 'spot7' in FileName:
		Sensor = SpotSensorName[2]

	# Test S1A
	elif 's1a' in BeginFileName.lower():
		Sensor = SentinelSensorName[0]

	# Test S1B
	elif 's1b' in BeginFileName.lower():
		Sensor = SentinelSensorName[1]

	# Test S2A
	elif 's2a' in BeginFileName.lower():
		Sensor = SentinelSensorName[2]

	# Test S2B
	elif 's2b' in BeginFileName.lower():
		Sensor = SentinelSensorName[3]

	# Test S3A
	elif 's3a' in BeginFileName.lower():
		Sensor = SentinelSensorName[4]

	# Test S3B
	elif 's3b' in BeginFileName.lower():
		Sensor = SentinelSensorName[5]

	# Test S2
	elif 's2' in BeginFileName.lower():
		Sensor = SentinelSensorName[6]

	# Test Landsat
	elif re.match(r'^l..', BeginFileName.lower()):
		if BeginFileName[2].isdigit():
			Sensor = 'Landsat-' + BeginFileName[2]
		elif BeginFileName[1].isdigit():
			Sensor = 'Landsat-' + BeginFileName[1]
		else:
			Sensor = 'Not recognised'

	else:
		Sensor = 'Not recognised'

	return Sensor

def StretchRasterTo8bits(aInputFile, aOutputFile, aNodata, aNumStdevStretch):
	src_ds = gdal.Open( aInputFile )
	if src_ds is None:
		print 'Could not open ' + fn
		sys.exit(1)
	else:

		# Get num bands
		NumBands = src_ds.RasterCount

		# Multilook
	 	cmd = "gdal_translate  "
	 	cmd += " -ot Byte "
	 	cmd += "-a_nodata " + str(aNodata)
		cmd += " -of GTiff "
		cmd += "-co COMPRESS=DEFLATE -co PREDICTOR=2 -co TILED=YES "

		# Loop to ad scale parameters for each bands
		for band in range(NumBands):
			band += 1
			srcband = src_ds.GetRasterBand(band)

			# Get stats band
			stats = srcband.GetStatistics( True, True )
			mean = stats[2]
			stdev = stats[3]

			ScaleMin = mean - aNumStdevStretch * stdev
			ScaleMax= mean + aNumStdevStretch * stdev

			cmd += " -scale_" + str(band) + ' ' + str(ScaleMin) + ' ' + str(ScaleMax) + ' '

		cmd += aInputFile + " "
		cmd += aOutputFile

		# print '\n', cmd
		p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
		ret= p1.communicate()[1]

	src_ds = None


def GetSensorInfFromFile(aInputListFile):
	'''
	This function return some informations (if available) of the data such as
	- Sensor name
	- Acquisition time
	- Number of bands
	Landsat
		- tif
		- LXS L = Landsat S = satellite number
	Spot
		4 et 5
			imagery.tif en lower
		5
		6 SPOT6 dans le nom
	Sentinel-2
		- debut s2a ou s2b en lower
	Sentinel-1
		- debut s1a ou s1b lower

	'''
	# LandsatSensorName = ['Landsat-1','Landsat-2','Landsat-3','Landsat-4','Landsat-5','Landsat-66','Landsat-7','Landsat-8','Landsat-9']
	SpotSensorName = ['Spot-4or5','Spot-6','Spot-7']
	SentinelSensorName = ['Sentinel1a','Sentinel1b','Sentinel2a','Sentinel2b','Sentinel3a','Sentinel3b']

	FilesSensorList = []
	FilesAcqTimeList = []
	FilesNumBandList = []
	for file in aInputListFile:
		Sensor = GetSensorNameFromFileName(file)
		AcquisitionTime = GetAcquistionTimeFromFileName(os.path.basename(file), Sensor)
		FilesAcqTimeList.append(AcquisitionTime)
		FilesSensorList.append(Sensor)

		# Get Number of bands
		src_ds = gdal.Open(file)
		if src_ds is None:
			print 'Unable to open ' + file
			sys.exit(1)

		NumBand = src_ds.RasterCount
		src_ds = None

		# Add num band to the list
		FilesNumBandList.append(NumBand)



	return FilesSensorList, FilesNumBandList, FilesAcqTimeList


def ReadFileFromCatalog(aInputShape):
	shapefile = aInputShape
	driver = ogr.GetDriverByName("ESRI Shapefile")
	dataSource = driver.Open(shapefile, 0)
	layer = dataSource.GetLayer()

	ListFiles = []
	for feature in layer:
	    ListFiles.append(feature.GetField("File"))
	# Save and close DataSources
	dataSource = None

	return ListFiles

def CopyShape(aInputShape, aOutputShape):
	cmd = "ogr2ogr -f \"ESRI Shapefile\" "
	cmd += aOutputShape + ' '
	cmd += aInputShape

	# print cmd

		# progress.setInfo('\nGdalMultilookFactor ' + cmd)
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def MergeShape(aInputShape,aOutputShape):
	cmd = "ogr2ogr -f \"ESRI Shapefile\"  -update -append "
	cmd += aOutputShape + ' '
	cmd += aInputShape
	# print cmd

		# progress.setInfo('\nGdalMultilookFactor ' + cmd)
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def GenerateCatalogue(aInputDir, aOutputShapeFile):
	UpdateCatalogue = False
	# Create Catalog dir
	CatalogNameDir = os.path.basename(os.path.splitext(aOutputShapeFile)[0])
	Nom_Du_Catalogue = CatalogNameDir
	CatalogDir = os.path.join(aInputDir, CatalogNameDir)
	if not os.path.exists(CatalogDir):
		os.makedirs(CatalogDir)
	# progress.setInfo('\nCatalogDir ' + str(CatalogDir))

	# create tmp dir
	TimeNow = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
	TmpDir = os.path.join(aInputDir, "tmp_" + TimeNow)
	if not os.path.exists(TmpDir):
		os.makedirs(TmpDir)

	# List all tif and jp2 files
	AllJp2File = directory_liste_ext_files(aInputDir, 'jp2')
	AllTifFile = directory_liste_ext_files(aInputDir, 'tif')
	AllTiffFile = directory_liste_ext_files(aInputDir, 'tiff')

	AllRasterFile = AllJp2File + AllTifFile + AllTiffFile

	# print 'AllRasterFile', AllRasterFile

	# Get Sensor information
	Sensor, NumBandList, AcquisitionTime = GetSensorInfFromFile(AllRasterFile)

	# Create Output Shape file
	# set up the shapefile driver
	driver = ogr.GetDriverByName("ESRI Shapefile")

	# Test if shape already exist and if yes delete it
	AlreadyProcFiles = []
	NewList = []
	if os.path.exists(aOutputShapeFile):
		# Create a Copy of the actual shape as backup
		ShapeCopy = aOutputShapeFile.replace('.shp', '_Backup-' + TimeNow +'.shp')
		CopyShape(aOutputShapeFile, ShapeCopy)
		AlreadyProcFiles = ReadFileFromCatalog(aOutputShapeFile)
		AlreadyProcFiles = [ os.path.join(aInputDir, item).replace("\\","/") for item in AlreadyProcFiles]

		# print '\nAlreadyProcFiles',AlreadyProcFiles


		# Filter the input file
		for file in AllRasterFile:
			# print 'file.replace("\\","/")', file.replace("\\","/")
			# pdb.set_trace()
			if (not file.replace("\\","/") in AlreadyProcFiles) and (not '_QL.'  in file) and (not "_ML" in file) and (not 'QI_DATA'  in file):
				NewList.append(file)
		AllRasterFile = NewList


	# print '\n\nAlreadyProcFiles',AlreadyProcFiles, '\n\n'
	# print 'NewList', NewList
	# print '\nAllTiffFile',AllTiffFile

	# pdb.set_trace()
	# create the data source
	NewCatalogue = aOutputShapeFile
	if os.path.exists(aOutputShapeFile):
		UpdateCatalogue = True
		NewCatalogue = aOutputShapeFile.replace('.shp', '_Update-' + TimeNow +'.shp')

	data_source = driver.CreateDataSource(NewCatalogue)

	# create the spatial reference, WGS84
	srs = osr.SpatialReference()
	srs.ImportFromEPSG(4326)

	outlayer = data_source.CreateLayer(Nom_Du_Catalogue, geom_type=ogr.wkbPolygon)

	# Add the fields we're interested in
	field_name = ogr.FieldDefn("File", ogr.OFTString)
	field_name.SetWidth(254)
	outlayer.CreateField(field_name)

	field_name = ogr.FieldDefn("FileQL", ogr.OFTString)
	field_name.SetWidth(254)
	outlayer.CreateField(field_name)

	field_name = ogr.FieldDefn("Sensor", ogr.OFTString)
	field_name.SetWidth(254)
	outlayer.CreateField(field_name)

	field_name = ogr.FieldDefn("NumBands", ogr.OFTInteger)
	field_name.SetWidth(5)
	outlayer.CreateField(field_name)

	field_name = ogr.FieldDefn("AcqTime", ogr.OFTString)
	field_name.SetWidth(254)
	outlayer.CreateField(field_name)

	# Get the output Layer's Feature Definition
	outLayerDefn = outlayer.GetLayerDefn()


	# Generate Quickloock
	i = 0
	for file in AllRasterFile:
		TimeNow = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
		# TimeNow = ''
		# print 'file: ', os.path.basename(AllRasterFile[i]), ' Sensor: ', Sensor[i], ' Num band: ', NumBandList[i], 'acq time ', AcquisitionTime[i],'\n'
		# Get dir file name
		FileDir = os.path.dirname(file)

		# Get file name without extension
		FileName = os.path.basename(os.path.splitext(file)[0])

		print '\nFileName', FileName
		# progress.setInfo('\nFileName ' + FileName)

		# Create Temporary Output ML File path
		# TmpMLFilePath = os.path.join(TmpDir,  FileName + '_ML.tif')
		TmpMLFilePath = os.path.join(TmpDir, 'ML'+TimeNow + '.vrt')

		# Create Temporary reprojected in vrt
		TmpVRTFilePath = os.path.join(TmpDir, TimeNow +'.vrt')

		# TmpMLVRTFilePath = os.path.join(TmpDir,  FileName + '_ML.vrt')
		TmpMaskFilePath = os.path.join(TmpDir, 'Mask'+TimeNow +'.tif')

		# Create Temporary Vector footprint file
		# TmpFootprintFilePath = os.path.join(TmpDir,  FileName + '_Footprint.shp')
		TmpFootprintFilePath = os.path.join(TmpDir, 'Footprint'+TimeNow +'.shp')

		# Create Temporary Dissolved Vector footprint file
		# TmpDissolvedFootprintFilePath = os.path.join(TmpDir,  FileName + '_Footprint_Dissolve.shp')
		TmpConvexFootprintFilePath = os.path.join(TmpDir,  'FootprintConvex'+TimeNow +'.shp')

		# Create Output QuickLoock File path
		OutputFilePath = os.path.join(FileDir,  FileName + '_QL.tif')

		OutputFilePath = OutputFilePath.replace(aInputDir + os.sep,CatalogDir + os.sep)

		# Create the output dir of QL file
		if not os.path.exists(os.path.dirname(OutputFilePath)):
			os.makedirs(os.path.dirname(OutputFilePath))

		# print '\nfile, FileDir, TmpMLFilePath, TmpVRTFilePath, TmpVRTMaskFilePath, TmpFootprintFilePath, TmpDissolvedFootprintFilePath, OutputFilePath\n',file, FileDir, TmpMLFilePath, TmpVRTFilePath, TmpVRTMaskFilePath, TmpFootprintFilePath, TmpDissolvedFootprintFilePath, OutputFilePath

		# Create the QL
		if (not '_QL.'  in file) and (not "_ML" in file) and (not 'QI_DATA'  in file):
			# DelDirFiles(TmpDir)
			#Create ML
			GdalMultilookFactor(file, TmpMLFilePath, 0, 10)

			# Convert original file to vrt to have nodata
			ReprojToVRT(file, TmpVRTFilePath, 0, 5)

			# Convert the reproj vrt to mask
			ConvertMLToMask(TmpVRTFilePath, TmpMaskFilePath, 0)

			Polygonize(TmpMaskFilePath, TmpFootprintFilePath)
			# DissolveAllFeatures(TmpFootprintFilePath, TmpDissolvedFootprintFilePath)

			ConvexHull(TmpFootprintFilePath, TmpConvexFootprintFilePath)

			# Open the dissolved File
			inDataSource = driver.Open(TmpConvexFootprintFilePath, 0)
			inLayer = inDataSource.GetLayer()

			#get spatial ref
			spatialRef = inLayer.GetSpatialRef()

			# We get the polygon
			inFeature = inLayer.GetFeature(0)

			# Create output Feature
			outFeature = ogr.Feature(outLayerDefn)
			outFeature.SetGeometry(inFeature.GetGeometryRef())

			# set field values
			FileField = file.replace(aInputDir + os.sep,"")
			if 'nt' in os.name:
				FileField = FileField.replace("\\","/")

			FileQLField = OutputFilePath.replace(aInputDir + os.sep,"")
			if 'nt' in os.name:
				FileQLField = FileQLField.replace("\\","/")

			outFeature.SetField("File", FileField)
			outFeature.SetField("FileQL", FileQLField)
			outFeature.SetField("Sensor", Sensor[i])
			outFeature.SetField("NumBands", NumBandList[i])
			outFeature.SetField("AcqTime", AcquisitionTime[i])

			# add the feature to the output layer
			outlayer.CreateFeature(outFeature)

			# destroy the features
			inFeature = None
			outFeature = None
			inLayer = None

			# Close DataSource
			inDataSource = None

			# Create Streched QuickLoock
			StretchRasterTo8bits(TmpMLFilePath, OutputFilePath, 0, 2)

		i += 1

	OutPrjFile = os.path.splitext(NewCatalogue)[0] + '.prj'
	spatialRef.MorphToESRI()
	filePrj = open(OutPrjFile, 'w')
	filePrj.write(spatialRef.ExportToWkt())
	filePrj.close()

	# Close DataSource
	outlayer = None
	data_source = None

	# Merge new one to the old one if required
	if UpdateCatalogue and os.path.exists(aOutputShapeFile):
		MergeShape(NewCatalogue,aOutputShapeFile)

		# Delete New catalogue
		DriverName = "ESRI Shapefile"
		driver = ogr.GetDriverByName(DriverName)
		if os.path.exists(NewCatalogue):
			driver.DeleteDataSource(NewCatalogue)

	# Delete temporary files
	DelDirAndItsFiles(TmpDir)

if __name__ == '__main__':
	main()



