##Radar=group
##Pre-Processing Free ALOS Mosaic=name
##Input_Data_Folder=folder
##Output_EPSG=crs
##Output_Data_Folder=folder
##Ram=number 256

import os, subprocess, shutil

'''
1 - List all file
2 - filter HH, HV, and incidence angle
3 - generate VRT for each one
4 - reproject to new proj (gdal translate)
5 - convert DN to dB for HH and HV to generate colorcomposition

'''

# Input_Data_Folder = '/media/cedric/CL/Meiganga/Alos/2007/N10E010_07_MOS'
# Output_Data_Folder = '/media/cedric/CL/Meiganga/Alos/2007/Process'
# Output_EPSG = 'EPSG:32633'
# Ram = 2000

def directory_liste_ext_files(directory, filt_ext):
    list_file = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in filenames:
            if filename.endswith((filt_ext)):
            	myfile = os.path.join(root, filename)
                list_file.append(myfile.replace("\\","/"))

    list_file.sort()
    return list_file

def DN2dB(InputFile, Band, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += InputFile + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\"" +'im1b1<10^2.4?-35:10*log10(im1b1*im1b1)-83' + "\""

	# progress.setInfo(cmd)
	print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

# OTB Create HVdB - HHdB
def DiffdB(aInputFileCopol, aInputFileCrosspol, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += aInputFileCopol + " "
	cmd += aInputFileCrosspol + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\" im2b1 - im1b1 \""

	# progress.setInfo(cmd)
	print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def GdalBuildVRT(aInputListFile, aOutputFile,  aSRCNoData, aVRTNoData):
    TmpListfile = aOutputFile.replace('.vrt', 'TMPListFile.txt')
    fic=open(TmpListfile,'w')
    for file in aInputListFile:
        fic.write(file +'\n')
    fic.close() 

    cmd = "gdalbuildvrt "
    cmd += " -separate -overwrite "
    cmd += " -srcnodata " + str(aSRCNoData)
    cmd += " -vrtnodata " + str(aVRTNoData)
    cmd += " " + aOutputFile
    cmd += " -input_file_list " + TmpListfile
    cmd += " " + aOutputFile

    # progress.setInfo(cmd)
    print cmd
    
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

    if os.path.exists(TmpListfile):
        os.remove(TmpListfile)

def GenerateDualPolColorcompositiondB(aHHDN, aHVDN, aOutHHdB, aOutHVdB, aOutDiff, aOutVRT, aRam):
	DN2dB(aHHDN, 'im1b1', aOutHHdB, aRam)
	DN2dB(aHVDN, 'im1b1', aOutHVdB, aRam)
	DiffdB(aOutHHdB, aOutHVdB, aOutDiff, Ram)
	GdalBuildVRT([aOutHHdB, aOutHVdB, aOutDiff], aOutVRT,  0, 0)

def GdalVrtStacking(aInputListFile, aOutputRaster, aNodataVal, aSeparate):
    TmpListfile = aOutputRaster.replace('.vrt', 'TMPListFile.txt')
    fic=open(TmpListfile,'w')
    for file in aInputListFile:
        fic.write(file +'\n')
    fic.close() 

    cmd = 'gdalbuildvrt '
    if aSeparate: cmd += '-separate '
    cmd += ' -resolution highest '
    cmd += ' -srcnodata ' + str(aNodataVal) + ' -vrtnodata '+ str(aNodataVal) + ' '
    cmd += '-input_file_list ' + TmpListfile + ' -overwrite '
    cmd += aOutputRaster

    # progress.setInfo(cmd)
    # print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

    if os.path.exists(TmpListfile):
        os.remove(TmpListfile)

def GdalWarp(aInputFile, aOutputRaster, aNodataVal, aOutEPSG, aResampleMethod, aOutputResolution ):
	cmd = "gdalwarp  -dstnodata "
	cmd += str(aNodataVal)
	cmd += " -tr " + str(aOutputResolution) + ' ' + str(aOutputResolution) + ' '
	cmd += " -srcnodata " + str(aNodataVal) + " "
	cmd += " -multi "
	cmd += " -r " + aResampleMethod +" "
	cmd += " -of GTiff "
	cmd += "-t_srs " + str(aOutEPSG) + " "
	cmd += aInputFile
	cmd += " "
	cmd += aOutputRaster

	# progress.setInfo(cmd)
    # print cmd

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]



# Program
# create tmp dir
TmpDir = os.path.join(Output_Data_Folder, "tMp")
if not os.path.exists(TmpDir):
	os.makedirs(TmpDir)


# List all files
AllFiles = directory_liste_ext_files(Input_Data_Folder, '.hdr')

HHFiles = [os.path.splitext(file)[0] for file in  AllFiles  if ('_HH' in file)]
HVFiles = [os.path.splitext(file)[0] for file in  AllFiles  if ('_HV' in file)]
linciFiles = [os.path.splitext(file)[0] for file in  AllFiles  if ('_linci' in file)]
dateFiles = [os.path.splitext(file)[0] for file in  AllFiles  if ('_date' in file)]
maskFiles = [os.path.splitext(file)[0] for file in  AllFiles  if ('_mask' in file)]

# Create VRT
#Get mosaic date
FileName1 = os.path.basename(os.path.splitext(HHFiles[0])[0])
MosDate = FileName1.split('_')[1]
MosDate = '20' + MosDate

HHVrt = os.path.join(TmpDir, "HH.vrt").replace("\\","/")
HVVrt = os.path.join(TmpDir, "HV.vrt").replace("\\","/")
linciVrt = os.path.join(TmpDir, "linci.vrt").replace("\\","/")
dateVrt = os.path.join(TmpDir, "date.vrt").replace("\\","/")
maskVrt = os.path.join(TmpDir, "mask.vrt").replace("\\","/")

GdalVrtStacking(HHFiles, HHVrt,  0 , 0)
GdalVrtStacking(HVFiles, HVVrt,  0 , 0)
GdalVrtStacking(linciFiles, linciVrt,  0 , 0)
GdalVrtStacking(dateFiles, dateVrt,  0 , 0)
GdalVrtStacking(maskFiles, maskVrt,  0 , 0)

# Reproject the files
HHRep = os.path.join(TmpDir, "HHRep.tif").replace("\\","/")
HVRep = os.path.join(TmpDir, "HVRep.tif").replace("\\","/")
linciRep = os.path.join(Output_Data_Folder, "ALOSMosaic_" + MosDate + "Incidence.tif").replace("\\","/")
dateRep = os.path.join(Output_Data_Folder, "ALOSMosaic_" + MosDate + "Dates.tif").replace("\\","/")
maskRep = os.path.join(Output_Data_Folder, "ALOSMosaic_" + MosDate + "Mask.tif").replace("\\","/")

GdalWarp(HHVrt, HHRep, 0, Output_EPSG, 'average', 25 )
GdalWarp(HVVrt, HVRep, 0, Output_EPSG, 'average', 25 )
GdalWarp(linciVrt, linciRep, 0, Output_EPSG, 'average', 25 )
GdalWarp(dateVrt, dateRep, 0, Output_EPSG, 'average', 25 )
GdalWarp(maskVrt, maskRep, 0, Output_EPSG, 'average', 25 )

# Generate colorcomposition
HHdB = os.path.join(Output_Data_Folder, "ALOSMosaic_" + MosDate + "HHdB.tif").replace("\\","/")
HVdB = os.path.join(Output_Data_Folder, "ALOSMosaic_" + MosDate + "HVdB.tif").replace("\\","/")
Diff = os.path.join(Output_Data_Folder, "ALOSMosaic_" + MosDate + "HVdB-HHdB.tif").replace("\\","/")
CompoColVRT = os.path.join(Output_Data_Folder, "ALOSMosaic_" + MosDate + "HHdB_HVdB_HVdB-HHdB.vrt").replace("\\","/")

GenerateDualPolColorcompositiondB(HHRep, HVRep, HHdB, HVdB, Diff, CompoColVRT, Ram)

shutil.rmtree(TmpDir)