##Sentinel-1 Deforestation Process=group
##1 - Enhance Loss of Vegetation Contrast=name
##Input_Data_Folder=folder
##Input_Forest_Polygon=vector
##Class_Polygon_Field_Name=field Input_Forest_Polygon
##Forest_Class_Field_Value=number 1
##Weight_Of_New_Date=number 0.3
##Forest_Mean_Radiometry_Crosspol_dB=number 0.
##Forest_Mean_Radiometry_Copol_dB=number 0.
##Apply_Temporal_Smoothing=boolean False
##Output_Data_Folder=folder
##Ram=number 256

import subprocess, os, shutil
# from osgeo import gdal
import numpy as np
import gdal, ogr, osr
import pandas as pd

import pdb
from random import shuffle
from scipy.signal import medfilt

'''
# Authors: Cedric Lardeux et Pierre-Louis Frison

'''

Input_Data_Folder = '/home/DataProcessing/FrenchGuyana/AdaptTempFilt097/p120'
Input_Forest_Polygon = '/home/DataProcessing/FrenchGuyana/OptCont/p120/roifP120.shp'
#
Class_Polygon_Field_Name = 'id'
Forest_Class_Field_Value = 1

Weight_Of_New_Date = 0.3

Forest_Mean_Radiometry_Copol_dB =  -5.67
Forest_Mean_Radiometry_Crosspol_dB =-11.57

Ram = 30000

Apply_Temporal_Smoothing = True


Output_Data_Folder = '/home/DataProcessing/FrenchGuyana/OptCont/p120'


# progress.setInfo(Input_Forest_Polygon)

# progress.setInfo(str(Forest_Mean_Radiometry_Crosspol_dB))
# progress.setInfo(str(Forest_Mean_Radiometry_Copol_dB))


Input_Vector = Input_Forest_Polygon.replace("\\","/")
Input_Folder  = str(Input_Data_Folder).replace("\\","/")
Category_Field_Name = Class_Polygon_Field_Name
RefClassId = Forest_Class_Field_Value
Alpha = Weight_Of_New_Date
Output_Folder = str(Output_Data_Folder).replace("\\","/")
Average_Forest_dB_Value_Crosspol = Forest_Mean_Radiometry_Crosspol_dB
Average_Forest_dB_Value_Copol = Forest_Mean_Radiometry_Copol_dB
MedianFiltering = Apply_Temporal_Smoothing


'''
This function list this immediate subdirectories in one given directory
'''
def get_immediate_subdirectories(a_dir):
    return [os.path.join(a_dir, name) for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

'''
This function list all file with a given file extention in one directory
'''
def directory_liste_ext_files(directory, filt_ext):
    list_file = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in filenames:
            if filename.endswith((filt_ext)):
                myfile = os.path.join(root, filename)
                list_file.append(myfile.replace("\\","/"))

    list_file.sort()
    return list_file

def EMWA(aData, aAlpha):
    Smooth = np.zeros(shape=aData.shape,dtype=np.float)
    SmoothOld = np.zeros(shape=aData.shape[1:3],dtype=np.float)

    SmoothOld = aData[0,:,:]
    Smooth[0,:,:] = aData[0,:,:]

    for i in range(1,aData.shape[0]):
        Smooth[i,:,:] = aAlpha * aData[i,:,:] + (1- aAlpha) * SmoothOld
        SmoothOld = Smooth[i,:,:]

    return Smooth

def GetFilesFromVrt(aInputVRT):
    #Open the VRT
    ds = gdal.Open(aInputVRT)

    # Get file name path
    RawVrtFileList = ds.GetFileList()[1:]

    ds = None

    if '.ovr' in RawVrtFileList[0]:
        RawVrtFileList = RawVrtFileList[1:]

    return RawVrtFileList

def GdalBuildVRT(aInputListFile, aOutputFile,  aSRCNoData, aVRTNoData):
    TmpListfile = aOutputFile.replace('.vrt', 'TMPListFile.txt')
    fic=open(TmpListfile,'w')
    for file in aInputListFile:
        fic.write(file +'\n')
    fic.close()

    cmd = "gdalbuildvrt "
    cmd += " -separate -overwrite "
    cmd += " -srcnodata " + str(aSRCNoData)
    cmd += " -vrtnodata " + str(aVRTNoData)
    cmd += " " + aOutputFile
    cmd += " -input_file_list " + TmpListfile

    # progress.setInfo(cmd)
    # print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

    if os.path.exists(TmpListfile):
        os.remove(TmpListfile)

def GetAcquistionTimeFromSentinel1FileName(aFileName):
    aFileName = aFileName.split("/")[-1]


    pos=max([aFileName.find('_2015'),aFileName.find('_2016'),aFileName.find('_2017'),aFileName.find('_2018'),aFileName.find('_2019'),aFileName.find('_2020')])
    return aFileName[pos+1:pos+9]


'''
This function return all pixel values overlayed by a polygon
For each pixel it return the value of requested band and
The label in the given Vector field
aInputVector -> path of the Input Polygon
aFielName    -> field name of the vector file that contain the label
aInputRaster -> path of the raster to get the pixel band values
aListBands   -> list of the bands number to get the pixels

return
first, one list of y,x,ygeo,xgeo that contain pixel coordinate and geographical
second, one list where first sublist is label, next sublist contain the pixel value
'''
def GetRasterBandsPixelsValuesFromVectorFile(aInputVector, aFielName, aInputRaster, aListBands, aMinSamplingPointPerFeature):
    aFielName = str(aFielName)
    # List that will store respectivaly Label, feature id and band pixel values
    StoreList = [[] for x in xrange(len(aListBands)+2)]

    # lIst that contain first pixel row cols next in geographical
    PixelCoordList = [[] for x in xrange(4)]

    # Open Raster data
    raster = gdal.Open(aInputRaster)

    # Get raster georeference info
    transform = raster.GetGeoTransform()
    xOrigin = transform[0]
    yOrigin = transform[3]
    pixelWidth = transform[1]
    pixelHeight = transform[5]

    # Open Vector data
    shp = ogr.Open(aInputVector)
    lyr = shp.GetLayer()

    # Reproject vector geometry to same projection as raster
    sourceSR = lyr.GetSpatialRef()
    targetSR = osr.SpatialReference()
    targetSR.ImportFromWkt(raster.GetProjectionRef())
    coordTrans = osr.CoordinateTransformation(sourceSR,targetSR)

    # Get number of Vector feature
    NumFeature = lyr.GetFeatureCount()


    for iFeat in range(NumFeature):
        # Get the input Feature
        feat = lyr.GetFeature(iFeat)

        geom = feat.GetGeometryRef()
        geom.Transform(coordTrans)

        # Get extent of feat
        geom = feat.GetGeometryRef()

        # Get Field Value
        FValue = feat.GetField(aFielName)
        if (geom.GetGeometryName() == 'MULTIPOLYGON'):
            count = 0
            pointsX = []; pointsY = []
            for polygon in geom:
                geomInner = geom.GetGeometryRef(count)
                ring = geomInner.GetGeometryRef(0)
                numpoints = ring.GetPointCount()
                for p in range(numpoints):
                    lon, lat, z = ring.GetPoint(p)
                    pointsX.append(lon)
                    pointsY.append(lat)
                count += 1
        elif (geom.GetGeometryName() == 'POLYGON'):
            ring = geom.GetGeometryRef(0)
            numpoints = ring.GetPointCount()
            pointsX = []; pointsY = []
            for p in range(numpoints):
                lon, lat, z = ring.GetPoint(p)
                pointsX.append(lon)
                pointsY.append(lat)

        else:
            sys.exit("ERROR: Geometry needs to be either Polygon or Multipolygon")

        xmin = min(pointsX)
        xmax = max(pointsX)
        ymin = min(pointsY)
        ymax = max(pointsY)

        # Specify offset and rows and columns to read
        xoff = int((xmin - xOrigin)/pixelWidth)
        #yoff = int((yOrigin - ymax)/(-pixelHeight))
        yoff = int((yOrigin - ymax)/pixelWidth)
        xcount = int((xmax - xmin)/pixelWidth)+1
        ycount = int((ymax - ymin)/(-pixelHeight))+1
        #ycount = int((ymax - ymin)/pixelWidth)+1

        # Create memory target raster to store Polygon mask area
        target_ds = gdal.GetDriverByName('MEM').Create('', xcount, ycount, 1, gdal.GDT_Byte)
        target_ds.SetGeoTransform((
            xmin, pixelWidth, 0,
            ymax, 0, pixelHeight,
        ))

        # Create for target raster the same projection as for the value raster
        raster_srs = osr.SpatialReference()
        raster_srs.ImportFromWkt(raster.GetProjectionRef())
        target_ds.SetProjection(raster_srs.ExportToWkt())

        # Rasterize zone polygon to raster
        gdal.RasterizeLayer(target_ds, [1], lyr, burn_values=[1])

        # Get raster mask in a numpy array
        bandmask = target_ds.GetRasterBand(1)
        datamask = bandmask.ReadAsArray(0, 0, xcount, ycount).astype(np.float)

        # clear target_ds
        target_ds = None

        # Get label of the current feature
        label = FValue

        # Get pixel coordinate of overlay with polygon
        # rows, cols = np.where(np.logical_not(datamask)) # commande prenant le complementaire du polygone
        rows, cols = np.where(datamask == 1)

        if aMinSamplingPointPerFeature > 0:
            if len(rows) > aMinSamplingPointPerFeature:
                NumPixRange = range(len(rows))
                shuffle(NumPixRange)
                rows = rows[NumPixRange[:aMinSamplingPointPerFeature]]
                cols = cols[NumPixRange[:aMinSamplingPointPerFeature]]



        PixelCoordList[0].extend((rows+yoff).tolist())
        PixelCoordList[1].extend((cols+xoff).tolist())

        # Convert Pixel Coordinate to geographic coordinate
        PixelCoordList[2].extend([ y * pixelHeight + yOrigin  for y in rows+yoff])
        PixelCoordList[3].extend([ x * pixelWidth +  xOrigin  for x in cols+xoff])
        #import pdb; pdb.set_trace()

        numPixel = len(rows.tolist())

        # Read raster bands as arrays
        for j in range(len(aListBands)):
            bandRaster = raster.GetRasterBand(aListBands[j])
            dataraster = bandRaster.ReadAsArray(xoff, yoff, xcount, ycount).astype(np.float)

            # Get pixel value of curent band
            bandVal = [dataraster[y,x] for y,x in zip(rows, cols)]

            # Store in StoreList
            StoreList[j+2].extend(bandVal)

        # We store corresponding label in first cols of the list
        StoreList[0].extend([label] * numPixel)
        # We store corresponding feature id in second cols of the list
        StoreList[1].extend([iFeat] * numPixel)



    shp = None
    raster = None
    return PixelCoordList, StoreList

def getDayFromS1File(aFileName):
    aFileName = aFileName.split("/")[-1]


    pos=max([aFileName.find('_2015'),aFileName.find('_2016'),aFileName.find('_2017'),aFileName.find('_2018'),aFileName.find('_2019'),aFileName.find('_2020')])
    return aFileName[pos+1:pos+9]


def GetRefTimeRadiometry(aInputRaster, aInputVector, aInputFieldName, aFieldValue):
    # We extract all point in ref polygon so 0
    NumSamplingPointPerFeature = 10000
    # We get vrt file list
    VRTFNameList = GetFilesFromVrt(aInputRaster)
    # Get acquisition time of each data
    FilesTimeStamp = []
    for file in VRTFNameList:
        FileName = os.path.basename(os.path.splitext(file)[0])
        # Time = GetAcquistionTimeFromSentinel1FileName(FileName)
        Time = getDayFromS1File(FileName)
        Time = Time[0:4] + '-' + Time[4:6] + '-' + Time[6:8]

        TimeStamp = pd.Timestamp(Time)
        FilesTimeStamp.append(int(TimeStamp.value /10E9))

    ListBands = [band + 1 for band in range(len(VRTFNameList))]

    # The list of all pixel
    __, PixelLists = GetRasterBandsPixelsValuesFromVectorFile(aInputVector, aInputFieldName, aInputRaster, ListBands,NumSamplingPointPerFeature)

    NumSamplePixel = len(PixelLists[0])

    # Define the col names
    DFColsName = ['label', 'time','sigma0dB']
    df = pd.DataFrame(columns=DFColsName)

    # list to add to dataframe
    LabelList = []
    TimeList = []
    SigmaList = []

    # Add values to the dataframe
    for Bi in range(len(VRTFNameList)):
        LabelList.extend(PixelLists[0])
        TimeList.extend([FilesTimeStamp[Bi]] * NumSamplePixel)
        SigmaList.extend(PixelLists[Bi + 2])

    df['label'] = LabelList
    df['time'] = TimeList
    df['sigma0dB'] = SigmaList

    # Get Stable forest average radiometry for each time
    dfForest = df[df['label'] == aFieldValue]

    # Subset the column (remove label and feature column)
    dfForest = dfForest[["time", "sigma0dB"]]

    # Average all pixel by time
    dfForestAverage = dfForest.groupby(["time"]).mean().unstack("time")

    # Get time list from groupby
    TimeStampFromDf = dfForest.groupby(["time"]).groups.keys()

    # Convert to list
    listForest = dfForestAverage.values.T.tolist()

    return sorted(TimeStampFromDf), listForest

def MedianTimeFiltering(aData,aWinSize):
    aDataMed = medfilt(aData, (aWinSize, 1,1))

    return aDataMed


def TileRasterProcessingEWMA(aInputRaster, aOutputFolder, aBlockSize,aWindowSize, aAlpha, aRefRadiometry, aAverage_Forest_dB_Value, aUpdate):
    '''
    TODO: Add sliding window parameter

    This method show how to process huge raster
    using easy tilling based on block size
    and if necessary taking into account window size
    when necessary in processing like filtering for example
    aInputRasterPath string --> Contain input raster path to process
    aOutputRasterPath string --> Contain output raster path to process
    aBlockSize integer --> Contain the size of square block to Tile
    aWindowSize integer --> Contain windows size for sliding window (0 size by default)
    '''
    # aWindowSize = aTempWindowSize

    # we put X and Y block size based on aBlockSize
    xBSize = aBlockSize
    yBSize = aBlockSize

    # We get vrt file list
    VRTFNameList = GetFilesFromVrt(aInputRaster)

    


    # Get acquisition time of each data
    TimeList = []
    for file in VRTFNameList:
        FileName = os.path.basename(os.path.splitext(file)[0])

        TimeList.append(getDayFromS1File(FileName))

    # Get sorted index by date
    SortIndexVRTTimeList = np.argsort(TimeList)

    # tmpname = [VRTFNameList[it] for it in SortIndexVRTTimeList]

    # print 'before sort\n',VRTFNameList

    # print '\nAfter sort\n',tmpname
    # sys.exit(1)

    # We open the raster
    src_ds = gdal.Open( aInputRaster )
    if src_ds is None:
        print 'Could not open ' + fn
        sys.exit(1)

    # We get number of row and cols
    rows = src_ds.RasterYSize
    cols = src_ds.RasterXSize

    NumBands = src_ds.RasterCount

    # We force Float32 to read data
    BandType = gdal.GDT_Float32

    # We get Projection from input raster
    InputGeoTransform = src_ds.GetGeoTransform()
    InputProjection = src_ds.GetProjection()

    # Output file
    driver = gdal.GetDriverByName( "GTiff" )
    GdalOutputFilePointerList = []
    GdalOutputCorrectedFilePointerList = []
    GdalInputBandPointerList = []

    for i, index in enumerate(SortIndexVRTTimeList):
        FileDir = os.path.dirname(VRTFNameList[index])
        # print '\nInput from vrt ', VRTFNameList[index]
        MainFileDir = os.path.split(FileDir)[0]
        FName = os.path.basename(os.path.splitext(VRTFNameList[index])[0])

        FileName = os.path.join(FileDir,FName + '_EWMA' + str(aAlpha) + '.tif')
        FileName = FileName.replace("\\","/")
        FileName = FileName.replace(MainFileDir + '/',aOutputFolder + '/')
        FileName.replace("\\","/")

        # Create input band pointer list
        GdalInputBandPointerList.append(src_ds.GetRasterBand(int(index) + 1))

        # print '\naUpdate', aUpdate



        # Create output directory
        if not os.path.exists(os.path.dirname(FileName)):
            os.makedirs(os.path.dirname(FileName))
        if aUpdate:
            # print '\n Ouput in update ', FileName
            if i > 0:
                GdalOutputFilePointerList.append(driver.Create(FileName, cols, rows, 1, BandType, options = [ 'COMPRESS=DEFLATE', 'PREDICTOR=3', 'ZLEVEL=9' , 'BIGTIFF=YES', 'TILED=YES'] ))
                GdalOutputFilePointerList[i-1].SetGeoTransform(InputGeoTransform)
                GdalOutputFilePointerList[i-1].SetProjection(InputProjection)
        else:
            GdalOutputFilePointerList.append(driver.Create(FileName, cols, rows, 1, BandType,  options = [ 'COMPRESS=DEFLATE', 'PREDICTOR=3', 'ZLEVEL=9', 'BIGTIFF=YES', 'TILED=YES' ] ))
            GdalOutputFilePointerList[i].SetGeoTransform(InputGeoTransform)
            GdalOutputFilePointerList[i].SetProjection(InputProjection)

        # print 'FName',FName

    GdalOutputBandPointerList = []
    for band in range( NumBands ):
        band += 1
        if aUpdate:
            if band >1:
                GdalOutputBandPointerList.append(GdalOutputFilePointerList[band - 2].GetRasterBand(1))
                GdalOutputBandPointerList[band -2].SetNoDataValue(0)
        else:
            GdalOutputBandPointerList.append(GdalOutputFilePointerList[band - 1].GetRasterBand(1))
            GdalOutputBandPointerList[band -1].SetNoDataValue(0)


    # print 'Rows, Cols: ',rows, cols

    BlockId = 0
    for i in range(0, rows, yBSize):
        if i + yBSize < rows:
            numRows = yBSize
        else:
            numRows = rows - i
        # Backup i and numRows
        numRowsDefault = numRows
        iOriginal = i
        for j in range(0, cols, xBSize):
            i = iOriginal
            numRows = numRowsDefault
            print '\n Block numero : ', BlockId
            if j + xBSize < cols:
                numCols = xBSize
            else:
                numCols = cols - j
            numColsDefault = numCols

            # print 'Indice i,j original : ', i, j
            # print 'numRows numCols :',numRows, numCols
            # Test for applying sliding window buffer
            # Backup j
            jOriginal = j

            iOffset = 0
            jOffset = 0
            numColsOffset = 0
            numRowsOffset = 0
            if i - aWindowSize >= 0:
                i = i - aWindowSize
                iOffset = aWindowSize
            if j - aWindowSize >= 0:
                j = j - aWindowSize
                jOffset = aWindowSize

            if jOriginal + numCols + aWindowSize <= cols:
                numCols = numCols + aWindowSize
                numColsOffset = aWindowSize
            numCols = numCols + jOffset

            if iOriginal + numRows + aWindowSize <= rows:
                numRows = numRows + aWindowSize
                numRowsOffset = aWindowSize
            numRows = numRows + iOffset

            # print 'Read as array j, i, numCols, numRows', j, i, numCols, numRows
            Data = np.zeros(shape=(NumBands, numRows, numCols ),dtype=np.float)

            #We get file values
            for iB, indexB in enumerate(SortIndexVRTTimeList):
                # print 'SortIndexVRTTimeList, iB,indexB',SortIndexVRTTimeList, iB,indexB
                Data[iB, :,:] = GdalInputBandPointerList[iB].ReadAsArray(j, i, numCols, numRows)
                # print '\n mediane input ', VRTFNameList[indexB], np.median(Data[iB, :,:])
            '''


            Do something like

            '''

            # Calculate exponential to enhance dynamics
            if MedianFiltering:
                ExpRefSmooth = np.array(aRefRadiometry)[:, None, None] - Data
                ExpRefSmooth = MedianTimeFiltering(np.exp(ExpRefSmooth),3)
                # ExpRefSmooth = MedianTimeFiltering(np.array(aRefRadiometry)[:, None, None] - Data,3)
            else:
                ExpRefSmooth = np.array(aRefRadiometry)[:, None, None] - Data
                ExpRefSmooth = np.exp(ExpRefSmooth)

            # ExpRefSmooth = np.exp(ExpRefSmooth)

            # Apply EWMA
            ExpRefSmooth = EMWA(ExpRefSmooth, aAlpha)

            # Came back to original data space
            # print 'aAverage_Forest_dB_Value, np.mean(aRefRadiometry)',aAverage_Forest_dB_Value, np.mean(aRefRadiometry)
            if aAverage_Forest_dB_Value == 0.:
                ExpRefSmooth = np.mean(aRefRadiometry)[np.newaxis,np.newaxis,np.newaxis] - np.log(ExpRefSmooth)
            else:
                ExpRefSmooth =  np.array(aAverage_Forest_dB_Value)[np.newaxis,np.newaxis,np.newaxis] - np.log(ExpRefSmooth)

            #Clip the border
            ExpRefSmooth = ExpRefSmooth[:,iOffset:iOffset + numRowsDefault,jOffset:jOffset + numColsDefault]

            Data = Data[:,iOffset:iOffset + numRowsDefault,jOffset:jOffset + numColsDefault]

            # We mask output using original data
            # ExpRefSmooth[Data == 0] = 0.

            #We writte Output
            for band in range( NumBands ):
                band += 1
                if aUpdate:
                    if band >1:
                        bandP = GdalOutputFilePointerList[band - 2].GetRasterBand(1)
                        bandP.SetNoDataValue(0)
                        bandP.WriteArray(ExpRefSmooth[band-1,:,:],jOriginal,iOriginal)
                else:
                    bandP = GdalOutputFilePointerList[band - 1].GetRasterBand(1)
                    bandP.SetNoDataValue(0)
                    # bandP.WriteArray(ExpRef[band-1,:,:],jOriginal,iOriginal)
                    bandP.WriteArray(ExpRefSmooth[band-1,:,:],jOriginal,iOriginal)

            BlockId = BlockId + 1

    # We close all file
    for index, item in enumerate(GdalInputBandPointerList):
        GdalInputBandPointerList[index] = None

    # We close all file
    for index, item in enumerate(GdalOutputBandPointerList):
        GdalOutputBandPointerList[index] = None

    src_ds = None

    for index, item in enumerate(GdalOutputFilePointerList):
        GdalOutputFilePointerList[index] = None


def GetNewDatesFromListFiles(aInputList, aInputDir, aOutputDir):
    # List all unique date already processed (output folder)
    AllOutputFiles = directory_liste_ext_files(aOutputDir, 'tiff') +  directory_liste_ext_files(aOutputDir, 'tif')
    AllOutputFiles = [ file.replace("\\","/") for file in AllOutputFiles]
    # List all unique date in input file
    AllInputFiles = directory_liste_ext_files(aInputDir, 'tiff') +  directory_liste_ext_files(aInputDir, 'tif')
    AllInputFiles = [ file.replace("\\","/") for file in AllInputFiles if 'TempProcStack' not in file]

    # print '\n\nAllOutputFiles',AllOutputFiles
    # print '\n\nAllInputFiles',AllInputFiles

    ListDates = [getDayFromS1File(RastPath) for RastPath in AllOutputFiles ]
    UniqueOutputDates = list(set(ListDates))
    UniqueOutputDates.sort()

    ListDates = [getDayFromS1File(RastPath) for RastPath in AllInputFiles ]
    UniqueInputDates = list(set(ListDates))
    UniqueInputDates.sort()


    # Get New dates
    NewDates = [x for x in UniqueInputDates if x not in UniqueOutputDates]

    print 'UniqueInputDates, UniqueOutputDates, NewDates', UniqueInputDates, UniqueOutputDates, NewDates
    # Filter the data

    NewList = []
    for NDate in NewDates:
        for item in aInputList:
            if NDate in item:
                NewList.append(item.replace("\\","/"))

    return NewList

def GetNewDatesAndOldOneFromListFiles(aInputList, aInputDir, aOutputDir):
    # List all unique date already processed (output folder)
    AllOutputFiles = directory_liste_ext_files(aOutputDir, 'tiff') +  directory_liste_ext_files(aOutputDir, 'tif')
    AllOutputFiles = [ file.replace("\\","/") for file in AllOutputFiles if 'TempProcStack' not in file]
    # List all unique date in input file
    AllInputFiles = directory_liste_ext_files(aInputDir, 'tiff') +  directory_liste_ext_files(aInputDir, 'tif')
    AllInputFiles = [ file.replace("\\","/") for file in AllInputFiles if 'TempProcStack' not in file]

    ListDates = [getDayFromS1File(RastPath) for RastPath in AllOutputFiles ]
    UniqueOutputDates = list(set(ListDates))
    UniqueOutputDates.sort()

    ListDates = [getDayFromS1File(RastPath) for RastPath in AllInputFiles ]
    UniqueInputDates = list(set(ListDates))
    UniqueInputDates.sort()

    # Get New dates
    NewDates = [x for x in UniqueInputDates if x not in UniqueOutputDates]

    # Add the new one to the list
    # print 'UniqueInputDates, UniqueOutputDates, NewDates', UniqueInputDates, UniqueOutputDates, NewDates

    # Filter the data
    NewList = []
    for NDate in NewDates:
        for item in aInputList:
            if NDate in item:
                NewList.append(item)

    # print 'NewList', NewList
    # We add the already processed
    OldestNewDataIndex = UniqueInputDates.index(NewDates[0])
    OldestNewData = UniqueInputDates[OldestNewDataIndex - 1]

    for item in AllOutputFiles:
        if OldestNewData in item:
            NewList.append(item.replace("\\","/"))
            # print '\n\nitem append, item, NewList', item, NewList
    NewList.sort()

    return NewList

def GenerateDualPolColorcompositiondB(aSubfolders, aOutputFolder):
    # Loop thru different S1 data (folder)
    for Folder in aSubfolders:
        # List all tif and tiff files
        AllTifFile = directory_liste_ext_files(Folder, 'tif')

        InDirName = os.path.split(Folder)[1]

        # Create Output subfolder
        OutFolder = os.path.join(aOutputFolder,InDirName)
        if not os.path.exists(OutFolder):
            os.makedirs(OutFolder)

        Dual = False
        if '1SDV' in Folder:
            CopolFile = [s for s in AllTifFile if "VV" in s][0]
            CrosspolFile = [s for s in AllTifFile if "VH" in s][0]
            Dual = True

        elif '1SDH' in Folder:
            CopolFile = [s for s in AllTifFile if "HH" in s][0]
            CrosspolFile = [s for s in AllTifFile if "HV" in s][0]
            Dual = True
        else:
            print 'Not dual pol data'

        if Dual:
            CopolFileName = os.path.basename(os.path.splitext(CopolFile)[0])

            if '1SDV' in Folder:
                BaseNameFile = CopolFileName.replace('VV','')
                BaseNameFile = os.path.basename(os.path.splitext(BaseNameFile)[0])

                DiffFile = os.path.join(OutFolder,  BaseNameFile + '_VHdB-VVdB' +'.tif')

                OutputColorCompVRTFile = os.path.join(OutFolder,  BaseNameFile + 'VVdB_VHdB_HVdB-VVdB' +'.vrt')
            else:
                BaseNameFile = CopolFileName.replace('HH','')
                BaseNameFile = os.path.basename(os.path.splitext(BaseNameFile)[0])

                DiffFile = os.path.join(OutFolder,  BaseNameFile + 'HVdB-HHdB' +'.tif')

                OutputColorCompVRTFile = os.path.join(OutFolder,  BaseNameFile + 'HHdB_HVdB_HVdB-HHdB' +'.vrt')

            # test if file is already existing
            if not os.path.exists(DiffFile):
                DiffdB(CopolFile, CrosspolFile, DiffFile, Ram)

            # VRT color composition
            if not os.path.exists(OutputColorCompVRTFile):
                GdalBuildVRT([CopolFile, CrosspolFile, DiffFile], OutputColorCompVRTFile,  -30, -30)

# OTB Create HVdB - HHdB
def DiffdB(aInputFileCopol, aInputFileCrosspol, OutputFile, Ram):
    cmd = "otbcli_BandMath -il "
    cmd += aInputFileCopol + " "
    cmd += aInputFileCrosspol + " "
    cmd += " -ram " + str(Ram)
    cmd += " -out "
    cmd +="\"" + OutputFile + "?&gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES&gdal:co:TILED=YES\""
    cmd += " -exp "
    cmd += "\" im2b1 - im1b1 \""

    # progress.setInfo(cmd)
    # print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

def getDateFromS1Raster(aFileName):
    aFileName = aFileName.split("/")[-1]


    pos=max([aFileName.find('_2015'),aFileName.find('_2016'),aFileName.find('_2017'),aFileName.find('_2018'),aFileName.find('_2019'),aFileName.find('_2020')])
    return aFileName[pos+1:pos+9]

# Program
#
'''
1 - Scan copol and sc
'''
# create main tmp dir
TmpDir = os.path.join(Output_Folder, "tMp").replace("\\","/")
if not os.path.exists(TmpDir):
    os.makedirs(TmpDir)



AllTifFile = directory_liste_ext_files(Input_Folder, 'tif')
AllTifFile = [ file.replace("\\","/") for file in AllTifFile if 'TempProcStack' not in file]
AllTifFileNumber = len(AllTifFile)

AllTifFile = GetNewDatesFromListFiles(AllTifFile, Input_Folder, Output_Folder)
RemainingAllTifFileNumber = len(AllTifFile)



# Test if data was already EWMA in output folder
ContinueFiltering = False

# print 'AllTifFileNumber, RemainingAllTifFileNumber', AllTifFileNumber, RemainingAllTifFileNumber
# sys.exit("FIN")
if AllTifFileNumber == RemainingAllTifFileNumber:
    ContinueFiltering = False
else:
    ContinueFiltering = True
    # We compute the new list that contain new one and one oldest one
    AllTifFile = directory_liste_ext_files(Input_Folder, 'tif')
    AllTifFile = [ file.replace("\\","/") for file in AllTifFile if 'TempProcStack' not in file]
    # print 'before Update, AllTifFile',AllTifFile
    AllTifFile = GetNewDatesAndOldOneFromListFiles(AllTifFile, Input_Folder, Output_Folder)
    # print 'Update, AllTifFile',AllTifFile


# Filter Co and Crosspol file
if 'nt' in os.name:
    AllCopolFile = [file.replace("\\","/") for file in AllTifFile if ('_VV_' in file) or  ('_HH_' in file)]
    AllCrosspolFile = [file.replace("\\","/") for file in AllTifFile if ('_HV_' in file) or  ('_VH_' in file)]
else:
    AllCopolFile = [file for file in AllTifFile if ('_VV_' in file) or  ('_HH_' in file)]
    AllCrosspolFile = [file for file in AllTifFile if ('_HV_' in file) or  ('_VH_' in file)]

# Sort list to have ordered time
AllCopolFile.sort()
AllCrosspolFile.sort()

# Name of VRT files
CopolVrt = os.path.join(TmpDir, "Copol.vrt").replace("\\","/")
CrosspolVrt = os.path.join(TmpDir, "Crosspol.vrt").replace("\\","/")

# print CopolVrt, CrosspolVrt

# print AllCopolFile
# print AllCrosspolFile
# Build VRT
GdalBuildVRT(AllCopolFile, CopolVrt,  0, 0)
GdalBuildVRT(AllCrosspolFile, CrosspolVrt,  0, 0)

# print 'avant radiometry'

# Process Copol
InputTimeList, RefRadList = GetRefTimeRadiometry(CopolVrt, Input_Vector, Category_Field_Name, RefClassId)
# Comupte Average_Forest_dB_Value if necessary (first time)
if Average_Forest_dB_Value_Copol == 0:
    Average_Forest_dB_Value_Copol = np.mean(RefRadList)


# We sort the RefRadList
SortIndexRefRadTimelist = np.argsort(InputTimeList)
SortedRefRadList = [ RefRadList[index]  for index in SortIndexRefRadTimelist]
SortedTimeList = [ InputTimeList[index]  for index in SortIndexRefRadTimelist]

# Writte ref radiometry in csv to know the value
# First Create DF
DFColsName = ['Date','Sigma0dB']
df = pd.DataFrame(columns=DFColsName)
df['Date'] = ['Average_Forest_dB_Value'] + SortedTimeList
df['Sigma0dB'] = [Average_Forest_dB_Value_Copol] + SortedRefRadList

# Save the dataframe
# Get Date
FileDates = [ getDateFromS1Raster(day) for day in AllCopolFile]
FileDates.sort()

OutputCsv =  os.path.join(Output_Folder, "Copol_ReferenceRadiometry_From_" + FileDates[0] + '_To_' + FileDates[-1] + '.csv')
df.to_csv(OutputCsv)

BlockSize = int(np.sqrt(float(Ram) * np.power(1024,2) /(4. * 2. *(5. * float(len(AllCopolFile))))))
# BlockSize = 1024

TileRasterProcessingEWMA(CopolVrt, Output_Folder, BlockSize,0, Alpha, SortedRefRadList, Average_Forest_dB_Value_Copol, ContinueFiltering)


# Process Crosspol
InputTimeList, RefRadList = GetRefTimeRadiometry(CrosspolVrt, Input_Vector, Category_Field_Name, RefClassId)
# Comupte Average_Forest_dB_Value if necessary (first time)
if Average_Forest_dB_Value_Crosspol == 0:
    Average_Forest_dB_Value_Crosspol = np.mean(RefRadList)


# We sort the RefRadList
SortIndexRefRadTimelist = np.argsort(InputTimeList)
SortedRefRadList = [ RefRadList[index]  for index in SortIndexRefRadTimelist]
SortedTimeList = [ InputTimeList[index]  for index in SortIndexRefRadTimelist]

# Writte ref radiometry in csv to know the value
# First Create DF
DFColsName = ['Date','Sigma0dB']
df = pd.DataFrame(columns=DFColsName)
df['Date'] = ['Average_Forest_dB_Value'] + SortedTimeList
df['Sigma0dB'] = [Average_Forest_dB_Value_Crosspol] + SortedRefRadList

# Save the dataframe
OutputCsv =  os.path.join(Output_Folder, "Crosspol_ReferenceRadiometry_From_" + FileDates[0] + '_To_' + FileDates[-1] + '.csv')
df.to_csv(OutputCsv)

TileRasterProcessingEWMA(CrosspolVrt, Output_Folder, BlockSize,0, Alpha, SortedRefRadList, Average_Forest_dB_Value_Crosspol, ContinueFiltering)

# Remove TMPDir
shutil.rmtree(TmpDir)

# Generate ratio intensity or convert in dB and creat vrt for color composition
# List all folders in Time Filtering forlder
# Get full path of time filtering folder
SubFolders = get_immediate_subdirectories(Output_Folder)
GenerateDualPolColorcompositiondB(SubFolders, Output_Folder)
