##Sentinel-1 IW Additional Batch Processing=group
##Clip Rasters from folder to other one=name
##Input_Data_Folder=folder
##Input_Polygon_File=vector
##Output_EPSG=crs
##Output_Resolution=number 10
##No_Data_Value=number 0
##Output_Data_Folder=folder
##Ram=number 256

'''
Auteur (Avril 2017)
Cedric Lardeux clardeux@gmail.com cedric.lardeux@onfinternational.com
Pierre-Louis Frison pierre-louis.frison@u-pem.fr
'''

import os, os.path
import subprocess

# This script is dedicated to orthorectified Sentinel-1 data in GRD IW mode and dual pol (VV, VH or HH, VH)
# It's based on OTB 5.6

#Input_Data_Folder='/media/clardeux/CL/livre_QGIS/CompocolLee7'
#Input_Polygon_File = '/media/clardeux/CL/livre_QGIS/CotriTSDeforestatioTest/Output/Alpha0.1/area.shp'
#Output_Resolution= 10
#Output_EPSG= 'EPSG:32621'
#Output_Data_Folder='/media/clardeux/CL/livre_QGIS/CotriTSDeforestatioTest/Test2/Clip'
# Output_in_dB= False
#Ram=1000

'''
INPUT
'''

DataFolder = Input_Data_Folder
OutputFolder = Output_Data_Folder

'''
This function list this immediate subdirectories in one given directory
'''
def get_immediate_subdirectories(a_dir):
    return [os.path.join(a_dir, name) for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

'''
This function list all file with a given file extention in one directory
'''
def directory_liste_ext_files(directory, filt_ext):
    list_file = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in filenames:
            if filename.endswith((filt_ext)):
                list_file.append(os.path.join(root, filename))

    list_file.sort()
    return list_file

def GdalClipRasterWithVector(aInputRaster, aInputVector, aOutputFile, aOutputResolution, aSRCNoData, aOutEPSG, aRam):
    cmd = "gdalwarp "
    cmd += "--config GDAL_CACHEMAX " + str(aRam) + " -multi -wo NUM_THREADS=val/ALL_CPUS "
    cmd += " -tr " + str(aOutputResolution) + ' ' + str(aOutputResolution) + ' '
    cmd += " -t_srs " + str(aOutEPSG)
    cmd += " -r average -q -multi -crop_to_cutline -cutline "
    cmd += aInputVector
    cmd += ' -co COMPRESS=DEFLATE -co PREDICTOR=2 '
    cmd += " -dstnodata " + str(aSRCNoData)
    cmd += " -of GTiff " + aInputRaster + ' ' + aOutputFile
    
    # progress.setInfo(cmd)
    # print cmd
    
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]



'''
Program
'''
# Stack Process
# List all SENTINEL-1 sub directories
SubFolders = get_immediate_subdirectories(DataFolder)

# Clip the data
#
# Loop thru different S1 data (folder) to create common shape file footprint
for Folder in SubFolders:
    # List all tif and tiff files
    AllTifFile = directory_liste_ext_files(Folder, 'tif')

    InDirName = os.path.split(Folder)[1]

    # Create Output subfolder
    OutFolder = os.path.join(OutputFolder,InDirName)
    if not os.path.exists(OutFolder):
        os.makedirs(OutFolder)

    for file in AllTifFile:
        FileName = os.path.basename(os.path.splitext(file)[0])
        OutputFile = os.path.join(OutFolder,  FileName +'.tif')

        GdalClipRasterWithVector(file,Input_Polygon_File , OutputFile, Output_Resolution,  No_Data_Value, Output_EPSG, Ram)
