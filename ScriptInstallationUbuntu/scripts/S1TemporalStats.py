##Sentinel-1 IW Additional Batch Processing=group
##Time Series Statistics=name
##Input_Data_Folder=folder
##Start_Analysis_Date=string 20160124
##End_Analysis_Date=string 20161123
##Output_Data_Folder=folder
##Ram=number 256

# Authors: Cedric Lardeux et Pierre-Louis Frison

import sys, shutil
import os, os.path
import subprocess
from osgeo import gdal
import numpy as np

# This script is dedicated to orthorectified Sentinel-1 data in GRD IW mode and dual pol (VV, VH or HH, VH)
# It's based on OTB 5.6

Input_Data_Folder='/home/DataProcessing/FrenchGuyana/OptCont/p120'
Output_Data_Folder='/home/DataProcessing/FrenchGuyana/OptCont/p120Stats'
Start_Analysis_Date = '20170106'
End_Analysis_Date = '20180206'
Ram=20000


Begin_Date = Start_Analysis_Date
End_Date = End_Analysis_Date

'''
INPUT
'''

DataFolder = Input_Data_Folder
OutputFolder = Output_Data_Folder

def DelDirAndItsFiles(aInputDir):
    for the_file in os.listdir(aInputDir):
        DelFile = os.path.join(aInputDir, the_file)
        if os.path.exists(DelFile):
            os.remove(DelFile)
    os.removedirs(aInputDir)

def DelDirFiles(aInputDir):
    for the_file in os.listdir(aInputDir):
        DelFile = os.path.join(aInputDir, the_file)
        if os.path.exists(DelFile):
            os.remove(DelFile)
'''
This function list this immediate subdirectories in one given directory
'''
def get_immediate_subdirectories(a_dir):
    return [os.path.join(a_dir, name).replace("\\","/") for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

'''
This function list all file with a given file extention in one directory
'''
def directory_liste_ext_files(directory, filt_ext):
    list_file = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in filenames:
            if filename.endswith((filt_ext)):
                myfile = os.path.join(root, filename)
                list_file.append(myfile.replace("\\","/"))

    list_file.sort()
    return list_file

def mad(a, c=1.4826, axis=None):
    """
    Compute *Median Absolute Deviation* of an array along given axis.
    """

    # Median along given axis, but *keeping* the reduced axis so that
    # result can still broadcast against a.
    med = np.median(a, axis=axis, keepdims=True)
    mad = c * np.median(np.absolute(a - med), axis=axis)  # MAD along given axis

    return mad

def GdalBuildVRT(aInputListFile, aOutputFile,  aSRCNoData, aVRTNoData):
    TmpListfile = aOutputFile.replace('.vrt', 'TMPListFile.txt')
    fic=open(TmpListfile,'w')
    for file in aInputListFile:
        fic.write(file +'\n')
    fic.close()    

    cmd = "gdalbuildvrt "
    cmd += " -separate -overwrite "
    cmd += " -srcnodata " + str(aSRCNoData)
    cmd += " -vrtnodata " + str(aVRTNoData)
    cmd += " " + aOutputFile
    cmd += " -input_file_list " + TmpListfile
    cmd += " " + aOutputFile

    # progress.setInfo(cmd)
    # print cmd
    
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

    if os.path.exists(TmpListfile):
        os.remove(TmpListfile)

'''
This function do orthorectification of any optical/radar file
aDEMDir     --> directory that contain DEM

opt.gridspacing is not in parameter function but need to be near 4 to 10 times of output pixel size
For example, with 10m output pixel size, you can chose bettween 40 to 100.
More less is the value more accurate is the results but more long
In addition this value have to be linked to DEM pixel size
'''
def Rasterize(aInputVector, aOutputRaster, aOutPixelSize):
    # Multilook
    cmd = "gdal_rasterize "
    cmd += " -burn 1 "
    cmd += " -of GTiff "
    cmd += " -a_nodata 0 "
    cmd += " -a_srs EPSG:3857 "
    cmd += " -tr " + str(aOutPixelSize) + " "+ str(aOutPixelSize)+ " "
    cmd += aInputVector + " "
    cmd += aOutputRaster

    # print cmd
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

def TiffTimeToPanda(aInputRaster):
    # We open raster and get only band 1
    src_ds = gdal.Open( aInputRaster )
    if src_ds is None:
        print 'Could not open ' + fn
        sys.exit(1)

    # Get metadata in datasource
    time = src_ds.GetMetadata().get("TIFFTAG_DATETIME")

    src_ds = None

    return "-".join(time.split(':'))


def GetAcquistionTimeFromSentinel1FileName(aFileName):
    aFileName = aFileName.split("/")[-1]


    pos=max([aFileName.find('_2015'),aFileName.find('_2016'),aFileName.find('_2017'),aFileName.find('_2018'),aFileName.find('_2019'),aFileName.find('_2020')])
    return aFileName[pos+1:pos+9]

def TileRasterProcessing(aInputRasterListPath, aOutputFolder, aBlockSize, aPolarizationName):
    '''
    TODO: Add sliding window parameter


    This method show how to process huge raster
    using easy tilling based on block size
    and if necessary taking into account window size
    when necessary in processing like filtering for example
    aInputRasterPath string --> Contain input raster path to process
    aOutputRasterPath string --> Contain output raster path to process
    aBlockSize integer --> Contain the size of square block to Tile
    aWindowSize integer --> Contain windows size for sliding window (0 size by default)
    '''

    aWindowSize = 0

    # we put X and Y block size based on aBlockSize
    xBSize = aBlockSize
    yBSize = aBlockSize
    
    # Prepare Output name file
    # Stats files
    OutputRasterStatListPath = []
    StatNames = ['Median','Mean','Mad','Stdev','Min','Max','P10','P90','P25','P75']
    Suffix = '_From_' + Begin_Date + '_To_'+ End_Date
    for name in StatNames:
        OutputRasterStatListPath.append(os.path.join(aOutputFolder,name + '_' + str(aPolarizationName) + Suffix + '.tif').replace("\\","/"))
    OutputVrt = os.path.join(aOutputFolder, '-'.join(StatNames) + '_' + str(aPolarizationName) + Suffix + '.vrt').replace("\\","/")

    NumBands = len(aInputRasterListPath)

    # We open one raster to get rows and cols
    src_ds = gdal.Open( aInputRasterListPath[0] )
    if src_ds is None:
        print 'Could not open ' + fn
        sys.exit(1)

    # We get number of row and cols
    rows = src_ds.RasterYSize
    cols = src_ds.RasterXSize

    # We force Float32 to read data
    BandType = gdal.GDT_Float32

    # We get Projection from input raster
    InputGeoTransform = src_ds.GetGeoTransform()
    InputProjection = src_ds.GetProjection()

    src_ds = None

    # Open input files
    GdalFilePointerList = []
    GdalBandFilePointerList = []
    for i in range(NumBands):
        GdalFilePointerList.append(gdal.Open( aInputRasterListPath[i] ))
        GdalBandFilePointerList.append(GdalFilePointerList[i].GetRasterBand(1))

    # Create Output file
    # Median - Mean - Mad - Stdev - Min - Max - Q10 - Q90 - Q25 - Q75
    format = "GTiff"  
    driver = gdal.GetDriverByName( format )
    GdalOutputStatFilePointerList = []
    GdalOutputStatFileBandPointerList = []
    for i in range(len(OutputRasterStatListPath)):
        file = OutputRasterStatListPath[i]
        GdalOutputStatFilePointerList.append(driver.Create(file, cols, rows, 1, BandType,  options = [ 'COMPRESS=DEFLATE', 'PREDICTOR=3', 'ZLEVEL=9', 'BIGTIFF=YES' ] ))
        GdalOutputStatFilePointerList[i].SetGeoTransform(InputGeoTransform)  
        GdalOutputStatFilePointerList[i].SetProjection(InputProjection)
        
        GdalOutputStatFileBandPointerList.append(GdalOutputStatFilePointerList[i].GetRasterBand(1))
        GdalOutputStatFileBandPointerList[i].SetNoDataValue(0)
        

    # print 'Rows, Cols: ',rows, cols
    BlockId = 0
    for i in range(0, rows, yBSize):
        if i + yBSize < rows:
            numRows = yBSize
        else:
            numRows = rows - i
        # Backup i and numRows
        numRowsDefault = numRows
        iOriginal = i
        for j in range(0, cols, xBSize):
            i = iOriginal
            numRows = numRowsDefault
            # print '\n Block numero : ', BlockId
            if j + xBSize < cols:
                numCols = xBSize
            else:
                numCols = cols - j
            numColsDefault = numCols
            
            # print 'Indice i,j original : ', i, j
            # print 'numRows numCols :',numRows, numCols
            # Test for applying sliding window buffer
            # Backup j
            jOriginal = j

            iOffset = 0
            jOffset = 0
            numColsOffset = 0
            numRowsOffset = 0
            if i - aWindowSize >= 0:
                i = i - aWindowSize
                iOffset = aWindowSize
            if j - aWindowSize >= 0:
                j = j - aWindowSize
                jOffset = aWindowSize

            if jOriginal + numCols + aWindowSize <= cols:
                numCols = numCols + aWindowSize
                numColsOffset = aWindowSize
            numCols = numCols + jOffset
            
            if iOriginal + numRows + aWindowSize <= rows:
                numRows = numRows + aWindowSize
                numRowsOffset = aWindowSize
            numRows = numRows + iOffset

            # print 'Read as array j, i, numCols, numRows', j, i, numCols, numRows
            Data = np.zeros(shape=(NumBands, numRows, numCols ),dtype=np.float)

            #We get file values
            for it in range(len(GdalFilePointerList)):
                Data[it, :,:] = GdalBandFilePointerList[it].ReadAsArray(j, i, numCols, numRows)
            '''

            Do something like

            '''

            #Clip the border due to window
            Data = Data[:,iOffset:iOffset + numRowsDefault,jOffset:jOffset + numColsDefault]

            #We writte Stats
            # Median
            GdalOutputStatFileBandPointerList[0].WriteArray(np.median(Data, axis=0),jOriginal,iOriginal)

            # Mean
            GdalOutputStatFileBandPointerList[1].WriteArray(np.mean(Data, axis=0),jOriginal,iOriginal)

            # MAD
            GdalOutputStatFileBandPointerList[2].WriteArray(mad(Data, axis=0),jOriginal,iOriginal)

            # Stdev
            GdalOutputStatFileBandPointerList[3].WriteArray(np.std(Data, axis=0),jOriginal,iOriginal)

            # Min
            GdalOutputStatFileBandPointerList[4].WriteArray(np.amin(Data, axis=0),jOriginal,iOriginal)

            # Max
            GdalOutputStatFileBandPointerList[5].WriteArray(np.amax(Data, axis=0),jOriginal,iOriginal)

            # P10
            GdalOutputStatFileBandPointerList[6].WriteArray(np.percentile(Data, 10,  axis=0),jOriginal,iOriginal)

            # P90
            GdalOutputStatFileBandPointerList[7].WriteArray(np.percentile(Data, 90,  axis=0),jOriginal,iOriginal)

            # P25
            GdalOutputStatFileBandPointerList[8].WriteArray(np.percentile(Data, 25,  axis=0),jOriginal,iOriginal)

            # P75
            GdalOutputStatFileBandPointerList[9].WriteArray(np.percentile(Data, 75,  axis=0),jOriginal,iOriginal)


            BlockId = BlockId + 1

    # We close all file
    # Close sources
    src_ds = None

    for band in range( len(GdalFilePointerList) ):
        GdalBandFilePointerList[band] = None
        GdalFilePointerList[band] = None
    
    # Close Output Stats
    for band in range(len(GdalOutputStatFilePointerList)):
        GdalOutputStatFileBandPointerList[band] = None
        GdalOutputStatFilePointerList[band] = None
        

    # Create vrt
    # print 'OutputRasterStatListPath, OutputVrt', OutputRasterStatListPath, OutputVrt
    GdalBuildVRT(OutputRasterStatListPath, OutputVrt,  0, 0)

def getDateFromS1Raster(aFileName):
    aFileName = aFileName.split("/")[-1]


    pos=max([aFileName.find('_2015'),aFileName.find('_2016'),aFileName.find('_2017'),aFileName.find('_2018'),aFileName.find('_2019'),aFileName.find('_2020')])
    return aFileName[pos+1:pos+9]

'''
Program
'''
AllTifFile = directory_liste_ext_files(DataFolder, 'tif')

# Sort Files
AllTifFile.sort()


# Filter the file to take only dates that begin at Stard date
UpdateInputList = []
for file in AllTifFile:
    dateFile = getDateFromS1Raster(file)
    if int(dateFile) >= int(Begin_Date) and int(dateFile) <= int(End_Date):
        UpdateInputList.append(file.replace("\\","/"))

AllCopolFile = [file.replace("\\","/") for file in UpdateInputList if ('_VV_' in file) or  ('_HH_' in file)]
AllCrosspolFile = [file.replace("\\","/") for file in UpdateInputList if ('_HV_' in file) or  ('_VH_' in file)]

# print 'AllCrosspolFile',AllCrosspolFile
# progress.setInfo(AllCrosspolFile)

NumDate = len(AllCopolFile)

BlockSize = int(np.sqrt(float(Ram) * np.power(1024,2) /(4. * 2. *(3. * float(NumDate) + 1.))))

# Apply Stats to HV
TileRasterProcessing(AllCrosspolFile, OutputFolder, BlockSize, 'VH')

# Apply Stats to VV
TileRasterProcessing(AllCopolFile, OutputFolder, BlockSize,'VV')