##Raster=group
##Advanced Resampling using Resolution parameter=name
##Input_Raster_File=raster
##No_Data=number 0
##Output_Resolution=number 10.
##Resample_Method_near_bilinear_average=string bilinear
##Output_CRS=crs
##Raster_Output_File_Tiff=output raster
from osgeo import gdal
import os, subprocess


#ouverture du fichier input
src_ds = gdal.Open( Input_Raster_File )
if src_ds is None:
	print 'Could not open ' + fn
	sys.exit(1)
else:
	# reprojection
	print ""
	cmd = "gdalwarp  -dstnodata "
	cmd += str(No_Data)
	cmd += " -srcnodata " + str(No_Data) + " "
	cmd += " -multi "
	cmd += " -r " + Resample_Method_near_bilinear_average +" "
	cmd += " -of GTiff "
	cmd += " -tr " + str(Output_Resolution) + " " + str(Output_Resolution) + " "
	cmd += "-t_srs " + str(Output_CRS) + " "
	cmd += Input_Raster_File
	cmd += " "
	cmd += Raster_Output_File_Tiff

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]