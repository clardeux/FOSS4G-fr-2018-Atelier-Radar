# -*- coding: utf-8 -*-
"""
Created on Wed Jun 28 10:48:01 2017

@author: cedric
"""
##Classification=group
##Sieve on Classification=name
##Input_Raster=raster
##Output_Raster=output raster
##Minimum_Agregate=number 10

import os, gdal, subprocess, shutil
 
#Input_Raster = '/media/cedric/CL/ONFGuyane/Livrables/SuiviRevegetatisation/Raster/ALOS2/ALOSMosaic_2016ClassifROI1.tif'
#Output_Raster = '/media/cedric/CL/ONFGuyane/Livrables/SuiviRevegetatisation/Raster/ALOS2/ALOSMosaic_2016ClassifROI1HA.tif'



#Minimum_Agregate = 10
 
tile_size_x = 5000
tile_size_y = 5000

def GdalSieve(aInputRaster, aOutputFile,  aPixelTh):
    if 'nt' in os.name:
        cmd = "gdal_sieve.bat   "
    else:
        cmd = "gdal_sieve.py   "
    cmd += " -nomask"
    cmd += " -st " + str(aPixelTh)
    cmd += " -of GTiff "
    cmd += " " + aInputRaster
    cmd += " " + aOutputFile
    
#    progress.setInfo(cmd)
#    print cmd
    
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]
    
    print 'cmd \n', cmd, '\nret', ret

def GdalBuildVRT(aInputListFile, aOutputFile,  aSRCNoData, aVRTNoData):
    TmpListfile = aOutputFile.replace('.vrt', 'TMPListFile.txt')
    fic=open(TmpListfile,'w')
    for file in aInputListFile:
        fic.write(file +'\n')
    fic.close() 

    cmd = "gdalbuildvrt "
    cmd += " -overwrite "
    cmd += " -srcnodata " + str(aSRCNoData)
    cmd += " -vrtnodata " + str(aVRTNoData)
    cmd += " " + aOutputFile
    cmd += " -input_file_list " + TmpListfile
    cmd += " " + aOutputFile

    # progress.setInfo(cmd)
    # print cmd
    
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]
    
    print 'cmd \n', cmd, '\nret', ret

    if os.path.exists(TmpListfile):
        os.remove(TmpListfile)

def GdalTranslate(aInputRaster, aOutputFile):
    cmd = "gdal_translate "
    cmd += " -of GTiff "
    cmd += " " + aInputRaster
    cmd += " " + aOutputFile
    
#    progress.setInfo(cmd)
#    print cmd
    
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]
    
    print 'cmd \n', cmd, '\nret', ret

DirOutputFile = os.path.dirname(Output_Raster).replace("\\","/")


# create tmp dir
TmpDir = os.path.join(DirOutputFile, "tMp").replace("\\","/")
if not os.path.exists(TmpDir):
    os.makedirs(TmpDir)
 
 # Create tile dir
OutTileDir =    os.path.join(TmpDir, "TileDir").replace("\\","/")
if not os.path.exists(OutTileDir):
    os.makedirs(OutTileDir)
output_tilename = 'tile_'

 # Create tile sieved dir
OutTileSievedDir =    os.path.join(TmpDir, "TileSieveDir").replace("\\","/")
if not os.path.exists(OutTileSievedDir):
    os.makedirs(OutTileSievedDir)
SievedMosaic = os.path.join(OutTileSievedDir, "SievedMosaic.vrt").replace("\\","/")

buff = Minimum_Agregate
 
ds = gdal.Open(Input_Raster)
band = ds.GetRasterBand(1)
xsize = band.XSize
ysize = band.YSize

OutTileFileList = []
OutTileSievedFileList = []
 
for i in range(0, xsize, tile_size_x):
    for j in range(0, ysize, tile_size_y):
        OutFileTile = os.path.join(OutTileDir,str(output_tilename) + str(i) + "_" + str(j) + ".vrt").replace("\\","/")
        OutFileTileSieved = os.path.join(OutTileSievedDir,str(output_tilename) + str(i) + "_" + str(j) + "SIEVED.tif").replace("\\","/")
        OutTileFileList.append(OutFileTile)
        
        OutTileSievedFileList.append(OutFileTileSieved)
        
        ix = i - buff
        jy = j - buff
        
        if ix < 0:
            ix = i
        
        if jy < 0:
            jy = j
            
                
        TileSizeX = tile_size_x + buff * 2
        TileSizeY = tile_size_y + buff * 2
        
        if ix + TileSizeX > xsize:
            TileSizeX = xsize - ix
        
        if jy + TileSizeY > ysize:
            TileSizeY = ysize - jy
        
        # Create the tile
        cmd = "gdal_translate -of VRT -srcwin  "
        cmd += str(ix)+ ", " + str(jy) + ", " + str(TileSizeX) + ", " + str(TileSizeY) + " "
        cmd += str(Input_Raster) + " "
        cmd += str(OutFileTile)
        
        p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
        ret= p1.communicate()[1]
        
        print 'cmd \n', cmd, '\nret', ret
        
        # Sieve the tile
        GdalSieve(OutFileTile, OutFileTileSieved,  Minimum_Agregate)
        
# Create mosaic of the sieved image in vrt
GdalBuildVRT(OutTileSievedFileList, SievedMosaic,  0, 0)

# Convert to tif
GdalTranslate(SievedMosaic, Output_Raster)

# del tmp dir
shutil.rmtree(TmpDir)
