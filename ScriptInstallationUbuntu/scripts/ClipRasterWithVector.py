##Raster=group
##Clip Rasters in a Folder using vector=name
##Output_Resolution=number 10
##Output_EPSG=crs
##Input_Vector_File=vector
##Input_Data_Folder=folder
##Raster_Extension_File_To_Clip=string .tif
##No_Data_Value=number 0
##Output_Data_Folder=folder


import sys
import glob, os, zipfile, os.path
import string
import subprocess
from datetime import datetime, date, time

# This script is dedicated to orthorectified Sentinel-1 data in GRD IW mode and dual pol (VV, VH or HH, VH)
# It's based on OTB 5.6
'''
INPUT
'''



def directory_liste_ext_files(directory, filt_ext):
	list_file = []
	for root, dirnames, filenames in os.walk(directory):
		for filename in filenames:
			if filename.endswith((filt_ext)):
				list_file.append(os.path.join(root, filename))

	list_file.sort()
	return list_file


def GdalClipRasterWithVector(aInputRaster, aInputVector, aOutputFile, aOutputResolution, aSRCNoData, aOutEPSG):
	cmd = "gdalwarp "
	cmd += " -tr " + str(aOutputResolution) + ' ' + str(aOutputResolution) + ' '
	cmd += " -t_srs " + aOutEPSG
	cmd += " -q -multi -crop_to_cutline -cutline "
	cmd += aInputVector
	cmd += " -dstnodata " + str(aSRCNoData)
	cmd += " -of GTiff " + aInputRaster + ' ' + aOutputFile
	
	# progress.setInfo(cmd)
	# print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]



'''
Program
'''
# List all SENTINEL-1 sub directories
TiffFileList = directory_liste_ext_files(Input_Data_Folder, Raster_Extension_File_To_Clip)

for tiff in TiffFileList:
	Name = os.path.splitext(os.path.basename(tiff))[0]
	OutPath = os.path.join(Output_Data_Folder, Name + '_Clip.tif')
	GdalClipRasterWithVector(tiff, Input_Vector_File, OutPath, Output_Resolution,  No_Data_Value, Output_EPSG)
