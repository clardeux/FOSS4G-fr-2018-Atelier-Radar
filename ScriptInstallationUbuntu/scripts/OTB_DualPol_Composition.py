##Radar=group
##Dual Pol Color Composition=name
##Input_Copol_Raster=raster
##Input_Crosspol_Raster=raster
##Copol_Raster_Band_Number=number 1
##Crosspol_Raster_Band_Number=number 1
##Input_Type_Int=boolean True
##Output_Raster=output raster
##Ram=number 256

'''
Auteur (Avril 2017)
Cedric Lardeux clardeux@gmail.com cedric.lardeux@onfinternational.com
Pierre-Louis Frison pierre-louis.frison@u-pem.fr
'''

import sys, shutil
import glob, os, zipfile, os.path
import string
import subprocess
from datetime import datetime, date, time

# This script is dedicated to orthorectified Sentinel-1 data in GRD IW mode and dual pol (VV, VH or HH, VH)
# It's based on OTB 5.6

#Input_Copol_Raster='/home/frison/A/data/TP/S1_Yaounde/processed_data/stack_3dates_9bandes_Spk.tif'
#Input_Crosspol_Raster='/home/frison/A/data/TP/S1_Yaounde/processed_data/stack_3dates_9bandes_Spk.tif'
#Copol_Raster_Band_Number=1
#Crosspol_Raster_Band_Number=2
#Input_Type_Int = True
#Output_Raster = '/home/frison/A/data/TP/S1_Yaounde/processed_data/Compocol.tif'
#Ram=1000

Input_Copol_Raster=str(Input_Copol_Raster)
Input_Crosspol_Raster=(Input_Crosspol_Raster)
'''
INPUT
'''

'''
This function list all file with a given file extention in one directory
'''
def directory_liste_ext_files(directory, filt_ext):
	list_file = []
	for root, dirnames, filenames in os.walk(directory):
		for filename in filenames:
			if filename.endswith((filt_ext)):
				list_file.append(os.path.join(root, filename))

	list_file.sort()
	return list_file

def Int2dB(InputFile, Band, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += InputFile + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\"" + Band + "==0 ? 0 : 10. * log10(" +Band +")\""

	# progress.setInfo(cmd)
	print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def OTBConcatenate(aInputListFile, aOutputFile, aRam):
	cmd = "otbcli_ConcatenateImages -il "
	for File in aInputListFile:
		cmd += " " + File
	cmd += " "
	cmd += " -ram " + str(aRam)
	cmd += " -out "
	cmd += aOutputFile

	# progress.setInfo(cmd)
	print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]
 
# OTB Create HVdB - HHdB
def DiffdB(aInputFileCopol, aInputFileCrosspol, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += aInputFileCopol + " "
	cmd += aInputFileCrosspol + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\" im2b1 - im1b1 \""

	# progress.setInfo(cmd)
	print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def ExtractBand(aInputFile, Band, OutputFile, Ram):
	cmd = "otbcli_BandMath -il "
	cmd += aInputFile + " "
	cmd += " -ram " + str(Ram)
	cmd += " -out "
	cmd += OutputFile
	cmd += " -exp "
	cmd += "\"" + Band + "\""

	# progress.setInfo(cmd)
	print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def GdalBuildVRT(aInputListFile, aOutputFile,  aSRCNoData, aVRTNoData):
	cmd = "gdalbuildvrt "
	cmd += " -separate "
	cmd += " -srcnodata " + str(aSRCNoData)
	cmd += " -vrtnodata " + str(aVRTNoData)
	cmd += " " + aOutputFile
	for File in aInputListFile:
		cmd += " " + File
	
	# progress.setInfo(cmd)
	print cmd
	
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

'''
Program
'''


OutputRasterFolder = os.path.dirname(Output_Raster)

# create tmp dir
TmpDir = os.path.join(OutputRasterFolder, "tmp")
if not os.path.exists(TmpDir):
	os.makedirs(TmpDir)

CopolFileName = os.path.basename(os.path.splitext(Input_Copol_Raster)[0])
CrosspolFileName = os.path.basename(os.path.splitext(Input_Crosspol_Raster)[0])


# Define TMP output file name
CopoldBFile = os.path.join(TmpDir,  CopolFileName + 'Co_dB' +'.tif')
CrosspoldBFile = os.path.join(TmpDir,  CrosspolFileName + 'X_dB' +'.tif')
DiffFile = os.path.join(TmpDir, 'CrossPoldB-CoPoldB' +'.tif')

if Input_Type_Int:
	Int2dB(Input_Copol_Raster, 'im1b' + str(Copol_Raster_Band_Number), CopoldBFile, Ram)
	Int2dB(Input_Crosspol_Raster, 'im1b' + str(Crosspol_Raster_Band_Number), CrosspoldBFile, Ram)
else:
	# Extract Copol dB
	ExtractBand(Input_Copol_Raster, 'im1b' + str(Copol_Raster_Band_Number), CopoldBFile, Ram)
	# Extract Crosspol dB
	ExtractBand(Input_Crosspol_Raster, 'im1b' + str(Crosspol_Raster_Band_Number), CrosspoldBFile, Ram)

DiffdB(CopoldBFile, CrosspoldBFile, DiffFile, Ram)
# Output
OTBConcatenate([CopoldBFile, CrosspoldBFile, DiffFile], Output_Raster, Ram)

shutil.rmtree(TmpDir)
