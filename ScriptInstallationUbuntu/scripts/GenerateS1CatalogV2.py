##Sentinel-1 Deforestation Process=group
##Generate S1 Footprint Catalog=name
##Input_Data_Folder=folder
##Output_Polygon_File=output vector

# Input_Data_Folder = 'F:/cedric/CL/ONFGuyane/Data/Sentinel1/TestScript/Input'
# Output_Polygon_File = 'F:/cedric/CL/ONFGuyane/Data/Sentinel1/TestScript/Input/dsze.shp'

Input_Data_Folder = Input_Data_Folder.replace("\\","/")
OutputShape = Output_Polygon_File.replace("\\","/")
'''
This script use codes from Thierry KOLECK (CNES)
'''

#-*- coding: utf-8 -*-
# =========================================================================
#   Program:   S1Processor
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
#
# Authors: Thierry KOLECK (CNES)
#
# =========================================================================
# from qgis.core import *
# import qgis.utils


import os, sys, glob, re, subprocess,shutil
from osgeo import gdal, ogr, osr
from datetime import datetime, date, time
import datetime as dT

def getOrigin(aManifestFile):
	with open(aManifestFile,"r") as saveFile:
		for line in saveFile:
			if "<gml:coordinates>" in line:
				coor = line.replace("                <gml:coordinates>","").replace("</gml:coordinates>","").split(" ")
				coord = [(float(val.replace("\n","").split(",")[0]),float(val.replace("\n","").split(",")[1]))for val in coor]

	return coord[0],coord[1],coord[2],coord[3]


def getRelativeOrbit(aManifestFile):
	# print str(aManifestFile)
	file = open(aManifestFile,"r")
	for line in file:
		if "<safe:relativeOrbitNumber type=\"start\">" in line:
			RelativeOrbit = int(line.replace("            <safe:relativeOrbitNumber type=\"start\">","").replace("</safe:relativeOrbitNumber>",""))
	file.close()
	return RelativeOrbit

'''
This function list this immediate subdirectories in one given directory
'''
def get_immediate_subdirectories(a_dir):
    return [os.path.join(a_dir, name) for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name).replace("\\","/"))]

'''
This function list all file with a given file extention in one directory
'''
def directory_liste_ext_files(directory, filt_ext):
    list_file = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in filenames:
            if filename.endswith((filt_ext)):
            	myfile = os.path.join(root, filename)
                list_file.append(myfile.replace("\\","/"))

    list_file.sort()
    return list_file

def getDateFromS1Folder(aFolderName):
	return aFolderName.split("/")[-1].split("_")[4]

def CreateS1ShapeFootPrint(aManifestFile,aOutputShapeFile):

	S1FootPrint = getOrigin(aManifestFile)
	poly = ogr.Geometry(ogr.wkbPolygon)
	ring = ogr.Geometry(ogr.wkbLinearRing)
	ring.AddPoint(S1FootPrint[0][1], S1FootPrint[0][0])
	ring.AddPoint(S1FootPrint[1][1], S1FootPrint[1][0])
	ring.AddPoint(S1FootPrint[2][1], S1FootPrint[2][0])
	ring.AddPoint(S1FootPrint[3][1], S1FootPrint[3][0])
	ring.AddPoint(S1FootPrint[0][1], S1FootPrint[0][0])
	poly.AddGeometry(ring)

	# Save extent to a new Shapefile
	outShapefile = aOutputShapeFile
	LayerName = str(os.path.basename(os.path.splitext(aOutputShapeFile)[0]))
	# LayerName = LayerName.encode('utf-8')
	print LayerName
	outDriver = ogr.GetDriverByName("ESRI Shapefile")

	# Remove output shapefile if it already exists
	if os.path.exists(outShapefile):
	    outDriver.DeleteDataSource(outShapefile)

	# Create the output shapefile
	outDataSource = outDriver.CreateDataSource(outShapefile)
	outLayer = outDataSource.CreateLayer(LayerName, geom_type=ogr.wkbPolygon)

	# Create the feature and set values
	featureDefn = outLayer.GetLayerDefn()
	feature = ogr.Feature(featureDefn)
	feature.SetGeometry(poly)
	outLayer.CreateFeature(feature)
	feature = None

	outDataSource = None

	srs = osr.SpatialReference()
	srs.ImportFromEPSG(4326)

	OutPrjFile = os.path.splitext(aOutputShapeFile)[0] + '.prj'
	srs.MorphToESRI()
	filePrj = open(OutPrjFile.replace("\\","/"), 'w')
	filePrj.write(srs.ExportToWkt())
	filePrj.close()


# Input_Data_Folder = '/media/clardeux/CL/livre_QGIS/Input'

# create tmp dir
TmpDir = os.path.join(Input_Data_Folder, "tmp_").replace("\\","/")
if not os.path.exists(TmpDir):
	os.makedirs(TmpDir)
# progress.setInfo('Bonjour 11')
# OutputShape = os.path.join(Input_Data_Folder, "S1Catalog.shp").replace("\\","/")
LayerName = str(os.path.basename(os.path.splitext(OutputShape)[0]))
# LayerName = LayerName.encode('utf-8')
print LayerName

# progress.setInfo(LayerName)
# Create Output Shape file
# set up the shapefile driver
driver = ogr.GetDriverByName("ESRI Shapefile")
# progress.setInfo('Bonjour 11')
# Test if shape already exist and if yes delete it
if os.path.exists(OutputShape):
	driver.DeleteDataSource(OutputShape)
# progress.setInfo('Bonjour 11')
# create the data source
data_source = driver.CreateDataSource(OutputShape)
# progress.setInfo('Bonjour 11')
# create the spatial reference, WGS84
srs = osr.SpatialReference()
srs.ImportFromEPSG(4326)
# progress.setInfo('Bonjour 149')
outlayer = data_source.CreateLayer(LayerName, geom_type=ogr.wkbPolygon)
# progress.setInfo('Bonjour 11')
# Add the fields we're interested in
field_name = ogr.FieldDefn("File", ogr.OFTString)
field_name.SetWidth(254)
outlayer.CreateField(field_name)
# progress.setInfo('Bonjour 11')
field_name = ogr.FieldDefn("RelOrbit", ogr.OFTInteger)
field_name.SetWidth(254)
outlayer.CreateField(field_name)
# progress.setInfo('Bonjour 11')
field_name = ogr.FieldDefn("AcqTime", ogr.OFTString)
field_name.SetWidth(254)
outlayer.CreateField(field_name)

field_name = ogr.FieldDefn("AcqDay", ogr.OFTString)
field_name.SetWidth(254)
outlayer.CreateField(field_name)
# progress.setInfo('Bonjour 55')
# Get the output Layer's Feature Definition
outLayerDefn = outlayer.GetLayerDefn()

# progress.setInfo('Bonjour 11')
# List all SENTINEL-1 sub directories
SubFolders = get_immediate_subdirectories(Input_Data_Folder)

S1DataFolders = [ Folder.replace("\\","/") for Folder in SubFolders if '.SAFE' in Folder ]
# progress.setInfo('Bonjour 11')
# Loop thru different S1 data (folder)
for Folder in S1DataFolders:
	ManifestFile = directory_liste_ext_files(Folder, 'manifest.safe')
	ManifestFile = os.path.join(Folder, 'manifest.safe').replace("\\","/")

	SafeName = os.path.split(Folder)[-1]
	FolderName = SafeName.split('.')[0]

	RelativeOrbit = getRelativeOrbit(ManifestFile)
	S1AcqDate = getDateFromS1Folder(FolderName)

	WorkingFolder = os.path.join(TmpDir,FolderName).replace("\\","/")
	if not os.path.exists(WorkingFolder):
		os.makedirs(WorkingFolder)

	# progress.setInfo('Bonjour 113')
	OutputShapeFile = os.path.join(WorkingFolder, "footprint.shp").replace("\\","/")
	CreateS1ShapeFootPrint(ManifestFile,OutputShapeFile)

	# Open the footprint File
	inDataSource = driver.Open(OutputShapeFile, 0)
	inLayer = inDataSource.GetLayer()

	#get spatial ref
	spatialRef = inLayer.GetSpatialRef()

	# We get the polygon
	inFeature = inLayer.GetFeature(0)

	# Create output Feature
	outFeature = ogr.Feature(outLayerDefn)
	outFeature.SetGeometry(inFeature.GetGeometryRef())

	# set field values
	outFeature.SetField("File", ManifestFile)
	outFeature.SetField("RelOrbit", RelativeOrbit)
	outFeature.SetField("AcqTime", S1AcqDate)
	outFeature.SetField("AcqDay", S1AcqDate[0:8])


	# add the feature to the output layer
	outlayer.CreateFeature(outFeature)

	# destroy the features
	inFeature = None
	outFeature = None
	# inLayer = None

	# Close DataSource
	inDataSource = None

OutPrjFile = os.path.splitext(OutputShape)[0] + '.prj'
spatialRef.MorphToESRI()
filePrj = open(OutPrjFile.replace("\\","/"), 'w')
filePrj.write(spatialRef.ExportToWkt())
filePrj.close()

data_source = None

if os.path.exists(TmpDir):
	shutil.rmtree(TmpDir)