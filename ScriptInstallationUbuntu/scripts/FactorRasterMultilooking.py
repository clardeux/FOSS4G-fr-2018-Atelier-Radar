##Raster=group
##Multilook Raster with a Factor=name
##Input_Raster_File=file
##No_Data=number 0
##Raster_Scale_Factor=number 4.
##Output_Raster_File=output raster
from osgeo import gdal
import os, subprocess


#ouverture du fichier input
src_ds = gdal.Open( Input_Raster_File )
if src_ds is None:
	print 'Could not open ' + fn
	sys.exit(1)
else:
	# Get raster information
	cols = src_ds.RasterXSize
	rows = src_ds.RasterYSize

	#lecture de la taille de pixel
	cols_out = int(cols / Raster_Scale_Factor)
	rows_out = int(rows / Raster_Scale_Factor)

	# reprojection
	print ""
	cmd = "gdalwarp  -dstnodata "
	cmd += str(No_Data)
	cmd += " -multi "
	cmd += " -r average -of GTiff "
	cmd += " -ts " + str(cols_out) + " " + str(rows_out) + " "
	cmd += Input_Raster_File
	cmd += " "
	cmd += Output_Raster_File

	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]