# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 18:14:51 2017

@author: cedric
"""

'''
This script apply calibration and orthorectification process of S1 GRD data
'''

'''
IMPORT
'''
import os, sys
from inspect import getsourcefile
import logging
from datetime import datetime




'''
This function join path always using unix sep
'''
def modOsJoinPath(alistToJoin):
    joinedPath = os.path.join(*alistToJoin).replace("\\","/")

    return joinedPath


'''
This function return the qgis script folder
'''
def getQGISProcessingScriptFolder():
    from qgis.core import QgsApplication
    QGISProcessingScriptFolder = os.path.dirname(QgsApplication.qgisSettingsDirPath())
    QGISProcessingScriptFolder = modOsJoinPath([QGISProcessingScriptFolder,
    'processing', 'scripts'])

    return QGISProcessingScriptFolder

'''
This function load the necessary libs in different way
if we are running script in qgis processing or not
'''
def AddS1LibToPath():
    # Boolean to know if in qgis
    import qgis.utils
    inqgis = qgis.utils.iface is not None

    if inqgis:
        QGISProcessingScriptFolder = getQGISProcessingScriptFolder()

        # Create the S1Lib lib folder path
        ScriptPath = modOsJoinPath([QGISProcessingScriptFolder, 'S1Lib'])
    else:
        LocalScriptFileDir = os.path.dirname(os.path.abspath((getsourcefile(lambda:0)))).replace("\\","/")
        # Create the S1Lib lib folder path
        ScriptPath = modOsJoinPath([LocalScriptFileDir, 'S1Lib'])

    # Add path to sys
    sys.path.insert(0,ScriptPath)



# Load OTB Libs
AddS1LibToPath()

# Load the libs
from S1OwnLib import (ReturnRealCalibrationOTBValue,
                          GetFileByExtensionFromDirectory,
                          GetNewDatesFromListZipFilesInput,
                          ReprojVector,
                          CheckAllDifferentRelativeOrbit,
                          getS1ZipByTile,
                          CreateShapeFromPath,
                          ProcessS1ZipDataset,
                          ExtractZipFileFromPartFileName,
                          ExtractFullZipFile,
                          GenerateTemporalAverage)
'''
NOTES
Things to do
    - Optimize the ortorectification by using extent of the polygon to replace clip
'''





##Sentinel-1 IW GRD Batch Processing=group
##1 - Cal. + Orthorect. over Orbits=name
##Input_Data_Folder=folder
##DEM_Folder=folder
##Input_Polygon_File=vector
##Relative_Orbit_Field_Name=field Input_Polygon_File
##Relative_Orbit_To_Process=string 120-47
##Average_All_Date=boolean false
##Calibration_Type=string Gamma0
##Output_EPSG=crs
##Output_Resolution=number 10
##Output_Data_Folder=folder
##Ram=number 512
# END TO DELETE



'''
Input OF THE PROGRAM
'''

'''
This string have to contain the folder path that contain all the unziped S1
 data to process
'''
# Input_Data_Folder = '/home/DataStorage/FrenchGuyana/Input'
# Input_Data_Folder = '/home/osboxes/RemoteSensing/PreTraitementsS1/S1GRDZip'

'''
This string have to contain the folder path that contain all dem covering the
study area. Tiff format is required
'''
# DEM_Folder = '/home/DataStorage/FrenchGuyana/SRTM'
# DEM_Folder = '/home/osboxes/RemoteSensing/PreTraitementsS1/DEM'

'''
Path of the vector file that contain one or more polygon to process
Each polygone will be process independantly like independant tile

This vector have to contain one field with integer type containing
the Relative orbit number to process for the current polygon
'''
# Input_Polygon_File = '/home/DataStorage/TestGuyane/FrenchGuyaneStudyAreaOrbit.shp'
# Input_Polygon_File = '/home/osboxes/RemoteSensing/PreTraitementsS1/Vecteur/MyStudyArea.shp'

'''
Name of the field containing the relative orbit
'''
# Relative_Orbit_Field_Name = 'relorb'


'''
Value of all the relative orbit to process. Have to be separated by '-' like
120-47
'''
# Relative_Orbit_To_Process = '47'


'''
String containing the calibration type:
Sigma0
Gamma0
Beta0
'''
# Calibration_Type = 'Gamma0'

'''
Output EPSG (only in meter)
'''
# Output_EPSG = 'EPSG:32622'
# Output_EPSG = 'EPSG:32622'

'''
Output resolution in meter
'''
# Output_Resolution = '10'

'''
Output folder that will contain one subfolder for each processed polygon and
each relative orbit
'''
# Output_Data_Folder = '/home/DataProcessing/FrenchGuyana/Ortho'
# Output_Data_Folder = '/home/osboxes/RemoteSensing/PreTraitementsS1/S1Ortho'

'''
Amount of allocate ram
In case of use half of the available memory (not the physical memory)
'''
# Ram = 500


# Average_All_Date = True

'''
THE PROGRAM ITSELF
'''
# Internal variable
Noise = 'false'
'''
Main step
1 - Transform Calibration user input into OTB command and get its acronym
2 - List ALL Sentinel-1 Manifest file
3 - Filter the one that not already processed in the output folder
4 - Scan path for all polygon and control that there is different path for all
polygon
5 - Loop thru relative orbit given by the user and
    5a - Loop thru polygon
        For each relative orbit loop thru all the polygon of this orbit
        - Create on subfolder per polygon
        - Filter all the data to take the one which intersect the study area
        - Process the data
'''

# Logging file parameters
TimeNow = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

LogFile =  modOsJoinPath([Output_Data_Folder, 'LogFile-OrthoByPath_' + TimeNow + '.txt'])
logging.basicConfig(filename=LogFile,level=logging.INFO)



'''
- List zip files in input and filter comparing output date do get the new one
- 
'''



# 1 - Transform Calibration user input into OTB command and get its acronym
OTBCalibrationType, CalibrationName = ReturnRealCalibrationOTBValue(Calibration_Type)

# 2 - List ALL Sentinel-1 Manifest file
# List all SENTINEL-1 manifest.safe files
ZipFiles = GetFileByExtensionFromDirectory(Input_Data_Folder, '.zip')


# 3 - Filter the one that not already processed in the output folder
ZipFiles = GetNewDatesFromListZipFilesInput(ZipFiles, Input_Data_Folder, Output_Data_Folder)

# 4 - Scan path for all polygon and control that there is different path for all
# polygon
# Get Path name list user
PathUserList = Relative_Orbit_To_Process.split('-')
PathUserList = [int(path) for path in PathUserList]
CheckAllDifferentRelativeOrbit(Input_Polygon_File,Relative_Orbit_Field_Name, PathUserList)

# Reproject Shapefile to 4326 to be compliant with S1 raw data
Input_Polygon_FileEPSG4326 = Input_Polygon_File.replace('.shp', 'EPSG4326.shp')
ReprojVector(Input_Polygon_File, Input_Polygon_FileEPSG4326, 4326)


# 5 - Loop thru relative orbit given by the user and
#    5a - Loop thru polygon
for userPath in PathUserList:
	# Filter files that intersect the required path
    intersectRaster = getS1ZipByTile(Input_Polygon_FileEPSG4326,
                                  ZipFiles,
                                  Relative_Orbit_Field_Name,
                                  userPath)
    # If intersect is not empty we create output directory
    if len(intersectRaster) > 0:
        PathDir = modOsJoinPath([Output_Data_Folder, 'p' + str(userPath)])
        if not os.path.exists(PathDir):
            os.makedirs(PathDir)
    else: # if no data go to next path
        continue

    # Create Shape file of the current path
    PathShape = modOsJoinPath([PathDir, 'p' + str(userPath) + '.shp'])
    CreateShapeFromPath(Input_Polygon_FileEPSG4326,
                        Relative_Orbit_Field_Name,
                        userPath,
                        PathShape)

    # Run the process
    ProcessS1ZipDataset(intersectRaster,
                     PathDir,
                     PathShape,
                     DEM_Folder,
                     Output_Resolution,
                     Calibration_Type,
                     Noise,
                     CalibrationName,
                     OTBCalibrationType,
                     Output_EPSG,
                     Ram)
	# Average all data if enable
    if Average_All_Date:
		GenerateTemporalAverage(PathDir, True, Ram)
		
GeomFiles=GetFileByExtensionFromDirectory(Output_Data_Folder, '.geom')
for GFile in GeomFiles:
	if os.path.exists(GFile):
		os.remove(GFile)
