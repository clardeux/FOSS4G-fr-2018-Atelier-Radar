RELEASE=`lsb_release -sc`


# Add qgis ltr repository
echo "deb https://qgis.org/debian-ltr ${RELEASE} main" >> /etc/apt/sources.list
echo "deb-src https://qgis.org/debian-ltr ${RELEASE} main" >> /etc/apt/sources.list

apt-key adv --keyserver keyserver.ubuntu.com --recv-key CAEB3DC3BDF7FB45

add-apt-repository ppa:ubuntugis/ubuntugis-unstable

apt-get update -y

# Install packages
echo -ne " Installing GIS/Remote sensing packages ..."
apt-get install --yes --allow-unauthenticated \
											gdal-bin \
											libgdal-dev \
											saga \
											libsaga-dev \
											geotiff-bin \
											libgeotiff-dev \
											spatialite-bin \
											spatialite-gui \
											python-pathlib \
											python-pip \
											python-scipy \
											python-sklearn \
											aria2 \
											curl \
											unrar \
											p7zip-full \
											git \
											qgis \
											grass

# Download and instal OTB
wget https://www.orfeo-toolbox.org/packages/OTB-6.4.0-Linux64.run

chmod +x OTB-6.4.0-Linux64.run && \
mv OTB-6.4.0-Linux64.run /usr/local/lib && \
cd /usr/local/lib && \
./OTB-6.4.0-Linux64.run && \
rm OTB-6.4.0-Linux64.run && \

# Download and instal RIOS
wget https://bitbucket.org/chchrsc/rios/downloads/rios-1.4.4.tar.gz && \
		 tar -xzf rios-1.4.4.tar.gz && \
		 mkdir RIOSInstall && cd rios-1.4.4 && \
		 python setup.py install --prefix=../RIOSInstall && \
		 mv ../RIOSInstall /usr/local/lib

# Instal sentinelsat python lib
pip install --upgrade pip
pip install sentinelsat

# Add env var
touch /etc/profile.d/AddPath.sh  && \
chmod +x /etc/profile.d/AddPath.sh && \
cd -
echo "if [ -d \"/usr/local/lib/OTB-6.4.0-Linux64/bin\" ] ; then" >> /etc/profile.d/AddPath.sh && \
echo "    PATH=\"\$PATH:/usr/local/lib/OTB-6.4.0-Linux64/bin\"" >> /etc/profile.d/AddPath.sh && \
echo "fi" >> /etc/profile.d/AddPath.sh && \
echo "if [ -d \"/usr/local/lib/RIOSInstall/bin\" ] ; then" >> /etc/profile.d/AddPath.sh && \
echo "    PATH=\"\$PATH:/usr/local/lib/RIOSInstall/bin\"" >> /etc/profile.d/AddPath.sh && \
echo "fi" >> /etc/profile.d/AddPath.sh && \
echo "if [ -d \"/usr/local/lib/RIOSInstall/lib/python2.7/site-packages\" ] ; then" >> /etc/profile.d/AddPath.sh && \
echo "    export PYTHONPATH=\"/usr/local/lib/RIOSInstall/lib/python2.7/site-packages\"" >> /etc/profile.d/AddPath.sh && \
echo "fi" >> /etc/profile.d/AddPath.sh